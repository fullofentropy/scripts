import json
import csv
import sys
import os
import collections


STATS_FILE = "src/test_gen_stats.json"


def strip_row(row):
    del row["Timestamp"]
    del row["Prior to taking this exam, did you complete training for Basic Dev?"]
    del row["Prior to taking this exam, did you complete training for Basic Dev? [Score]"]
    del row["Prior to taking this exam, did you complete training for Basic Dev? [Feedback]"]
    del row["Username"]
    del row["Please provide your full name."]
    del row["Please provide your full name. [Score]"]
    del row["Please provide your full name. [Feedback]"]
    del row["Total score"]


def get_question_paths(questions_path):
    question_fname_map = {}
    dir_list = os.listdir(questions_path)
    # look in the testbank's workrole directory specified by user
    for dir_path in dir_list:
        question_dir = "/".join([questions_path, dir_path])
        if os.path.isdir(question_dir):
            for question_conts in os.listdir(question_dir):
                # check if group question folder
                if os.path.isdir("/".join([question_dir, question_conts])):
                    # check each directory in the group
                    group_dir = "/".join([question_dir, question_conts])
                    for group_conts in os.listdir(group_dir):
                        if group_conts.endswith(".json"):
                            question_fname_map["/".join([dir_path, group_conts[:-5]])] = "/".join([group_dir, group_conts])
                # if not group add question filepath to mapping
                if question_conts.endswith(".json"):
                    question_fname_map[question_conts[:-5]] = "/".join([question_dir, question_conts])
    return question_fname_map


def update_questions(exam_file, questions_path):
    # read the configuration and central statistics file
    with open(STATS_FILE) as stats:
        gen_stats = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(stats.read())
    # read contents of specified .csv file
    with open(exam_file, newline='') as exam_csv:
        exam_reader = csv.DictReader(exam_csv)
        csvcont = list(exam_reader)

    # get the dictionary containing answers and clean it
    answers = csvcont[0]
    strip_row(answers)
    # get the dictionary of question names and clean it
    q_names = csvcont[2]
    strip_row(q_names)
    # get a list of the examinees and clean each examinee dictionary
    examinees = csvcont[3:]
    for row in examinees:
        strip_row(row)
    examinee_count = len(examinees)

    # create dict of question names and exam results
    question_chunk = {}
    for key in q_names:
        # filter out the [Score] and [Feedback] for each question
        if not key.endswith("[Score]") and not key.endswith("[Feedback]"):
            correct_q = 0
            for examinee in examinees:
                # look in the corresponding [Score] entry. A score of 1.0 indicates a correct answer
                if examinee[key + " [Score]"].startswith("1"):
                    correct_q += 1
            # add the question with the question name as a key to a new dictionary to easily iterate over results
            question_chunk[q_names[key]] = (examinee_count, correct_q)

    # update the number of exams provisioned for future generation
    gen_stats["number_exams"] += 1

    # update question stats
    # get a mapping of question names to filepaths
    q_map = get_question_paths(questions_path)
    # iterate over each question name that was asked in the exam
    for question in question_chunk:
        try:
            # open the json file at the specified filepath for this question
            with open(q_map[question], "r") as q_file:
                j_dat = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(q_file.read())
            # update the metrics of the question
            j_dat["provisioned"] += 1
            j_dat["attempts"] += question_chunk[question][0]
            j_dat["passes"] += question_chunk[question][1]
            j_dat["failures"] += question_chunk[question][0] - question_chunk[question][1]
            # print(q_map[question], j_dat["provisioned"], j_dat["attempts"], j_dat["passes"], j_dat["failures"])
            # save the updated question metrics
            with open(q_map[question], "w") as q_file:
                json.dump(j_dat, q_file, indent=4)
        except FileNotFoundError:
            print(q_map[question], "was not found")
        except KeyError:
            print("Could not update", question, "statistics.")
            print("attempts:", question_chunk[question][0], "passes:", question_chunk[question][1], "failures:",
                  question_chunk[question][0] - question_chunk[question][1])

    # write updated stats back to file
    with open(STATS_FILE, "w") as stats:
        json.dump(gen_stats, stats, indent=4)


if __name__ == '__main__':
    update_questions(sys.argv[1], sys.argv[2])
