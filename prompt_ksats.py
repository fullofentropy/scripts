#!/usr/bin/python3

import json
import argparse
import os
import shutil
import pymongo
import re
import subprocess
import lib.mttl_db_helpers

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links

file_padding = {".c": "  ", ".py" : "  ", ".md" : "- "}

list_intro = "This question is intended to evaluate the following topics:"
python_prompt_header = f'"""\n{list_intro}\n'
c_prompt_header = f'/*\n{list_intro}\n'
md_prompt_header = f'## {list_intro}\n'
clone_cmd = ["git", "clone"]
mttl_ssh = "git@gitlab.com:90cos/mttl.git"

c_reg = r'(?s)\/[*]\nThis question is intended to evaluate.*?[*]\/[\n]*'
py_reg = r'(?s)\"\"\"\nThis question is intended to evaluate.*?\"\"\"[\n]*'
md_reg = r'(?s)## This question is intended to evaluate.*?[\n]*-\s*[KSAT]\d{,4} - .*?[\n]{2,2}'

FILE_NAME_LIST = set(["TestCode", "testfile", "client", "instructions", "Instructions"])


def gitclone(git_url):
    subprocess.run(clone_cmd + [mttl_ssh], stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def embed_ksats(q_ksat_prompt, root, test_file):
    with open(os.path.join(root, test_file)) as base_file:
        try:
            file_conts = base_file.read()
        except UnicodeDecodeError:
            print(f"can not read from {os.path.join(root,test_file)}, Unicode error")
    file_conts = re.sub(c_reg, "", file_conts)
    file_conts = re.sub(py_reg, "", file_conts)
    file_conts = re.sub(md_reg, "", file_conts)
    q_ksat_prompt += file_conts
    with open(os.path.join(root, test_file), "w") as rewrite:
        try:
            rewrite.write(q_ksat_prompt)
            print(f"wrote to {os.path.join(root,test_file)}")
        except UnicodeDecodeError:
            print(f"can not write to {os.path.join(root,test_file)}, Unicode error")

def pad_lines(ksa_prompt, ext):
    listed_prompts = []
    pad = file_padding[ext]
    for line in ksa_prompt.splitlines():
        listed_prompts.append(f"{pad}{line}")
    return listed_prompts

def main():
    parser = argparse.ArgumentParser(description="Add or update KSAT lists to performance questions.")
    parser.add_argument("target", help="Path to the directory containing performance content. The content can be at any level within the directory.")
    args = parser.parse_args()
    gitclone(mttl_ssh)
    os.chdir("./mttl")
    client.drop_database("mttl")
    subprocess.run("scripts/before_script.sh")
    os.chdir("..")

    ksats_not_found_list = []
    solutions_not_found = []

    # get KSAT lists
    for root, dirs, files in os.walk(args.target):
        ksa_prompt = ""
        for file_name in files:  # have to use file name because directory name and file name are not always the same
            if file_name.endswith(".question.json"):
                with open(os.path.join(root, file_name)) as q_file:
                    q_conts = json.load(q_file)
                ksat_list = lib.mttl_db_helpers.get_question_ksats(q_conts["rel-link_id"], db)
                for ksat in ksat_list:
                    desc = reqs.find_one({"ksat_id": ksat})["description"]
                    ksa_prompt += f"{ksat} - {desc}\n"  # add new line with KSAT and description

                # write prompts
                prompts_written = False
                content_files = os.listdir(root)
                for c_file in content_files:
                    c_file_name, ext = os.path.splitext(c_file)
                    if c_file_name in FILE_NAME_LIST:
                        prepend_list = False
                        if ext == ".c":
                            ## do C
                            listed_prompts = "\n".join(pad_lines(ksa_prompt, ext))
                            q_ksat_prompt = f'{c_prompt_header}{listed_prompts}*/\n'
                            prepend_list = True
                        elif ext == ".py":
                            ## do python
                            listed_prompts = "\n".join(pad_lines(ksa_prompt, ext))
                            q_ksat_prompt = f'{python_prompt_header}{listed_prompts}"""\n'
                            prepend_list = True
                        elif ext == ".md":
                            ## do markdown
                            listed_prompts = "\n".join(pad_lines(ksa_prompt, ext))
                            q_ksat_prompt = f'{md_prompt_header}\n{listed_prompts}\n\n'
                            prepend_list = True
                        
                        if prepend_list:
                            embed_ksats(q_ksat_prompt, root, c_file)
                            prompts_written = True
                            try:
                                embed_ksats(q_ksat_prompt, os.path.join(root, "Solution"), c_file)
                            except FileNotFoundError:
                                solutions_not_found.append(os.path.join(root, "Solution", c_file))
                        if prompts_written:
                            break
                if not prompts_written:
                    # write to a text file by default
                    with open(os.path.join(root, "KSAT_list.txt"), "w") as ksat_list:
                        ksat_list.write(ksa_prompt)

    shutil.rmtree("./mttl")

    if solutions_not_found:
        for line in solutions_not_found:
            print(line, "was not found")

    if ksats_not_found_list:
        for line in ksats_not_found_list:
            print("could not find ksats for", line)


if __name__ == "__main__":
    main()
