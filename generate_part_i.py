import argparse
import jinja2
import os
import pymongo
from itertools import chain

# default values if environment does not provide them
HOST = os.environ.get("MONGO_HOST", "localhost")
PORT = int(os.environ.get("MONGO_PORT", 27017))

# connect to the mongodb and switch to the rel_links collection
client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
rls = db.rel_links

class PartI:
    def __init__(self, module, topic, subjects):
        self.module = module
        self.topic = topic
        self.subjects = subjects
        self.lesson_objectives = self.get_lesson_objectives()
        self.performance_objectives = self.get_performance_objectives()
        self.ksats = self.get_ksats()
        self.lecture_time = self.get_lecture_time()
        self.perf_demo_time = self.get_perf_demo_time()
        self.references = self.get_references()

    def get_lesson_objectives(self):
        return [lo for subject in self.subjects if subject.get("lesson_objectives") for lo in subject["lesson_objectives"]]

    def get_performance_objectives(self):
        return [po for subject in self.subjects if subject.get("performance_objectives") for po in subject["performance_objectives"]]

    def get_ksats(self):
        # collect all KSATs from the lesson objectives.
        nested_ksats = []
        for lo in self.lesson_objectives:
            # if KSATs are defined, toss them in a list
            if lo.get("KSATs"):
                nested_ksats.append(lo["KSATs"])

            # now we check if MSBs is not empty
            if lo.get("MSBs"):
                for msb in lo["MSBs"]:
                    # we still only want existing and non-empty KSATs
                    if msb.get("KSATs"):
                        nested_ksats.append(msb["KSATs"])

        # flatten the ksat lists
        lo_ksats = list(chain.from_iterable(nested_ksats))

        # we can collect and flatten the performance objective ksats because we never nest a po
        po_ksats = list(chain.from_iterable(po["KSATs"] for po in self.performance_objectives if po.get("KSATs")))

        return sorted(set(lo_ksats + po_ksats))

    def get_lecture_time(self):
        return sum([subject.get("lecture_time", 0) for subject in self.subjects])

    def get_perf_demo_time(self):
        return sum([subject.get("perf_demo_time", 0) for subject in self.subjects])
    
    def format_time(self, total_time):
        hours = int(total_time / 60)
        minutes = int(total_time % 60)

        # format the string if hours or minutes are not 0 and plurize when neccessary
        formatted_hours = f"{hours} Hour{'s' if hours > 1 else ''}" if hours else ''
        formatted_minutes = f"{minutes} Minute{'s' if minutes != 1 else ''}" if minutes else ''

        # filter out any empty strings and joins whatever is left with a space if need be
        return " ".join(filter(None, [formatted_hours, formatted_minutes]))

    def get_references(self):
        return sorted([reference for subject in self.subjects if subject.get("references") for reference in subject["references"]])

    def to_dict(self):
        return {
            "module_name": self.module,
            "topic_name": self.topic,
            "ksats": self.ksats,
            "lecture_time": self.format_time(self.lecture_time),
            "perf_demo_time": self.format_time(self.perf_demo_time),
            "lesson_objectives": self.lesson_objectives,
            "performance_objectives": self.performance_objectives,
            "references": self.references
        }


# prepare the template environment
env = jinja2.Environment(loader=jinja2.FileSystemLoader("scripts/part_i_templates"))
part_i_template = env.get_template("part_i.html")
objectives_template = env.get_template("objectives.html")

# read the script arguments
parser = argparse.ArgumentParser(description="Generate Part I")
parser.add_argument("module", type=str, help="MongoDB module name to retrieve rel-links.")
module = parser.parse_args().module

# find all the unique topics
topics = rls.find({"module": module}).distinct("topic")

for topic in topics:
    # don't forget list() or you can only itereate once over the returned Cursor object
    subjects = list(rls.find({"$and": [{"module": module}, {"topic": topic}]}))

    part_i = PartI(module, topic, subjects)

    topic_path = {subject["topic_path"] for subject in subjects}.pop()

    # write out the part_i markdown document
    with open(os.path.join(topic_path, "part_i.md"), "w") as md_file:
        md_file.write(
            part_i_template.render(part_i.to_dict())
        )

    # write out the objectives markdown document
    with open(os.path.join(topic_path, "objectives.md"), "w") as md_file:
        md_file.write(
            objectives_template.render(part_i.to_dict())
        )
