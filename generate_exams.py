import json
import os
import argparse
import shutil
import errno
import pymongo
import datetime
import subprocess
from gen_exam_gitlab_ci_yml import get_gitlab_ci_yml
from lib.exam_generator import evalbuilder
import gen_knowl_exam_csv as GenGSFiles
from lib.file_list_generator import dir_walk
from lib.default_strings import keyword_desc, keyword_default, knowledge_key, performance_key, topic_key, \
    question_type_key
from lib import mttl_db_helpers

DB_NAME_DEFAULT = "mttl"
DB_NAME_DESC = "This is the name of the data base and is used for DB connection purposes. " \
               f"The default is {DB_NAME_DEFAULT}"
DESCRIPTION = "This will pick questions that will go on an exam."
LOG_FILE_DEFAULT = "GenKnowledgeReport.json"
LOG_FILE_DESC = f"The name of the report file; the default is '{LOG_FILE_DEFAULT}'."
MAX_CHOICES = 9
MAX_CHOICES_DEFAULT = 5
MAX_CHOICES_DESC = "This is the maximum number of choices to allow per multiple choice question. " \
                   f"The default is '{MAX_CHOICES_DEFAULT}'."
MIN_CHOICES = 2
MONGO_HOST_DESC = f"This is the host name where the Mongo Database is running. " \
                  f"The default is '{mttl_db_helpers.HOST_NAME}'"
MONGO_PORT_DESC = f"This is the port where the Mongo Database is running. The default is '{mttl_db_helpers.PORT_NUM}'"
NAMEOF_MQL_CSV_DEFAULT = "Master_Question_List.csv"
NAMEOF_MQL_CSV_DESC = "The name of the Master Question List (MQL) CSV file. Note, it should have a .csv extension. " \
                      f"The default is '{NAMEOF_MQL_CSV_DEFAULT}'."
OUT_DIR_DEFAULT = "generated_exams/"
OUT_DIR_DESC = "The directory to store the resulting Generate Google Script Files report. " \
               f"The default is '{OUT_DIR_DEFAULT}'."
PERF_EXAM_NAME_DEFAULT = "perf_exam"
PERF_EXAM_NAME_DESC = "The name of the output file for each performance exam. Note, the output is a compressed file. " \
                      f"The default is '{PERF_EXAM_NAME_DEFAULT}'."
ROOT_QUEST_DIRS_DESC = "The path to one or more root directories with the desired questions, relative to the current " \
                       "working directory. This allows specifying both knowledge and performance question content."
STATS_FILE_DEFAULT = "test_gen_stats.json"
STATS_FILE_LOC_DEF = "scripts/lib/exam_generator"
STATS_FILE_DESC = f"The file storing all the test generation statistics; " \
                  f"the default is '{STATS_FILE_LOC_DEF}/{STATS_FILE_DEFAULT}'."
TYPEOF_QUEST_CHOICES = ["knowledge", "performance", "both"]
TYPEOF_QUEST_DEFAULT = "knowledge"
TYPEOF_QUEST_DESC = "Indicates whether knowledge or performance questions should be generated. " \
                    f"Options are '{TYPEOF_QUEST_CHOICES}'; the default is '{TYPEOF_QUEST_DEFAULT}'."

EXAM_FILES_DIR_DEFAULT = 'scripts/exam_files'
EXAM_FILES_DIR_DESC = "Specify directory to copy extra exam files from"
IGNORE_PATTERN_DEFAULT = {"*Solution*", "*.question.json"}


def get_test_gen_stats(stats_file: str) -> dict:
    '''
    Purpose: This loads the stored generator statistics data into a dictionary.
    :param stats_file: The path to the file with the stored generator statistics data.
    :return: gen_stats - The stored generator statistics data
    '''
    gen_stats = {}
    try:
        with open(stats_file) as stats:
            gen_stats = json.load(stats)
    except (json.JSONDecodeError, FileNotFoundError) as err:
        raise ValueError(f"Unable to load {stats_file}: {err}")
    return gen_stats


def get_cur_topics(question_list: list) -> dict:
    '''
    Purpose: This gets a generator statistics dictionary of all current topics in use, according to the input list.
    :param question_list: The list of questions being considered as possible exam question candidates.
    :return: stat_map - The new generator statistics topics data
    '''
    topics = {}
    for question in question_list:
        try:
            with open(question, "r") as q_file:
                q_data = json.load(q_file)
        except (json.JSONDecodeError, FileNotFoundError) as err:
            raise ValueError(f"Unable to load {question}: {err}")
        else:
            stat_type = None
            topic = None
            if question_type_key in q_data and q_data[question_type_key] == "knowledge":
                stat_type = "knowledge_stats"
            elif question_type_key in q_data and q_data[question_type_key].startswith("performance"):
                stat_type = "performance_stats"
            if topic_key in q_data:
                topic = q_data[topic_key]
            if stat_type is not None and topic is not None:
                if stat_type not in topics:
                    topics[stat_type] = {}
                if "topics" not in topics[stat_type]:
                    topics[stat_type]["topics"] = {}
                if topic not in topics[stat_type]["topics"]:
                    topics[stat_type]["topics"][topic] = 0
    return topics


def update_topics(src_topics: dict, dst_gen_stats: dict) -> None:
    '''
    Purpose: This updates the generator statistics with any new topics, as returned from get_cur_topics().
    :param src_topics: The updated topics discovered in the question list.
    :param dst_gen_stats: The data from the generator statistics file stored in the repository.
    :return: None
    '''
    for key, value in src_topics.items():
        try:
            topics = value["topics"]
            for topic in topics.keys():
                if topic not in dst_gen_stats[key]["topics"]:
                    dst_gen_stats[key]["topics"][topic] = value["topics"][topic]
        except (KeyError, TypeError):
            pass


def check_old_topics(current_topics: dict, gen_stats: dict) -> None:
    '''
    Purpose: This checks the generator statistics data for any outdated topics.
    :param current_topics: The updated topics discovered in the question list.
    :param gen_stats: The data from the generator statistics file stored in the repository.
    :return: None
    '''
    for key, value in gen_stats.items():
        try:
            topics = value["topics"]
            for topic in topics.keys():
                if topic not in current_topics[key]["topics"]:
                    print(f"WARNING: Input '{key}' topics configuration, with outdated topic "
                          f"'{topic}':'{topics[topic]}', is not currently in use.")
        except (KeyError, TypeError):
            pass


def check_unused_category(generator_stats: dict, exam_type: str):
    '''
    Purpose: This checks for topics that have zero questions configured for use.
    :param generator_stats: The data from the generator statistics file stored in the repository.
    :param exam_type: Specifies if the exams being generated are 'knowledge', 'performance' or 'both'.
    :return: None
    '''
    for exam_t, value in generator_stats.items():
        try:
            topics = value["topics"]
            for topic, amount in topics.items():
                if amount == 0 and (exam_type in exam_t or exam_type == "both"):
                    print(f"WARNING: Input '{exam_t}' configuration is requesting '{amount}' '{topic}' questions.")
        except (KeyError, TypeError):
            pass


def copy_quest_files(src: str, dst: str, ignore_patterns: set = None):
    '''
    Purpose: This copies the necessary files for a performance question to the requested destination. Note the
    destination folder must not already exist. Solution folders, and their contents, are not copied.
    :param src: The source folder with the files to be copied.
    :param dst: The destination folder where the files are to end up; note, the destination folder must not already
                exist.
    :param ignore_patterns: Patterns to ignore when copying files. The default is '*Solution*'.
    :return: None
    '''
    if ignore_patterns is None:
        ignore_patterns = IGNORE_PATTERN_DEFAULT
    try:
        # Begin by copying directories
        shutil.copytree(src, dst, ignore=shutil.ignore_patterns(*ignore_patterns), dirs_exist_ok=True)
    except NotADirectoryError:
        shutil.copy(src, dst, ignore=shutil.ignore_patterns(*ignore_patterns))


def generate_knowledge(generator_stats: dict, eb: evalbuilder.EvalBuilder, nameof_mql_csv: str, max_choices: int,
                       out_dir: str, keyword: str, db_obj: pymongo.database.Database):
    '''
    Purpose: This generates the requested amount of Knowledge exams.
    :param generator_stats: The data from the generator statistics file stored in the repository.
    :param eb: The EvalBuilder object used : for exam generation.
    :param nameof_mql_csv: The name for the output Master Question List CSV file.
    :param max_choices: The max allowable number of choices; the valid range is from 2 - 9.
    :param out_dir: The name of the output folder.
    :param keyword: A keyword to search for in the file structure
    :param db_obj: The PyMongo Database object
    :return: None
    '''
    eb.clear_categories()  # Start with fresh list of categories
    prev_exams = []  # Store previous exams in case the new exam has the same questions
    total_quest = 0
    eb.time_allowed = generator_stats["knowledge_stats"]["time_allowed"]
    for topic in sorted(generator_stats["knowledge_stats"]["topics"]):
        eb.register_category(topic, generator_stats["knowledge_stats"]["topics"][topic])
        total_quest += generator_stats["knowledge_stats"]["topics"][topic]
    eb.sort_cat_list_by_value()
    for exam in range(eb.exams_to_generate):
        exam_ver = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d-%H%M%S%f")
        exam_q_list = eb.create_exam(knowledge_key, prev_exams)
        eb.feedback_update(exam_q_list)
        col_headers = GenGSFiles.get_col_headers(max_choices)
        mql_csv = f"{os.path.splitext(nameof_mql_csv)[0]}_{exam_ver}.csv"  # To generate separate exam files
        GenGSFiles.gen_google_script_csv(exam_q_list, out_dir, mql_csv, max_choices, keyword, col_headers, db_obj)
        prev_exams.append(exam_q_list)


def generate_performance(generator_stats: dict, eb: evalbuilder.EvalBuilder, perf_exam_name: str, out_dir: str,
                         exam_files: str):
    '''
    Purpose: This generates the requested amount of Performance exams.
    :param generator_stats: The data from the generator statistics file stored in the repository.
    :param eb: The EvalBuilder object used for exam generation.
    :param perf_exam_name: A base name for each performance exam created.
    :param out_dir: The name of the output folder.
    :param exam_files: A string representing the path to the exam files that should be included in each exam.
    :return: None
    '''
    eb.clear_categories()  # Start with fresh list of categories
    prev_exams = []  # Store previous exams in case the new exam has the same questions
    total_quest = 0
    eb.time_allowed = generator_stats["performance_stats"]["time_allowed"]
    for topic in sorted(generator_stats["performance_stats"]["topics"]):
        eb.register_category(topic, generator_stats["performance_stats"]["topics"][topic])
        total_quest += generator_stats["performance_stats"]["topics"][topic]
    eb.sort_cat_list_by_value()
    # clone googletest once
    subprocess.run('git clone https://github.com/google/googletest.git'.split())
    for exam in range(eb.exams_to_generate):
        exam_ver = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d-%H%M%S%f")
        src_base = os.path.join(out_dir, f"{perf_exam_name}_{exam_ver}")
        if not os.path.exists(src_base):
            os.makedirs(src_base)
        exam_q_list = eb.create_exam(performance_key, prev_exams)
        for exam_q in exam_q_list:
            this_quest = eb.get_question(exam_q)
            dst_dir = os.path.join(src_base, os.path.join(this_quest.topic.replace(" ", "_"),
                                                          os.path.basename(os.path.dirname(exam_q))))
            copy_quest_files(os.path.dirname(exam_q), dst_dir)
            if 'C_Programming' in exam_q:
                copy_quest_files("googletest", os.path.join(dst_dir, "googletest"), {".git"})
        copy_quest_files(os.path.abspath(exam_files), src_base)
        eb.feedback_update(exam_q_list)
        prev_exams.append(exam_q_list)
        with open(os.path.join(src_base, ".gitlab-ci.yml"), "w") as pipeline_fp:
            # Build and save the .gitlab-ci.yml file for each exam
            pipeline_fp.write(get_gitlab_ci_yml(src_base))


def main(args: argparse.Namespace):
    '''
    Purpose: This is the starting point for the exam generator.
    :param: args: The arguments collected by argparse.
    :return: None
    '''
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', args.Host)
    port = int(os.getenv('MONGO_PORT', args.Port))
    client = pymongo.MongoClient(host, port)
    db = client[args.db_name]

    q_list = dir_walk(os.getcwd(), args.root_quest_dirs, keyword=args.filter)
    eb = evalbuilder.EvalBuilder()
    # Build fresh stat map with current topics
    current_topics = get_cur_topics(q_list)
    generator_stats = get_test_gen_stats(args.stats_file)
    check_old_topics(current_topics=current_topics, gen_stats=generator_stats)
    update_topics(src_topics=current_topics, dst_gen_stats=generator_stats)
    check_unused_category(generator_stats, args.typeof_question)  # Send warning for topics config'd with 0 questions

    eb.set_question_list(q_list, db)
    if args.typeof_question == "knowledge" or args.typeof_question == "both":
        eb.exams_to_generate = generator_stats["knowledge_stats"]["exams_to_gen"]
        generate_knowledge(generator_stats, eb, args.nameof_mql_csv, args.max_choices, args.out_dir,
                           args.filter, db)
    if args.typeof_question == "performance" or args.typeof_question == "both":
        eb.exams_to_generate = generator_stats["performance_stats"]["exams_to_gen"]
        generate_performance(generator_stats, eb, args.perf_exam_name, args.out_dir, args.exam_files)
    generator_stats["exams_generated"] += eb.exams_generated
    try:
        with open(args.stats_file, "w") as stats_f:
            json.dump(generator_stats, stats_f, indent=4)
    except (json.JSONDecodeError, FileNotFoundError) as err:
        print(f"Unable to save updated generator stats to {args.stats_file}: {err}.")
    else:
        print(f"Updated generator stats saved to {args.stats_file}.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument("root_quest_dirs", nargs='+', type=str, help=ROOT_QUEST_DIRS_DESC)
    parser.add_argument("-d", "--db_name", type=str, default=DB_NAME_DEFAULT, help=DB_NAME_DESC)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-H", "--Host", type=str, default=mttl_db_helpers.HOST_NAME, help=MONGO_HOST_DESC)
    parser.add_argument("-l", "--log_file", type=str, default=LOG_FILE_DEFAULT, help=LOG_FILE_DESC)
    parser.add_argument("-n", "--nameof_mql_csv", type=str, default=NAMEOF_MQL_CSV_DEFAULT, help=NAMEOF_MQL_CSV_DESC)
    parser.add_argument("-m", "--max_choices", type=int, choices=range(MIN_CHOICES, MAX_CHOICES),
                        default=MAX_CHOICES_DEFAULT, help=MAX_CHOICES_DESC)
    parser.add_argument("-o", "--out_dir", type=str, default=OUT_DIR_DEFAULT, help=OUT_DIR_DESC)
    parser.add_argument("-p", "--perf_exam_name", type=str, default=PERF_EXAM_NAME_DEFAULT, help=PERF_EXAM_NAME_DESC)
    parser.add_argument("-P", "--Port", type=str, default=mttl_db_helpers.PORT_NUM, help=MONGO_PORT_DESC)
    parser.add_argument("-s", "--stats_file", type=str, default=f"{STATS_FILE_LOC_DEF}/{STATS_FILE_DEFAULT}",
                        help=OUT_DIR_DESC)
    parser.add_argument("-t", "--typeof_question", type=str, choices=TYPEOF_QUEST_CHOICES,
                        default=TYPEOF_QUEST_DEFAULT, help=TYPEOF_QUEST_DESC)
    parser.add_argument("-e", "--exam_files", type=str, default=EXAM_FILES_DIR_DEFAULT, help=EXAM_FILES_DIR_DESC)
    params = parser.parse_args()

    main(params)
