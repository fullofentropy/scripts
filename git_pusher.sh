#!/bin/bash

# git diff-index --quiet HEAD --
[[ -z $(git status -s) ]]
if [ $? -eq 1 ]
then 
	echo "In do git loop"
	# git add README.md
	git commit -m 'Updating with latest content.'
    git push $*
else 
	exit 0
fi
