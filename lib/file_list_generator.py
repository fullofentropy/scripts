#!/usr/bin/python3
import os
from git import *


def strip_avoid_paths(paths: list, avoid_list: list, avoid_files_only: bool = False):
    '''
    Purpose: This function removes any unwanted paths from a list of paths according to the input avoid_list.
    :param paths: The paths to validate.
    :param avoid_list: A list of directories to avoid.
    :param avoid_files_only: Determines if only files should be considered when avoiding
    :return: result: The resulting list of wanted paths.
    '''
    result = []

    for path in paths:
        skip = False
        for avoid in avoid_list:
            avoid = avoid.strip("\'")
            if avoid_files_only and avoid.lower() in os.path.basename(path.lower()):
                skip = True
                break
            elif not avoid_files_only and avoid.lower() in path.lower():
                skip = True
                break
        if not skip:
            result.append(path)

    return result


def changed_paths(repo_base_dir: str, keyword: str = ".json", avoid_list: list = None) -> list:
    '''
    Purpose: The changed_paths function will return a list of changed .json files from the current HEAD
    :param repo_base_dir: The base directory of the desired repository
    :param keyword: A filter used to extract paths containing keyword; default=.json
    :param avoid_list: A list of directories to avoid
    :return: list of relative paths: String, or an empty list if nothing was found
    '''
    repository = Repo(repo_base_dir)
    pathstmp = repository.git.diff('HEAD', name_only=True).split("\n")
    paths = []  # empty buff where our filtered paths will go

    for path in pathstmp:
        # Search all changed files for the keyword and add them to the paths list
        if keyword in path:
            # Ensure starting data is removed so just the path is left and slashes are used for directory separators
            paths.append(os.path.join(repo_base_dir, path))

    if avoid_list is not None and len(avoid_list) > 0:
        paths = strip_avoid_paths(paths, avoid_list)

    return paths


def dir_walk(root_dir: str, json_dirs: list, keyword: str = ".json", recurse: bool = True,
             avoid_list: list = None, avoid_files_only: bool = False) -> list:
    '''
    Purpose: The dir_walk function will walk the inputted directory path and return a list of files containing keyword.
    :param root_dir: String path to where the walk starts
    :param json_dirs: A list of directories to search, in relation to root_dir. This allows searching multiple
                          directories within root_dir's base directory. Note, the json_dir will be appended to
                          root_dir; i.e. root_dir/json_dir/
    :param keyword: A filter used to extract paths containing keyword; default=*.json
    :param recurse: Determines if the search should recurse into subdirectories; default=True. If set to True then the
                    search will look in all sub-folders; if set to False then the search will only look in the current
                    folder.
    :param avoid_list: A list of directories to avoid
    :param avoid_files_only: Determines if only files should be considered when avoiding
    :return: list of relative paths: String, or an empty list if nothing was found
    '''
    # Stores the list of paths to files with filter in their path
    paths = []

    for folder in json_dirs:
        # Get files with filter in their path from all root json folders
        if recurse:
            # In case the search should look in sub-folders
            for (root, dirs, files) in os.walk(os.path.join(root_dir, folder)):
                # Search the current root json folder for files with filter
                for file in files:
                    if keyword in file:
                        # In case a file is found with filter
                        paths.append(os.path.join(root, file))
        else:
            # In case the search shouldn't include sub-folders
            for file in os.listdir(os.path.join(root_dir, folder)):
                if keyword in file:
                    # In case a file is found with filter
                    paths.append(os.path.join(root_dir, folder, file))

    if avoid_list is not None and len(avoid_list) > 0:
        paths = strip_avoid_paths(paths, avoid_list, avoid_files_only)

    return paths


def get_dirs(root_dir: str) -> list:
    '''
    Purpose: Returns a list of directories within the current root_dir only, doesn't recurse sub-directories. Note, the
    list excludes hidden directories as specified in a UNIX/LINUX environment.
    :param root_dir: The directory to look for directories.
    :return: q_dirs = A list of directories within root_dir; an empty list indicates nothing found.
    '''
    q_dirs = []
    for item in os.listdir(root_dir):
        if os.path.isdir(os.path.join(root_dir, item)) and not item.startswith("."):
            q_dirs.append(item)
    return q_dirs


if __name__ == "__main__":
    # Tests to validate functions
    print("This tests collecting a list of only changed JSON files in the repository")
    print(changed_paths("../"))
    print("This tests collecting a list of only changed py files in the repository")
    print(changed_paths("./", keyword=".py"))
    print("\nThis tests a recursive list of JSON files starting in the 'questions' directory")
    print(dir_walk("../", ["./"]))
    print("\nThis tests a non-recursive list of md files starting in the repository directory")
    print(dir_walk("../", ["./"], recurse=False, keyword=".md"))
