'''
This data is necessary for building a mock MTTL DB that's representative of the production MTTL and removes the
possibility of messing up the MTTL as well as provides stable data for testing purposes. This data was separated into
a separate file from the fake_mttl_mongo_db due to its length.
'''
rdmap_data = {
    "PO": {
        "courses": [
            {
                "name": "PSPO I (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            },
            {
                "name": "Qualification Exam (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "Knowledge Study Material",
                        "url": "https://gitlab.com/90cos/product-owner-po-eval-study/basic-po/-/blob/master/"
                               "knowledge/README.md#master-question-list"
                    },
                    {
                        "name": "Performance Study Material",
                        "url": ""
                    }
                ]
            }
        ],
        "trn-rel-link-urls": [
            "https://gitlab.com/90cos/training/courses/external/scrum.org-professional-scrum-product-owner-i.git"
        ],
        "evl-rel-link-urls": [
            "https://gitlab.com/90cos/public/evaluations/preparatory/basic-po"
        ],
        "follow-ons": [
            "SPO"
        ]
    },
    "SPO": {
        "courses": [],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": [
            "MPO"
        ]
    },
    "MPO": {
        "courses": [
            {
                "name": "SEE",
                "url": "https://gitlab.com/90cos/training/modules/see-basics/-/tree/master/Training%20Slides"
            },
            {
                "name": "Instructor",
                "url": ""
            }
        ],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": []
    },
    "SM": {
        "courses": [
            {
                "name": "PSM I (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            },
            {
                "name": "[Temp] Same as PO Exam (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "PO Knowledge Study Material",
                        "url": "https://gitlab.com/90cos/product-owner-po-eval-study/basic-po/-/blob/master/"
                               "knowledge/README.md#master-question-list"
                    },
                    {
                        "name": "PO Performance Study Material",
                        "url": ""
                    }
                ]
            }
        ],
        "trn-rel-link-urls": [
        ],
        "evl-rel-link-urls": [
        ],
        "follow-ons": [
            "SSM"
        ]
    },
    "SSM": {
        "courses": [],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": [
            "MSM"
        ]
    },
    "MSM": {
        "courses": [
            {
                "name": "SEE",
                "url": "https://gitlab.com/90cos/training/modules/see-basics/-/tree/master/Training%20Slides"
            },
            {
                "name": "Instructor",
                "url": ""
            }
        ],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": []
    },
    "CCD": {
        "courses": [
            {
                "name": "IDF (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "Python",
                        "url": "https://90cos.gitlab.io/training/modules/python/"
                    },
                    {
                        "name": "C Programming",
                        "url": "https://90cos.gitlab.io/training/modules/C-Programming/"
                    },
                    {
                        "name": "Reverse Engineering",
                        "url": "https://90cos.gitlab.io/training/modules/reverse-engineering/"
                    },
                    {
                        "name": "Algorithms",
                        "url": "https://90cos.gitlab.io/training/modules/algorithms/"
                    },
                    {
                        "name": "Powershell",
                        "url": "https://90cos.gitlab.io/training/modules/powershell/"
                    },
                    {
                        "name": "Debugging",
                        "url": "https://90cos.gitlab.io/training/modules/debugging/"
                    },
                    {
                        "name": "Pseudocode",
                        "url": "https://90cos.gitlab.io/training/modules/pseudocode/"
                    },
                    {
                        "name": "C++ Programming",
                        "url": "https://90cos.gitlab.io/training/modules/cpp-programming/"
                    },
                    {
                        "name": "Git",
                        "url": "https://90cos.gitlab.io/training/modules/introduction-to-git/"
                    },
                    {
                        "name": "Assembly",
                        "url": "https://90cos.gitlab.io/training/modules/assembly/"
                    },
                    {
                        "name": "Networking",
                        "url": "https://90cos.gitlab.io/training/modules/network-programming/"
                    }
                ]
            },
            {
                "name": "Qualification Exam (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "Knowledge Study Material",
                        "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                               "ccd-master-question-file/-/tree/master/knowledge#master-question-list"
                    },

                    {
                        "name": "Performance Study Material",
                        "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                               "ccd-master-question-file/-/tree/master/performance"
                    }
                ]
            }
        ],
        "trn-rel-link-urls": [
            "https://gitlab.com/90cos/training/modules/python.git",
            "https://gitlab.com/90cos/training/modules/c-programming.git",
            "https://gitlab.com/90cos/training/modules/reverse-engineering.git",
            "https://gitlab.com/90cos/training/modules/algorithms.git",
            "https://gitlab.com/90cos/training/modules/powershell.git",
            "https://gitlab.com/90cos/training/modules/debugging.git",
            "https://gitlab.com/90cos/training/modules/pseudocode.git",
            "https://gitlab.com/90cos/training/modules/cpp-programming.git",
            "https://gitlab.com/90cos/training/modules/introduction-to-git.git",
            "https://gitlab.com/90cos/training/modules/assembly.git",
            "https://gitlab.com/90cos/training/modules/network-programming.git"
        ],
        "evl-rel-link-urls": [
            "https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev.git"
        ],
        "follow-ons": [
            "SCCD-L",
            "SCCD-W",
            "Special Missions",
            "TAE"
        ]
    },
    "SCCD-L": {
        "courses": [
            {
                "name": "ACTP (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            },
            {
                "name": "Qualification Exam (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "Performance Study Material",
                        "url": "https://gitlab.com/90cos/senior-ccd-linux-sccd-l-eval-study/"
                               "sccd-l-master-question-file/-/tree/master/performance"
                    }
                ]
            }
        ],
        "trn-rel-link-urls": [
            "https://gitlab.com/90cos/training/courses/external/actp-linux.git"
        ],
        "evl-rel-link-urls": [
            "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux.git"
        ],
        "follow-ons": [
            "MCCD"
        ]
    },
    "SCCD-W": {
        "courses": [
            {
                "name": "ACTP (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            },
            {
                "name": "Qualification Exam (NIPR)",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "Performance Study Material",
                        "url": "https://gitlab.com/90cos/senior-ccd-windows-sccd-w-eval-study/"
                               "sccd-w-master-question-file/-/tree/master/performance"
                    }
                ]
            }
        ],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [
            "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows.git"
        ],
        "follow-ons": [
            "MCCD"
        ]
    },
    "MCCD": {
        "courses": [
            {
                "name": "SEE",
                "url": "https://gitlab.com/90cos/training/modules/see-basics/-/tree/master/Training%20Slides"
            },
            {
                "name": "Instructor",
                "url": ""
            }
        ],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": []
    },
    "Special Missions": {
        "courses": [
            {
                "name": "VR",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11",
                "lessons": [
                    {
                        "name": "https://ghidra.re",
                        "url": "https://ghidra.re/online-courses/"
                    }
                ]
            },
            {
                "name": "Mobile",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            },
            {
                "name": "Networking",
                "url": "https://jira.90cos.hmc.hpc.mil/servicedesk/customer/portal/11"
            }
        ],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": [
        ]
    },

    "Sysadmin": {
        "courses": [],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": [
            "TAE"
        ]
    },
    "TAE": {
        "courses": [],
        "trn-rel-link-urls": [],
        "evl-rel-link-urls": [],
        "follow-ons": [
            "MCCD"
        ]
    }
}
work_role_data = [
    {
        "work-role": "CCD",
        "ksat_id": "A0004",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0353",
        "proficiency": "1"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0355",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0356",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0358",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0564",
        "proficiency": "1"
    },
    {
        "work-role": "CCD",
        "ksat_id": "A0563",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0816",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0823",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0825",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0824",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0889",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0689",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0756",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0755",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0029",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0132",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0023",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "K0834",
        "proficiency": "B"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0176",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0175",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0181",
        "proficiency": "1"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0180",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0179",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0082",
        "proficiency": "3"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0081",
        "proficiency": "3"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0083",
        "proficiency": "3"
    },
    {
        "work-role": "CCD",
        "ksat_id": "S0172",
        "proficiency": "2"
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0028",
        "proficiency": ""
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0031",
        "proficiency": ""
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0036",
        "proficiency": ""
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0032",
        "proficiency": ""
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0018",
        "proficiency": ""
    },
    {
        "work-role": "CCD",
        "ksat_id": "T0009",
        "proficiency": ""
    },
    {
        "work-role": "PO",
        "ksat_id": "A0639",
        "proficiency": "2"
    },
    {
        "work-role": "PO",
        "ksat_id": "A0641",
        "proficiency": "1"
    },
    {
        "work-role": "PO",
        "ksat_id": "A0640",
        "proficiency": "1"
    },
    {
        "work-role": "PO",
        "ksat_id": "A0634",
        "proficiency": "1"
    },
    {
        "work-role": "PO",
        "ksat_id": "A0642",
        "proficiency": "1"
    },
    {
        "work-role": "PO",
        "ksat_id": "K0260",
        "proficiency": ""
    },
    {
        "work-role": "PO",
        "ksat_id": "K0365",
        "proficiency": "C"
    },
    {
        "work-role": "PO",
        "ksat_id": "K0369",
        "proficiency": "B"
    },
    {
        "work-role": "PO",
        "ksat_id": "K0372",
        "proficiency": "A"
    },
    {
        "work-role": "PO",
        "ksat_id": "K0368",
        "proficiency": "B"
    },
    {
        "work-role": "PO",
        "ksat_id": "S0191",
        "proficiency": "1"
    },
    {
        "work-role": "PO",
        "ksat_id": "S0195",
        "proficiency": "2"
    },
    {
        "work-role": "PO",
        "ksat_id": "S0194",
        "proficiency": "2"
    },
    {
        "work-role": "PO",
        "ksat_id": "S0200",
        "proficiency": "2"
    },
    {
        "work-role": "PO",
        "ksat_id": "S0201",
        "proficiency": "2"
    },
    {
        "work-role": "SEE",
        "ksat_id": "S0219",
        "proficiency": "3"
    },
    {
        "work-role": "SEE",
        "ksat_id": "S0220",
        "proficiency": "3"
    },
    {
        "work-role": "SEE",
        "ksat_id": "S0221",
        "proficiency": "3"
    },
    {
        "work-role": "SEE",
        "ksat_id": "S0222",
        "proficiency": "3"
    },
    {
        "work-role": "SEE",
        "ksat_id": "S0215",
        "proficiency": "3"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "A0544",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "A0543",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "A0554",
        "proficiency": "3"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "A0630",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "A0631",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "K0497",
        "proficiency": "B"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "K0496",
        "proficiency": "B"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "K0508",
        "proficiency": "B"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "K0506",
        "proficiency": "B"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "K0507",
        "proficiency": "B"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "S0015",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "S0105",
        "proficiency": "3"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "S0130",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "S0131",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-L",
        "ksat_id": "S0133",
        "proficiency": "2"
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "A0695",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "A0630",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "A0694",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "A0632",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "A0735",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "K0071",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "K0459",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "K0483",
        "proficiency": "A"
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "K0484",
        "proficiency": "A"
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "K0488",
        "proficiency": "A"
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "S0015",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "S0105",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "S0130",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "S0131",
        "proficiency": ""
    },
    {
        "work-role": "SCCD-W",
        "ksat_id": "S0133",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "A0461",
        "proficiency": "3"
    },
    {
        "work-role": "SPO",
        "ksat_id": "A0444",
        "proficiency": "3"
    },
    {
        "work-role": "SPO",
        "ksat_id": "A0452",
        "proficiency": "2"
    },
    {
        "work-role": "SPO",
        "ksat_id": "A0645",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "A0638",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "K0375",
        "proficiency": "B"
    },
    {
        "work-role": "SPO",
        "ksat_id": "K0374",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "K0376",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "K0378",
        "proficiency": "B"
    },
    {
        "work-role": "SPO",
        "ksat_id": "K0380",
        "proficiency": "B"
    },
    {
        "work-role": "SPO",
        "ksat_id": "S0192",
        "proficiency": "2"
    },
    {
        "work-role": "SPO",
        "ksat_id": "S0190",
        "proficiency": "2"
    },
    {
        "work-role": "SPO",
        "ksat_id": "S0191",
        "proficiency": ""
    },
    {
        "work-role": "SPO",
        "ksat_id": "S0193",
        "proficiency": "3"
    },
    {
        "work-role": "SPO",
        "ksat_id": "S0196",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "A0739",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "A0737",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "A0742",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "A0740",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "A0741",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "S0334",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "K0730",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "K0830",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "K0836",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "K0833",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "K0831",
        "proficiency": "C"
    },
    {
        "work-role": "TAE",
        "ksat_id": "S0207",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "S0206",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "S0208",
        "proficiency": "3"
    },
    {
        "work-role": "TAE",
        "ksat_id": "S0209",
        "proficiency": "3"
    },
    {
        "work-role": "VR",
        "ksat_id": "K0766",
        "proficiency": "C"
    },
    {
        "work-role": "VR",
        "ksat_id": "S0204",
        "proficiency": "2"
    }
]
requirement_data = [
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d95"
        },
        "ksat_id": "S0024",
        "description": "Declare and/or implement container data type.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "List, Multi-Dimensional List, Dictionary, Tuple, and Set",
        "updated_on": "2020-07-09"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a3c"
        },
        "ksat_id": "A0061",
        "description": "Create and implement functions to meet a requirement.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "In addition to the program package itself (program plus comments, documentation, etc) must PRESENT the functionality to an evaluator.",
        "updated_on": "2020-07-09"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a36"
        },
        "ksat_id": "A0117",
        "description": "Develop client/server network programs.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f08"
            }
        ],
        "topic": "Python",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a3a"
        },
        "ksat_id": "A0087",
        "description": "Create and implement a sort routine.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ef0"
            }
        ],
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Pick which algorithm to implement; \"create\" could mean finding psuedocode online and using it",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d98"
        },
        "ksat_id": "S0029",
        "description": "Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-07-09"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32da4"
        },
        "ksat_id": "S0033",
        "description": "Utilize assignment operators to update a variable.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d9b"
        },
        "ksat_id": "S0034",
        "description": "Declare and implement appropriate data types for program requirments.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            }
        ],
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "short, int, float, char, double, and long",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32de4"
        },
        "ksat_id": "S0097",
        "description": "Create and use pointers.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            }
        ],
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Declare, assign, derefernce etc.",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dff"
        },
        "ksat_id": "S0129",
        "description": "Utilize common string instructions.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dee"
        },
        "ksat_id": "S0125",
        "description": "Utilize general purpose instructions.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32def"
        },
        "ksat_id": "S0126",
        "description": "Utilize arithmetic instructions.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e20"
        },
        "ksat_id": "S0136",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Utilize x86 calling conventions",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e21"
        },
        "ksat_id": "S0134",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Utilize registers and sub-registers",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a66"
        },
        "ksat_id": "A0610",
        "parent": [],
        "description": "Disassemble Binary",
        "topic": "Reverse Engineering",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a69"
        },
        "ksat_id": "A0609",
        "parent": [],
        "description": "WinDbg Dynamic Analysis",
        "topic": "Reverse Engineering",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32df8"
        },
        "ksat_id": "S0110",
        "description": "Implement error handling.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "buffer overflow, array index out of bounds, divide by zero etc.",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d93"
        },
        "ksat_id": "S0026",
        "description": "Utilize standard library modules.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "sys, os, regex et.al.",
        "updated_on": "2020-07-09"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dfb"
        },
        "ksat_id": "S0096",
        "description": "Utilize the Socket library.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a36"
            }
        ],
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a49"
        },
        "ksat_id": "A0019",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ef5"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32ef1"
            }
        ],
        "description": "Integrate functionality between multiple software components.",
        "topic": "Software Engineering fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Take multiple blocks of code that they have developed/functions and integrate them together",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a4b"
        },
        "ksat_id": "A0018",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ef5"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32ef1"
            }
        ],
        "description": "Analyze a problem to formulate a software solution.",
        "topic": "Software Engineering fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "I.e. Identify functions, variables, inputs, outputs, memory considerations, modularity, error "
                    "conditions, etc",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e1c"
        },
        "ksat_id": "S0141",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Execute instructions using raw opcodes",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e2f"
        },
        "ksat_id": "S0177",
        "description": "Write assembly code that accomplishes the same functionality as a given a high level language "
                       "(C or Python) code snippet.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d18"
        },
        "ksat_id": "K0798",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32dff"
            }
        ],
        "description": "Understand the purpose of the scas instruction.",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32de7"
        },
        "ksat_id": "S0117",
        "description": "Utilize labels for control flow.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32aba"
        },
        "ksat_id": "K0014",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32d95"
            }
        ],
        "description": "Understand how to declare and implement a Dictionary.",
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32abe"
        },
        "ksat_id": "K0041",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32aa5"
            }
        ],
        "description": "Describe the behavior of factory design pattern.",
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-08-18"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cb3"
        },
        "ksat_id": "K0690",
        "parent": [],
        "description": "(U) Explain the purpose and usage of elements and components of the Python programming "
                       "language",
        "topic": "Python",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "KSA21",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32abc"
        },
        "ksat_id": "K0040",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32aa5"
            }
        ],
        "description": "Describe the purpose of a class attribute.",
        "topic": "Python",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ae1"
        },
        "ksat_id": "K0087",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adc"
            }
        ],
        "description": "Understand the behavior of Network address translation (NAT)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Add to 3.7.1 in JQR",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ae0"
        },
        "ksat_id": "K0088",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adc"
            }
        ],
        "description": "Understand how to apply CIDR notation to an IP address.",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Add to 3.7.1 in JQR",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c77"
        },
        "ksat_id": "K0617",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f08"
            }
        ],
        "description": "Know the purpose and how to use Reading Request For Comments (RFC)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ad6"
        },
        "ksat_id": "K0065",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a36"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a6b"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32cb7"
            }
        ],
        "description": "Understand constructs of the User Datagram Protocol (UDP)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c70"
        },
        "ksat_id": "K0610",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a36"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a6b"
            }
        ],
        "description": "Understand how to utilize ports and protocols in network programming.",
        "topic": "Networking Concepts",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ade"
        },
        "ksat_id": "K0081",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adc"
            }
        ],
        "description": "Understand how an IPv4 address is constructed.",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ae3"
        },
        "ksat_id": "K0089",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ad9"
            }
        ],
        "description": "Describe common network devices",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Add to 3.7.1 in JQR",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c72"
        },
        "ksat_id": "K0611",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ad9"
            }
        ],
        "description": "Address Resolution Protocol (ARP)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c73"
        },
        "ksat_id": "K0613",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adb"
            }
        ],
        "description": "Domain Name System (DNS)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c76"
        },
        "ksat_id": "K0616",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adb"
            }
        ],
        "description": "Dynamic Host Configuration Protocol (DHCP)",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ad2"
        },
        "ksat_id": "K0061",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a7f"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32b86"
            }
        ],
        "description": "Describe the meaning of First in First Out (FIFO) and Last in First Out (LIFO)",
        "topic": "Data Structures",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ace"
        },
        "ksat_id": "K0057",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32dcb"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32dba"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32db9"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32b86"
            }
        ],
        "description": "Describe how to implement and use a Doubly linked list",
        "topic": "Data Structures",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ad0"
        },
        "ksat_id": "K0060",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a7f"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32b86"
            }
        ],
        "description": "Identify common pitfalls when using linked lists, queues, trees, and graphs",
        "topic": "Data Structures",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cc0"
        },
        "ksat_id": "K0700",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a7f"
            }
        ],
        "description": "(U) Explain the concepts and terminology of datastructures and associated algorithms (e.g., "
                       "search, sort, traverse, insert, delete).",
        "topic": "Data Structures",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "KSA31",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ae4"
        },
        "ksat_id": "K0093",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3c"
            }
        ],
        "description": "Understand the purpose and use of the main() function",
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32aee"
        },
        "ksat_id": "K0103",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32de4"
            }
        ],
        "description": "Understand how to create and utilize pointers",
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ab6"
        },
        "ksat_id": "K0009",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3c"
            }
        ],
        "description": "Understand how to create and implement functions.",
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cec"
        },
        "ksat_id": "K0735",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32da4"
            }
        ],
        "description": "Identify and understand assignment operators.",
        "topic": "Programming Fundamentals",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-19"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ccd"
        },
        "ksat_id": "K0716",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32d98"
            }
        ],
        "description": "Identify and understand arithmetic operators (PEMDAS)",
        "topic": "Programming Fundamentals",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ae9"
        },
        "ksat_id": "K0096",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32d9b"
            }
        ],
        "description": "Know how to declare and implement data types",
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "short, int, float, char, double, and long",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cfd"
        },
        "ksat_id": "K0769",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32dee"
            }
        ],
        "description": "Understand the purpose of the movzx instruction.",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c3a"
        },
        "ksat_id": "K0528",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e20"
            }
        ],
        "description": "Describe how to use x86 64 Calling Conventions",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d0b"
        },
        "ksat_id": "K0781",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32def"
            }
        ],
        "description": "Understand the purpose of the div instruction.",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b38"
        },
        "ksat_id": "K0201",
        "parent": [],
        "description": "Describe specifics about the x86 architecture.",
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Needs additional section",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b3d"
        },
        "ksat_id": "K0213",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e21"
            }
        ],
        "description": "Identify and describe the 64-bit registers",
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cca"
        },
        "ksat_id": "K0691",
        "parent": [],
        "description": "(U) Explain the purpose and usage of elements of Assembly language (e.g., x86 and x86_64, ARM, "
                       "PowerPC).",
        "topic": "Assembly Programming",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "KSA22",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b4d"
        },
        "ksat_id": "K0204",
        "parent": [],
        "description": "Differentiate between types of memory (registers, cache, system memory)",
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Needs additional section",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b41"
        },
        "ksat_id": "K0202",
        "parent": [],
        "description": "Describe specifics about the x86-64 architecture",
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Needs additional section",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32aff"
        },
        "ksat_id": "K0134",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3a"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            }
        ],
        "description": "Know the runtime efficiency for a Selection Sort",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32afe"
        },
        "ksat_id": "K0133",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3a"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            }
        ],
        "description": "Know the runtime efficiency for an Insertion Sort",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b02"
        },
        "ksat_id": "K0136",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3a"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            }
        ],
        "description": "Know the runtime efficiency for a Heap Sort",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b05"
        },
        "ksat_id": "K0135",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3a"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            }
        ],
        "description": "Know the runtime efficiency for a Merge Sort",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ac3"
        },
        "ksat_id": "K0046",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ac2"
            }
        ],
        "description": "Describe the behavior of depth first traversal.",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ac4"
        },
        "ksat_id": "K0047",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ac2"
            }
        ],
        "description": "Describe the behavior of breadth first traversal.",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d3d"
        },
        "ksat_id": "K0834",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a35"
            }
        ],
        "description": "Understand the function of a program written in Pseudocode ",
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e29"
        },
        "ksat_id": "S0172",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a35"
            }
        ],
        "description": "Write Pseudocode with repeating steps to the process",
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a3d"
        },
        "ksat_id": "A0093",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ef0"
            }
        ],
        "description": "Analyze data structures and implement most efficient Big-O sorting routine.",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b01"
        },
        "ksat_id": "K0132",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3d"
            }
        ],
        "description": "Understand the varying degrees of Asymptotic notation (Big-O) pertaining to algorithms.",
        "topic": "Algorithms",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Given scenario know when or when not to use certain algorithms due to processing time",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32aa0"
        },
        "ksat_id": "K0023",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "description": "Identify differences between Python 2 vs Python 3",
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a5c"
        },
        "ksat_id": "A0563",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "description": "Programming with Functions and Metaclasses",
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32aa8"
        },
        "ksat_id": "K0029",
        "parent": [],
        "description": "Describe the purpose and use of C-types",
        "topic": "Python",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Add to 3.2.1 in JQR",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cee"
        },
        "ksat_id": "K0755",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32dfa"
            }
        ],
        "description": "Understand how to implement a switch statement.",
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dda"
        },
        "ksat_id": "S0084",
        "description": "Compile and link a program.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            }
        ],
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b07"
        },
        "ksat_id": "K0140",
        "description": "Describe concepts of compiling, linking, debugging, and executables.",
        "parent": [],
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32df2"
        },
        "ksat_id": "S0081",
        "description": "Implement a looping construct.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "For, while (do/while in C)",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dfa"
        },
        "ksat_id": "S0082",
        "description": "Implement conditional control flow constructs.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "if, if/else, switch etc.",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cdc"
        },
        "ksat_id": "K0689",
        "parent": [],
        "description": "(U) Explain the purpose and usage of elements and components of the C programming language",
        "topic": "C",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "KSA20",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e42"
        },
        "ksat_id": "S0199",
        "parent": [],
        "description": "Incorporate validated assumptions into the Scrum framework.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bd3"
        },
        "ksat_id": "K0405",
        "description": "Validating Product Assumptions",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e42"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32ebb"
            }
        ],
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a3f"
        },
        "ksat_id": "A0116",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f08"
            }
        ],
        "description": "Debug IP network traffic.",
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32de3"
        },
        "ksat_id": "S0095",
        "description": "Utilize Wireshark to analyze and debug a client/server program's network traffic.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3f"
            }
        ],
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f00"
        },
        "ksat_id": "T0014",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Develop, modify, and utilize automation technologies to enable employment of "
                       "capabilities as efficiently as possible (e.g. TDD, CI/CD, etc.)",
        "topic": "DevOps",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T014",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ef0"
        },
        "ksat_id": "T0024",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Utilize data structures to organize, sort, and manipulate elements of information",
        "topic": "Data Structures",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T024",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a50"
        },
        "ksat_id": "A0459",
        "description": "Create, Maintain, and Refine the Product Backlog",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32efe"
            }
        ],
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a5a"
        },
        "ksat_id": "A0564",
        "parent": [],
        "description": "Multi-threading",
        "topic": "Python",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a42"
        },
        "ksat_id": "A0343",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Conduct static analysis of a binary to determine its functionality.",
        "topic": "Reverse Engineering",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "Something 'simple' like hello world",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a3e"
        },
        "ksat_id": "A0344",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Conduct dynamic analysis of a binary to determine its functionality.",
        "topic": "Reverse Engineering",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "Something 'simple' like hello world"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ded"
        },
        "ksat_id": "S0122",
        "description": "Invoke system calls.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ee9"
        },
        "ksat_id": "T0005",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Interpret customer requirements and evaluate resource and system constraints to create "
                       "solution design specifications.",
        "topic": "Agile",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T005",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e0d"
        },
        "ksat_id": "S0155",
        "description": "Utilize man pages to find function details and utilize function",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a2e"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a34"
            }
        ],
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "File IO, File and Directory Management, Date/Time Conventions, Signals, IPC, Sockets, Inline "
                    "Functions, Buffered IO, Advanced IO, PIDS, Fork(), exec(), wait(), waidpid(), system(), "
                    "setuid(), setgid(), setreuid(), setreguid(), setpgid(), getpriority(), setpriority(), nice()",
        "updated_on": "2020-08-20"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e0b"
        },
        "ksat_id": "S0154",
        "description": "Utilize MSDN Documents to find function details",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a2e"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a34"
            }
        ],
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "registry, Processes (CreateProcess(), Synchronization: WaitForSingleObject()), Threads "
                    "(CreateThread(), CreateMutex()), Windows Sockets (WSAStartup), WSACleanup()...), Windows "
                    "Services (StartServiceCtrlDispatcher()...)"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a39"
        },
        "ksat_id": "A0082",
        "description": "Identify and eliminate memory leaks.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            }
        ],
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e07"
        },
        "ksat_id": "S0151",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a39"
            }
        ],
        "description": "Zero-out memory securely",
        "topic": "C",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ef1"
        },
        "ksat_id": "T0025",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Design and develop user interfaces (e.g. web pages, GUIs, CLIs, Console Interfaces)",
        "topic": "Software Engineering",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T025",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ef5"
        },
        "ksat_id": "T0023",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Design and develop data requirements, database structure, process flow, systematic "
                       "procedures, algorithms, data analysis, and file structures.",
        "topic": "Software Engineering",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T023",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f09"
        },
        "ksat_id": "T0012",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document custom interface specifications between "
                       "interconnected systems.",
        "topic": "Software Engineering",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T012",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a2e"
        },
        "ksat_id": "A0408",
        "description": "Given a 4-6 person team and customer requirements/assigned scenario, develop a tool.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee9"
            }
        ],
        "topic": "Tool Development",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Difficulty level 1; Build a client on Windows and server on Linux to transfer files back and "
                    "forth.; Build a Windows tool in C that receives data and writes to disk.; Build a Windows tool"
                    " in C that will list running processes.; RATs Windows and Linux that implement the following "
                    "functionality: Execute user commands (e.g. ps, netstat, etc.) and returns output. Capture "
                    "running processes by API and return output. Return the current user. (Windows) Pull registry "
                    "keys specified by instructor (e.g. Run, RunOnce). (Linux) Pull the contents of /etc/shadow and"
                    " /etc/issue. Return a file/directory listing using API. Upload and download a file from the "
                    "system the RAT is on. Change the current working directory of the RAT (future user executed "
                    "commands should inherit the current working directory). Add self to startup process "
                    "(Persistence- students are told exactly where). Burn off - Delete persistence and exit "
                    "process. Determine hosts on the collision domain (ARP sniffing). Implements a time-based call "
                    "back (use standard TCP for C2). Create custom C2 protocol. Encrypt file (use instructor "
                    "provided library). Receive call back domain/IP via command line arguments. Notes: Not "
                    "expecting this to be an enterprise level tool. This will be proof of concept. Not looking for "
                    "OPSEC, stealth or authentication of RATs or network traffic."
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a34"
        },
        "ksat_id": "A0419",
        "description": "Given a 4-6 person team and incomplete customer requirements/assigned scenario, develop a "
                       "tool.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee9"
            }
        ],
        "topic": "Tool Development",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Difficulty level 4 and 5"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32eec"
        },
        "ksat_id": "T0008",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document software and applications which run in "
                       "kernel and user space.",
        "topic": "Kernel Development",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T008",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32eea"
        },
        "ksat_id": "T0009",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eec"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document software and applications in C programming"
                       " language.",
        "topic": "C",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T009",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f04"
        },
        "ksat_id": "T0010",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document software and applications in Python "
                       "programming language.",
        "topic": "Python",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T010",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f08"
        },
        "ksat_id": "T0011",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document software and applications utilizing "
                       "standard, non-standard, specialized, and/or unique network communication protocols.",
        "topic": "Network Analysis/Programming",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T011",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f0a"
        },
        "ksat_id": "T0017",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Analyze, modify, develop, debug and document software and applications using assembly "
                       "languages.",
        "topic": "Assembly Programming",
        "requirement_src": [
            "JCT\u0026CS",
            "ACC CCD CDD TTL"
        ],
        "requirement_src_id": "T01",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a25"
        },
        "ksat_id": "A0353",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            }
        ],
        "description": "Perform test driven development",
        "topic": "Development Operations",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a26"
        },
        "ksat_id": "A0355",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32eea"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f04"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f08"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Package an applicaton/tool for delivery.",
        "topic": "Development Operations",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "Should this be a task?"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a27"
        },
        "ksat_id": "A0356",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a26"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a2e"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a34"
            }
        ],
        "description": "Write an application/tool user guide",
        "topic": "Development Operations",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a29"
        },
        "ksat_id": "A0358",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a26"
            }
        ],
        "description": "Write application/tool installation documentation",
        "topic": "Development Operations",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a35"
        },
        "ksat_id": "A0004",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f09"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32ef5"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32ef1"
            }
        ],
        "description": "Write pseudocode to solve a programming problem",
        "topic": "Programming Fundamentals",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d74"
        },
        "ksat_id": "K0889",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ec0"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Identify the four Agile values.",
        "topic": "Agile",
        "created_on": "2020-06-24"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d35"
        },
        "ksat_id": "K0823",
        "description": "Understand how to zero-out allocated memory.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e07"
            }
        ],
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d33"
        },
        "ksat_id": "K0824",
        "description": "Understand how to find information in MSDN documents.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e0b"
            }
        ],
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d34"
        },
        "ksat_id": "K0825",
        "description": "Understand how to use man pages in linux to find information.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e0d"
            }
        ],
        "topic": "C",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d2b"
        },
        "ksat_id": "K0816",
        "description": "Describe how to invoke system calls.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ded"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e31"
        },
        "ksat_id": "S0179",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a42"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3e"
            }
        ],
        "description": "Reverse engineer a binary using IDA Pro.",
        "topic": "Reverse Engineering",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Something 'simple' like hello world"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e33"
        },
        "ksat_id": "S0180",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a3e"
            }
        ],
        "description": "Reverse engineer a binary using WinDBG.",
        "topic": "Reverse Engineering",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Something 'simple' like hello world"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e36"
        },
        "ksat_id": "S0181",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a42"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32a3e"
            }
        ],
        "description": "Analyze binaries to identify function calls",
        "topic": "Reverse Engineering",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "Something 'simple' like hello world"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e2b"
        },
        "ksat_id": "S0175",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a5a"
            }
        ],
        "description": "Implement a thread in an OS utility.",
        "topic": "Operating System",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e2e"
        },
        "ksat_id": "S0176",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a5a"
            }
        ],
        "description": "Implement an OS utility that requires a lock.",
        "topic": "Operating System",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ee8"
        },
        "ksat_id": "T0001",
        "parent": [],
        "description": "(U) Modify cyber capabilites to detect and disrupt nation-state cyber threat actors across "
                       "the threat chain (kill chain) of adversary execution",
        "topic": "Mission",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T001",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32eed"
        },
        "ksat_id": "T0002",
        "parent": [],
        "description": "(U) Create or enhance cyberspace capabilities to compromise, deny, degrade, disrupt, "
                       "destroy, or manipulate automated information systems.",
        "topic": "Mission",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T002",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f02"
        },
        "ksat_id": "T0003",
        "parent": [],
        "description": "(U) Create or enhance cyberspace solutions to enable surveillance and reconnaissance of "
                       "automated information systems.",
        "topic": "Mission",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T003",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f05"
        },
        "ksat_id": "T0004",
        "parent": [],
        "description": "(U) Create or enhance cyberspace solutions to enable the defense of national interests.",
        "topic": "Mission",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T004",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f07"
        },
        "ksat_id": "T0018",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Utilize tools to decompile, disassembe, analzye, and reverse engineer compiled "
                       "binaries.",
        "topic": "Reverse Engineering",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T018",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32f01"
        },
        "ksat_id": "T0036",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Enhance capability design strategies and tactics by synthesizing information, "
                       "processes, and techniques in the areas of malicious software, vulnerabilities, reverse "
                       "engineering, secure software engineering, and exploitation.",
        "topic": "Software Engineering",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T036",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ef9"
        },
        "ksat_id": "T0031",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Locate and utilize technical specifications and industry standards (e.g. Internet "
                       "Engineering Task Force (IETF), IEEE, IEC, International Standards Organization (ISO)).",
        "topic": "Standards",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T031",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32efb"
        },
        "ksat_id": "T0032",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Deliver technical documentation (e.g. user guides, design documentation, test plans, "
                       "change logs, training materials, etc.) to end users.",
        "topic": "Technical Writing",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T032",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ef6"
        },
        "ksat_id": "T0028",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ee8"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32eed"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f02"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f05"
            }
        ],
        "description": "(U) Apply cryptography primitives to protect the confidentiality and integrity of sensitive"
                       " data.",
        "topic": "Implement Security Measures",
        "requirement_src": [
            "JCT\u0026CS"
        ],
        "requirement_src_id": "T028",
        "requirement_owner": "USCYBERCOM/J7",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ba8"
        },
        "ksat_id": "K0372",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Understand decision-making approaches a Product Owner might use.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ba6"
        },
        "ksat_id": "K0368",
        "parent": [],
        "description": "Describe benifits of mastering the Product Owner role.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ba5"
        },
        "ksat_id": "K0369",
        "parent": [],
        "description": "Identify the impact on a Scrum Team and organization anti-patterns that might exist for "
                       "Product Owners.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32b5d"
        },
        "ksat_id": "K0260",
        "description": "Identify steps to create or refine a product backlog item",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e44"
            }
        ],
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e44"
        },
        "ksat_id": "S0200",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a50"
            }
        ],
        "description": "Create Product Backlog item that includes description of desired outcome and value.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e3d"
        },
        "ksat_id": "S0194",
        "parent": [],
        "description": "Prioritize between conflicting customer (or user) needs.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e50"
        },
        "ksat_id": "S0215",
        "parent": [],
        "description": "Obtain all required evaluation material ",
        "topic": "Stan/Eval",
        "requirement_src": [
            "ACCI 17-202v2 "
        ],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e54"
        },
        "ksat_id": "S0219",
        "parent": [],
        "description": "Brief examiner/examinee evaluation roles and responsibilities",
        "topic": "Stan/Eval",
        "requirement_src": [
            "ACCI 17-202v2 "
        ],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e56"
        },
        "ksat_id": "S0220",
        "parent": [],
        "description": "Identify and document all discrepancies in the appropriate evaluation area",
        "topic": "Stan/Eval",
        "requirement_src": [
            "ACCI 17-202v2 "
        ],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e58"
        },
        "ksat_id": "S0221",
        "parent": [],
        "description": "Assign proper area grades based on discrepancy documentation and criteria guidance",
        "topic": "Stan/Eval",
        "requirement_src": [
            "ACCI 17-202v2 "
        ],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e5a"
        },
        "ksat_id": "S0222",
        "parent": [],
        "description": "Factor all cumulative deviations in the examinee's performance when assigning overall "
                       "grade",
        "topic": "Stan/Eval",
        "requirement_src": [
            "ACCI 17-202v2 "
        ],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a53"
        },
        "ksat_id": "A0543",
        "parent": [],
        "description": "Develop a system program (Kernel)",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a55"
        },
        "ksat_id": "A0544",
        "parent": [],
        "description": "Develop a shell program",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a56"
        },
        "ksat_id": "A0554",
        "parent": [],
        "description": "Develop a basic driver",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a6d"
        },
        "ksat_id": "A0631",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adc"
            }
        ],
        "description": "IPv4 Addressing",
        "topic": "Networking Concepts",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c39"
        },
        "ksat_id": "K0506",
        "parent": [],
        "description": "Executable and Linkable Format (ELF)",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c24"
        },
        "ksat_id": "K0507",
        "parent": [],
        "description": "Files System Management Utilities",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c37"
        },
        "ksat_id": "K0508",
        "parent": [],
        "description": "Variables",
        "topic": "Linux Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c17"
        },
        "ksat_id": "K0496",
        "parent": [],
        "description": "Makefiles",
        "topic": "*nix",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c11"
        },
        "ksat_id": "K0497",
        "parent": [],
        "description": "Special File Systems",
        "topic": "*nix",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a7f"
        },
        "ksat_id": "A0735",
        "description": "Demonstrate the ability to create and use complex Data Structures.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32ef0"
            }
        ],
        "topic": "Data Structures",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "Elf Parsing for Senior Linux, Others include Linked Lists, Doubly-Linked Lists, "
                    "Circularly-Linked Lists, Trees, Binary Search Trees, Weighted Graph, Hash Tables, Queues, and "
                    "Stacks.",
        "updated_on": "2020-07-22"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a77"
        },
        "ksat_id": "A0694",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "description": "Ability to configure Gitlab CI YAML files to define stages and jobs.",
        "topic": "CI/CD",
        "requirement_src": [
            "https://gitlab.com/90cos/public/mttl/-/merge_requests/17"
        ],
        "requirement_owner": "90COS/CYK",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a6c"
        },
        "ksat_id": "A0632",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32adc"
            }
        ],
        "description": "IPv6 Addressing",
        "topic": "Networking Concepts",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a76"
        },
        "ksat_id": "A0695",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "description": "Ability to configure Gitlab CI YAML files generate and store artifacts.",
        "topic": "CI/CD",
        "requirement_src": [
            "https://gitlab.com/90cos/public/mttl/-/merge_requests/17"
        ],
        "requirement_owner": "90COS/CYK",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c0d"
        },
        "ksat_id": "K0488",
        "parent": [],
        "description": "Alternate Driver Architectures",
        "topic": "Windows Kernel Development",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c06"
        },
        "ksat_id": "K0483",
        "parent": [],
        "description": "Basic Administration Using The Graphic User Interface (GUI)",
        "topic": "Windows Operating System",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32c05"
        },
        "ksat_id": "K0484",
        "parent": [],
        "description": "Basic Administration Using Command Line (CLI)",
        "topic": "Windows Operating System",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bee"
        },
        "ksat_id": "K0459",
        "parent": [],
        "description": "Real-Time Operations and Innovation (RTO\u0026I) Development Requirements",
        "topic": "Organization Overview",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32adc"
        },
        "ksat_id": "K0071",
        "description": "Describe the purpose of OSI Layer 3",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32de3"
            },
            {
                "$oid": "5f457f1e1ea90ba9adb32f08"
            }
        ],
        "topic": "Networking Concepts",
        "requirement_src": [
            "ACC CCD CDD TTL"
        ],
        "requirement_owner": "ACC A3/2/6KO",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e18"
        },
        "ksat_id": "S0133",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Utilize the stack frame",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e16"
        },
        "ksat_id": "S0130",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Utilize a GDB debugger to identify errors in a program.",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dfe"
        },
        "ksat_id": "S0131",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "description": "Utilize WinDBG debugger to identify errors in a program.",
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dac"
        },
        "ksat_id": "S0015",
        "description": "Register a Gitlab runner.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "CI/CD",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32dfd"
        },
        "ksat_id": "S0105",
        "description": "Create and use a C datatypes: Word, DoubleWord, and Quadword.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f0a"
            }
        ],
        "topic": "Assembly Programming",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a4f"
        },
        "ksat_id": "A0461",
        "parent": [],
        "description": "Organize and facilitate a session with stakeholders to break down a solution or feature as "
                       "progressively smaller items that may be completed in Sprints.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a4e"
        },
        "ksat_id": "A0444",
        "parent": [],
        "description": "Create a product plan or forecast with stakeholders.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bb7"
        },
        "ksat_id": "K0380",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Describe ways a group of stakeholders could reach their final decision.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bae"
        },
        "ksat_id": "K0378",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Identify indicators when a group is engaged in divergent thinking and indicators where a "
                       "group is engaged in convergent thinking.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32ba9"
        },
        "ksat_id": "K0374",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Identify stakeholder groups.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bac"
        },
        "ksat_id": "K0375",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Describe techniques to collaborate with the key stakeholders.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32bb8"
        },
        "ksat_id": "K0376",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32e3b"
            }
        ],
        "description": "Identify in a scenario when the Product Owner should not act as the facilitator for the stakeholders.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e3f"
        },
        "ksat_id": "S0196",
        "parent": [],
        "description": "Use proper techniques to connect teams directly to customers and users.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e3a"
        },
        "ksat_id": "S0190",
        "parent": [],
        "description": "Demonstrate facilitative listening techniques.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e39"
        },
        "ksat_id": "S0192",
        "parent": [],
        "description": "Identify purpose or define strategy.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e40"
        },
        "ksat_id": "S0193",
        "parent": [],
        "description": "Plan a product release.",
        "topic": "Agile",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a80"
        },
        "ksat_id": "A0737",
        "description": "Demonstrate the ability to integrate individual tests, jobs, and/or scripts into testing "
                       "frameworks, tools, and CI pipelines.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a8f"
        },
        "ksat_id": "A0739",
        "description": "Demonstrate the ability to build and configure virtual machines, containers, and/or "
                       "hardware systems used as target devices/systems.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a93"
        },
        "ksat_id": "A0740",
        "description": "Demonstrate the ability to design and document operationally representative environments "
                       "consisting of multiple target devices and systems and necessary networking.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a91"
        },
        "ksat_id": "A0741",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Judge when it is appropriate to use a docker container instead of a virtual machine or bare"
                       " metal installation.",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32a92"
        },
        "ksat_id": "A0742",
        "parent": [],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Create a service in docker (http, database, etc.) with persistant storage",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32eb9"
        },
        "ksat_id": "S0334",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32dac"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "work-roles": [],
        "specializations": [],
        "description": "Carry out the steps used to register a runner in GitLab",
        "topic": "GitLab",
        "created_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d3f"
        },
        "ksat_id": "K0836",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a8f"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Summarize the purpose of Docker",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d3a"
        },
        "ksat_id": "K0833",
        "description": "Identify and describe proper data collection requirements for test scripts and plans.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d39"
        },
        "ksat_id": "K0830",
        "description": "Identify and understand how to utilize orchestration technologies and tools.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d38"
        },
        "ksat_id": "K0831",
        "description": "Identify and describe the differences between automation and orchestration.",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f00"
            }
        ],
        "topic": "Testing Process",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32cd4"
        },
        "ksat_id": "K0730",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a76"
            }
        ],
        "description": "Clarify how to configure a .gitlab-ci.yml file to cache files in a GitLab pipeline",
        "topic": "GitLab",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32d08"
        },
        "ksat_id": "K0766",
        "parent": [],
        "description": "Understand the purpose of a ROP gadget",
        "topic": "Vulnerability Research",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": ""
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e48"
        },
        "ksat_id": "S0204",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32f11"
            }
        ],
        "description": "Configure Ghidra client to use scripts.",
        "topic": "Vulnerability Research",
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "Make sure to cover installation and config of 90th scripts.",
        "updated_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e49"
        },
        "ksat_id": "S0206",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a8f"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Design a dockerfile that meets certain requirements",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e47"
        },
        "ksat_id": "S0207",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a8f"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Carry out the docker build command to build a container image",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e4a"
        },
        "ksat_id": "S0208",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a8f"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Carry out the docker run command to start a docker container",
        "topic": "Docker",
        "created_on": "2020-06-17"
    },
    {
        "_id": {
            "$oid": "5f457f1e1ea90ba9adb32e4b"
        },
        "ksat_id": "S0209",
        "parent": [
            {
                "$oid": "5f457f1e1ea90ba9adb32a8f"
            }
        ],
        "requirement_src": [],
        "requirement_owner": "",
        "comments": "",
        "description": "Carry out the docker exec command to interact with an already running container",
        "topic": "Docker",
        "created_on": "2020-06-17"
    }
]
rel_link_data = [
    #  Training Rel-links
    {
        "_id": {
            "$oid": "5ee92513580c86e868bd6f66"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e54"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/cyt/admin/docs/evaluations/SEE/training.html"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e56"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/cyt/admin/docs/evaluations/SEE/training.html"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e58"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/cyt/admin/docs/evaluations/SEE/training.html"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e5a"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/cyt/admin/docs/evaluations/SEE/training.html"
            }
        ],
        "course": "SEE",
        "module": "see-basics",
        "topic": "SEE 101",
        "subject": "see_training",
        "lecture_time": 60,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "SEE_Training",
        "network": "NIPR",
        "work-roles": [
            "SEE"
        ],
        "map_for": "training",
        "topic_path": "./",
        "created_on": "2020-06-16",
        "updated_on": "2020-06-23"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf3108"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d3d"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/pseudocode/index.html"
            }
        ],
        "course": "IDF",
        "module": "pseudocode",
        "topic": "Functions",
        "subject": "07_Functions",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_pseudocode",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf3109"
        },
        "KSATs": [],
        "course": "IDF",
        "module": "pseudocode",
        "topic": "Checkpoints",
        "subject": "03_Structure_Checkpoints",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_pseudocode",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf3106"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e29"
                },
                "item_proficiency": "2",
                "url": "https://90cos.gitlab.io/training/modules/pseudocode/index.html"
            }
        ],
        "course": "IDF",
        "module": "pseudocode",
        "topic": "Checkpoints",
        "subject": "05_Looping_Checkpoints",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_pseudocode",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30d7"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32aa0"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/python/python_features/index.html"
            }
        ],
        "course": "IDF",
        "module": "python",
        "topic": "python_features",
        "subject": "py2_py3",
        "lecture_time": 10,
        "perf_demo_time": 40,
        "references": [
            "https://wiki.python.org/moin/Python2orPython3?action=recall\u0026rev=96",
            "https://www.guru99.com/python-2-vs-python-3.html"
        ],
        "lesson_objectives": [
            {
                "description": "Identify code written for Python 2 or Python 3",
                "proficiency_level": "B",
                "KSATs": [
                    "K0023"
                ],
                "MSBs": []
            },
            {
                "description": "Describe execution of Python from the command line",
                "proficiency_level": "B",
                "KSATs": [
                    "K0514"
                ],
                "MSBs": []
            }
        ],
        "performance_objectives": [
            {
                "description": "Launch Python from the command line",
                "KSATs": []
            }
        ],
        "module_id": "BD_PY_FEATURES_PY2_PY3",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/python_features/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30e0"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b01"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/python/Algorithms/index.html"
            }
        ],
        "course": "IDF",
        "module": "python",
        "topic": "Algorithms",
        "subject": "BigONotation",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [
            "https://www.tutorialspoint.com/python_data_structure/python_big_o_notation.htm"
        ],
        "lesson_objectives": [
            {
                "description": "",
                "proficiency_level": "",
                "KSATs": [],
                "MSBs": []
            }
        ],
        "performance_objectives": [
            {
                "description": "",
                "KSATs": []
            }
        ],
        "module_id": "BD_PY_ALGO_BIGO",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Algorithms/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30d2"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a5c"
                },
                "item_proficiency": "2",
                "url": "https://90cos.gitlab.io/training/modules/python/advanced/index.html"
            }
        ],
        "course": "IDF",
        "module": "python",
        "topic": "Advanced",
        "subject": "metaclasses",
        "lecture_time": 30,
        "perf_demo_time": 60,
        "references": [
            "https://realpython.com/python-metaclasses/"
        ],
        "lesson_objectives": [
            {
                "description": "Understand metaclasses.",
                "proficiency_level": "B",
                "KSATs": [],
                "MSBs": [
                    {
                        "description": "Describe the purpose of metaclasses.",
                        "proficiency_level": "B",
                        "KSATs": []
                    },
                    {
                        "description": "Describe the benefits of metaclasses.",
                        "proficiency_level": "B",
                        "KSATs": []
                    },
                    {
                        "description": "Describe the purpose of the super() method.",
                        "proficiency_level": "B",
                        "KSATs": []
                    },
                    {
                        "description": "Describe the purpose of the new() method.",
                        "proficiency_level": "B",
                        "KSATs": []
                    }
                ]
            }
        ],
        "performance_objectives": [
            {
                "description": "Given a set of requirements, implement a metaclass.",
                "KSATs": [
                    "A0563"
                ]
            }
        ],
        "module_id": "BD_PY_ADVNCD_MCLASS",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/advanced/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30d0"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32aa8"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/python/advanced/index.html"
            }
        ],
        "course": "IDF",
        "module": "python",
        "topic": "Advanced",
        "subject": "ctypes",
        "lecture_time": 45,
        "perf_demo_time": 90,
        "references": [
            "https://docs.python.org/3/library/ctypes.html",
            "https://pythonpedia.com/en/tutorial/9050/ctypes"
        ],
        "lesson_objectives": [
            {
                "description": "Employ C-Types.",
                "proficiency_level": "B",
                "KSATs": [],
                "MSBs": [
                    {
                        "description": "Describe the purpose of C-Types.",
                        "proficiency_level": "B",
                        "KSATs": [
                            "K0029"
                        ]
                    },
                    {
                        "description": "Differentiate between ctypes.CDLL and ctypes.PyDLL.",
                        "proficiency_level": "B",
                        "KSATs": [
                            "K0029"
                        ]
                    },
                    {
                        "description": "Describe the load library function.",
                        "proficiency_level": "B",
                        "KSATs": [
                            "K0029"
                        ]
                    },
                    {
                        "description": "Describe the of purpose of structures.",
                        "proficiency_level": "B",
                        "KSATs": [
                            "K0029"
                        ]
                    }
                ]
            }
        ],
        "performance_objectives": [
            {
                "description": "Create a structure to pass to an imported CDLL.",
                "KSATs": []
            },
            {
                "description": "Import a C library (.DLL/.SO) to utilize a C compiled function.",
                "KSATs": []
            }
        ],
        "module_id": "BD_PY_ADVNCD_CTYPE",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/advanced/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30a6"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Control_flow/index"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cee"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Control_flow/index.html"
            }
        ],
        "course": "IDF",
        "module": "c-programming",
        "topic": "Control_flow",
        "subject": "Switch",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_C_Switch",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Control_flow/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30b1"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b07"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Introduction/index.html"
            }
        ],
        "course": "IDF",
        "module": "c-programming",
        "topic": "Introduction",
        "subject": "gcc",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "BD_C_Introduction",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Introduction/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30a7"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Control_flow/index.html"
            }
        ],
        "course": "IDF",
        "module": "c-programming",
        "topic": "Control_flow",
        "subject": "Loops",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_C_Loops",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Control_flow/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30a8"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Control_flow/index.html"
            }
        ],
        "course": "IDF",
        "module": "c-programming",
        "topic": "Control_flow",
        "subject": "If_else",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_C_If_Else",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Control_flow/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30b0"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Introduction/index.html"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32eea"
                },
                "item_proficiency": " ",
                "url": "https://90cos.gitlab.io/training/modules/C-Programming/Introduction/index.html"
            }
        ],
        "course": "IDF",
        "module": "c-programming",
        "topic": "Introduction",
        "subject": "Definitions",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "BD_C_Definitions",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/Introduction/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf30d3"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a5a"
                },
                "item_proficiency": "1",
                "url": "https://90cos.gitlab.io/training/modules/python/advanced/index.html"
            }
        ],
        "course": "IDF",
        "module": "python",
        "topic": "Advanced",
        "subject": "multithreading",
        "lecture_time": 20,
        "perf_demo_time": 40,
        "references": [
            "https://docs.python.org/3/library/threading.html",
            "https://realpython.com/intro-to-python-threading/"
        ],
        "lesson_objectives": [
            {
                "description": "Describe multithreading.",
                "proficiency_level": "B",
                "KSATs": [],
                "MSBs": [
                    {
                        "description": "Describe the purpose of multithreading.",
                        "proficiency_level": "B",
                        "KSATs": []
                    },
                    {
                        "description": "Describe the benefits of multithreading.",
                        "proficiency_level": "B",
                        "KSATs": []
                    },
                    {
                        "description": "Identify the Python library methods used for threading.",
                        "proficiency_level": "A",
                        "KSATs": []
                    }
                ]
            }
        ],
        "performance_objectives": [
            {
                "description": "Utilize the Thread library to implement a simple multithreaded program.",
                "KSATs": [
                    "A0564"
                ]
            }
        ],
        "module_id": "BD_PY_ADVNCD_MTHREAD",
        "network": "COMMERCIAL",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/advanced/",
        "created_on": "2020-06-16"
    },
    {
        "_id": {
            "$oid": "5ee9250a761cf87371cf3105"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a35"
                },
                "item_proficiency": "2",
                "url": "https://90cos.gitlab.io/training/modules/pseudocode/index.html"
            }
        ],
        "course": "IDF",
        "module": "pseudocode",
        "topic": "Input_Validation",
        "subject": "08_Input_Validation_Practice",
        "lecture_time": 0,
        "perf_demo_time": 0,
        "references": [],
        "lesson_objectives": [],
        "performance_objectives": [],
        "module_id": "IDF_pseudocode",
        "network": "Commercial",
        "work-roles": [
            "CCD"
        ],
        "map_for": "training",
        "topic_path": "mdbook/src/",
        "created_on": "2020-06-16"
    },

    # Eval Rel-links
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf315e"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b01"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                       "ccd-master-question-file/-/tree/master/knowledge/Algorithms/algorithms-big-O/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0001",
        "network": "commercial",
        "question_name": "algorithms-big-O",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf315f"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ac4"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                       "ccd-master-question-file/-/tree/master/knowledge/Algorithms/algorithms-breadth-first/"
                       "README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0009",
        "network": "commercial",
        "question_name": "breadth-first",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3163"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ac3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                       "ccd-master-question-file/-/tree/master/knowledge/Algorithms/algorithms-depth-first/"
                       "README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0008",
        "network": "commercial",
        "question_name": "depth-first",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3160"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b05"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/"
                       "ccd-master-question-file/-/tree/master/knowledge/Algorithms/algorithms-merge-sort/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0005",
        "network": "commercial",
        "question_name": "merge-sort",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3161"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b02"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Algorithms/algorithms-heap-sort/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0003",
        "network": "commercial",
        "question_name": "heap-sort",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3164"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32afe"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Algorithms/algorithms-insertion-sort/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0004",
        "network": "commercial",
        "question_name": "insertion-sort",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3162"
        },
        "topic": "Algorithms",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32aff"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Algorithms/algorithms-selection-sort/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ALG_0007",
        "network": "commercial",
        "question_name": "selection-sort",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3181"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b41"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b4d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b3d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_2/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0003",
        "network": "commercial",
        "question_name": "assembly-arithmetic-instructions_2",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf316a"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b41"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-basics-and-memory_4/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b4d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-basics-and-memory_4/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-basics-and-memory_4/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0011",
        "network": "commercial",
        "question_name": "assembly-basics-and-memory_4",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3178"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b41"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-bitwise_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b4d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-bitwise_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b3d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-bitwise_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-bitwise_2/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 1,
        "attempts": 2,
        "passes": 2,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0029",
        "network": "commercial",
        "question_name": "assembly-negative-bitwise_2",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3185"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b41"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-numbers-and-bitwise_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b4d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-numbers-and-bitwise_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-numbers-and-bitwise_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b3d"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-negative-numbers-and-bitwise_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0031",
        "network": "commercial",
        "question_name": "assembly-negative-numbers-and-bitwise_1",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf316f"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b38"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_8/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_8/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d0b"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-arithmetic-instructions_8/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0044",
        "network": "commercial",
        "question_name": "assembly-arithmetic-instructions_8",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-05-29 08:38:00",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3177"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-creating-asm_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c3a"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-creating-asm_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0021",
        "network": "commercial",
        "question_name": "assembly-creating-asm_1",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3174"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32b38"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-instructions-movzx/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cca"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-instructions-movzx/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cfd"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Assembly/assembly-instructions-movzx/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_ASM_0045",
        "network": "commercial",
        "question_name": "assembly-instructions-movzx",
        "language": "x86/x86_64",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-05-29 08:38:00",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31d1"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae9"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-arithmetic-operators_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ccd"
                },
                "item_proficiency": "C",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-arithmetic-operators_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cec"
                },
                "item_proficiency": "C",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-arithmetic-operators_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-arithmetic-operators_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0002",
        "network": "commercial",
        "question_name": "c-arithmetic-operators_1",
        "language": "C89",
        "question_proficiency": "C",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31fa"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ab6"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-group_1/c-functions_2/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-group_1/c-functions_2/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 1,
        "attempts": 2,
        "passes": 2,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0025",
        "network": "commercial",
        "question_name": "c-functions_2",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31f1"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32aee"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-group_1/c-pointers_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-group_1/c-pointers_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0032",
        "network": "commercial",
        "question_name": "c-pointers_1",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31c2"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae4"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-main-function/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-main-function/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0041",
        "network": "commercial",
        "question_name": "c-main-function",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31bd"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cec"
                },
                "item_proficiency": "C",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-bit-shift-left/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "b",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-bit-shift-left/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0007",
        "network": "commercial",
        "question_name": "c-bit-shift-left",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31de"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae9"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-data-types_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-data-types_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0011",
        "network": "commercial",
        "question_name": "c-data-types_1",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31bf"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cec"
                },
                "item_proficiency": "C",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-stack-memory/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cdc"
                },
                "item_proficiency": "C",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/C_Programming/c-stack-memory/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_C_0054",
        "network": "commercial",
        "question_name": "c_stack_memory",
        "language": "C89",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b6"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ad0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structure-pitfalls/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structure-pitfalls/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0001",
        "network": "commercial",
        "question_name": "pitfalls",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b7"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-circularly-linked-lists/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0002",
        "network": "commercial",
        "question_name": "circularly-linked-list",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b4"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ace"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-doubly-linked-lists/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-doubly-linked-lists/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0003",
        "network": "commercial",
        "question_name": "doubly-linked-list",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31ba"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-weighted-graphs/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0004",
        "network": "commercial",
        "question_name": "weighted-graphs",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b5"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structure-queue/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ad2"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structure-queue/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0005",
        "network": "commercial",
        "question_name": "queue",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b8"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-stack/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ad2"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-stack/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0006",
        "network": "commercial",
        "question_name": "stack",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b9"
        },
        "topic": "Data Structures",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cc0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Data_Structures/data-structures-tree/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_DSTRUC_0007",
        "network": "commercial",
        "question_name": "tree",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31b3"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-dhcp_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c72"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-dhcp_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c73"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-dhcp_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c76"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-dhcp_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 1,
        "attempts": 2,
        "passes": 2,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0005",
        "network": "commercial",
        "question_name": "networking-dhcp_1",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf319f"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ade"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-ipv4-addresses_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0009",
        "network": "commercial",
        "question_name": "networking-ipv4-addresses_1",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-16 17:03:12",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31ad"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ad6"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-networks-and-ports_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c70"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-networks-and-ports_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0015",
        "network": "commercial",
        "question_name": "networking-networks-and-ports_1",
        "language": "",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf31a1"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32c77"
                },
                "item_proficiency": "A",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-rfc/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0038",
        "network": "commercial",
        "question_name": "rfc",
        "language": "",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5f088587ef8faf28b78d3b2d"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae0"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-CIDR/README.md"
            }
        ],
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0040",
        "question_name": "networking-CIDR",
        "question_proficiency": "B",
        "network": "NIPR",
        "topic": "Networking",
        "language": "",
        "complexity": 1,
        "estimated_time_to_complete": 1,
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval",
        "created_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5f088587ef8faf28b78d3b2e"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ae1"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-NAT/README.md"
            }
        ],
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0041",
        "question_name": "networking-NAT",
        "question_proficiency": "B",
        "network": "NIPR",
        "topic": "Networking",
        "language": "",
        "complexity": 1,
        "estimated_time_to_complete": 1,
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval",
        "created_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf319e"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32ad6"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Networking/networking-udp/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_NET_0032",
        "network": "commercial",
        "question_name": "networking-udp",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 1,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf321d"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32abc"
                },
                "item_proficiency": "A",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-class-attributes/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-class-attributes/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0100",
        "network": "commercial",
        "question_name": "class-attributes",
        "language": "3.7",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3213"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32abe"
                },
                "item_proficiency": "A",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-factory-design/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-factory-design/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0103",
        "network": "commercial",
        "question_name": "knowledge_Python_python-factory-pattern_factory_pattern",
        "language": "3.7",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:27:47",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf322c"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32aba"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_1/dict_1/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_1/dict_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 1,
        "attempts": 2,
        "passes": 2,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0023",
        "network": "commercial",
        "question_name": "dict_1",
        "language": "3.7",
        "question_proficiency": "B",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3246"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_2/object/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0106",
        "network": "commercial",
        "question_name": "object",
        "language": "3.7",
        "question_proficiency": "C",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:08:10",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3227"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_1/lists_7/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0020",
        "network": "commercial",
        "question_name": "lists_7",
        "language": "3.7",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 19:43:08",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3249"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32abc"
                },
                "item_proficiency": "A",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_2/attribute/README.md"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-group_2/attribute/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0103",
        "network": "commercial",
        "question_name": "attribute",
        "language": "3.7",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:08:10",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3221"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32cb3"
                },
                "item_proficiency": "B",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/knowledge/Python/python-oop_1/README.md"
            }
        ],
        "complexity": 1,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "knowledge",
        "question_id": "BD_PY_0040",
        "network": "commercial",
        "question_name": "python-oop_1",
        "language": "3.7",
        "question_proficiency": "A",
        "estimated_time_to_complete": 2.25,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-23 08:54:35",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3266"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de7"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/character_replace/character_replace.asm"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d18"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/character_replace/character_replace.asm"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dee"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/character_replace/character_replace.asm"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/character_replace/character_replace.asm"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/character_replace/character_replace.asm"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0037",
        "network": "commercial",
        "question_name": "character_replace",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3265"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de7"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/mem_move/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dee"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/mem_move/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/mem_move/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/mem_move/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dff"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/mem_move/testfile.S"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0039",
        "network": "commercial",
        "question_name": "mem_move",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3263"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de7"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dee"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dff"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32def"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e2f"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/power_of_two/testfile.S"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0040",
        "network": "commercial",
        "question_name": "power_of_two",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3264"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de7"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d18"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dee"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dff"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e2f"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/reverse_string/testfile.S"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0041",
        "network": "commercial",
        "question_name": "reverse_string",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3262"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/add_values/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/add_values/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e1c"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/add_values/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32def"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/add_values/testfile.S"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0036",
        "network": "commercial",
        "question_name": "add_values",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-23 08:54:35",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3267"
        },
        "topic": "Assembly",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de7"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d18"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dee"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e21"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e20"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dff"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e1c"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e2f"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Assembly/count_characters/testfile.S"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_ASM_0038",
        "network": "commercial",
        "question_name": "count_characters",
        "language": "x86/x86_64",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf3281"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_binary_search_tree/TestCode.c"
            }
        ],
        "complexity": 3,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0067",
        "network": "commercial",
        "question_name": "c_binary_search_tree",
        "language": "C89",
        "question_proficiency": "2",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf3280"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming_Converted/c_circularly_linked_lists/TestCode.c"
            }
        ],
        "complexity": 3,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0109",
        "network": "commercial",
        "question_name": "c_circularly_linked_lists",
        "language": "89",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3272"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/grades/TestCode.c"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0082",
        "network": "commercial",
        "question_name": "grades",
        "language": "C89",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3276"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/string_copying/TestCode.c"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0100",
        "network": "commercial",
        "question_name": "string_copying",
        "language": "C89",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf326d"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a3a"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/bubble_sort/TestCode.c"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0070",
        "network": "commercial",
        "question_name": "bubble_sort",
        "language": "C89",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf3299"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32de4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/files/TestCode.c"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0078",
        "network": "commercial",
        "question_name": "files",
        "language": "C89",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf328f"
        },
        "topic": "C Programming",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d9b"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dda"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/C_Programming/reverse_bits/TestCode.c"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_C_0096",
        "network": "commercial",
        "question_name": "reverse_bits",
        "language": "C89",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf326a"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfb"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a6d"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d93"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df8"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Crypto_Sockets/client.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_NET_0037",
        "network": "commercial",
        "question_name": "Crypto_Sockets",
        "language": "",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:24:31",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3269"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/KeyServ_REST/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/KeyServ_REST/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfb"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/KeyServ_REST/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a6d"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/KeyServ_REST/client.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_NET_0034",
        "network": "commercial",
        "question_name": "KeyServ_REST",
        "language": "",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf326b"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Socket_Key_Match/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Socket_Key_Match/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfb"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Socket_Key_Match/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a6d"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Socket_Key_Match/client.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_NET_0035",
        "network": "commercial",
        "question_name": "Socket_Key_Match",
        "language": "",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250b761cf87371cf3268"
        },
        "topic": "Networking",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/TCP_Modes/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/TCP_Modes/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfb"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/TCP_Modes/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a6d"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/TCP_Modes/client.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_NET_0036",
        "network": "commercial",
        "question_name": "TCP_Modes",
        "language": "",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:05",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5f08c70d645a2e409006d908"
        },
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Send_Data/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Send_Data/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfb"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Send_Data/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a36"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Send_Data/client.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a6d"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Networking/Send_Data/client.py"
            }
        ],
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_NET_0042",
        "question_name": "Send_Data",
        "question_proficiency": "3",
        "network": "NIPR",
        "topic": "Networking",
        "language": "",
        "complexity": 1,
        "estimated_time_to_complete": 60,
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval",
        "created_on": "2020-07-10"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32c5"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df8"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a3c"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/binary_to_decimal/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0058",
        "network": "commercial",
        "question_name": "binary_to_decimal",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf329f"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d95"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/change_for_snacks/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0061",
        "network": "commercial",
        "question_name": "change_for_snacks",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32c1"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df8"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/exponential_euler/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0075",
        "network": "commercial",
        "question_name": "exponential_euler",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32a8"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/reverse_string_and_characters/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/reverse_string_and_characters/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d95"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/reverse_string_and_characters/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/reverse_string_and_characters/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/reverse_string_and_characters/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0093",
        "network": "commercial",
        "question_name": "reverse_string_and_characters",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32c7"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d95"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d93"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/IP_port_match/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0079",
        "network": "commercial",
        "question_name": "IP_port_match",
        "language": "3.7",
        "question_proficiency": "3",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf329e"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d98"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d95"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/perfect_num/testfile.py"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0086",
        "network": "commercial",
        "question_name": "perfect_num",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32cb"
        },
        "topic": "Python",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a4b"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a49"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32d95"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32df2"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32dfa"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32da4"
                },
                "item_proficiency": "3",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Python/python_queue/testfile.py"
            }
        ],
        "complexity": 3,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_PY_0089",
        "network": "commercial",
        "question_name": "python_queue",
        "language": "3.7",
        "question_proficiency": "3c",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-04-03 13:36:06",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32cd"
        },
        "topic": "Reverse Engineering",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a42"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a69"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a66"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e31"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e33"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_RE_0001",
        "network": "commercial",
        "question_name": "NOSCHEMABasic_MagicNum",
        "language": "",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    },
    {
        "_id": {
            "$oid": "5ee9250c761cf87371cf32cc"
        },
        "topic": "Reverse Engineering",
        "KSATs": [
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a42"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a69"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32a66"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e31"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            },
            {
                "ksat_id": {
                    "$oid": "5f457f1e1ea90ba9adb32e33"
                },
                "item_proficiency": "2",
                "url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/"
                       "tree/master/performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            }
        ],
        "complexity": 2,
        "revision_number": 1,
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "test_id": "Basic-Dev",
        "test_type": "performance",
        "question_id": "BD_RE_0002",
        "network": "commercial",
        "question_name": "NOSCHEMABasic_MysteryArg",
        "language": "",
        "question_proficiency": "2b",
        "estimated_time_to_complete": 96,
        "created_on": "2020-06-16",
        "updated_on": "2020-03-13 23:02:40",
        "work-roles": [
            "CCD"
        ],
        "map_for": "eval"
    }
]
