class Question:
    def __init__(self, _id: str = "", rellink_id: str = "", question_name: str = "", question_path: str = "",
                 question_type: str = "", topic: str = "", disabled: bool = True, provisioned: int = 0,
                 attempts: int = 0, passes: int = 0, fails: int = 0):
        self._id = _id  # The unique identifier
        self.rellink_id = rellink_id  # The OID of the question's rel-link in the MTTL
        self.question_name = question_name  # The name of the question's JSON file
        self.question_path = question_path  # The path of the question's JSON file
        self.question_type = question_type  # An identifier designating if this is a knowledge or performance question
        self.topic = topic  # The name of the question's topic
        self.disabled = disabled  # Determines if this question is allowed to be used during exam auto generation
        self.provisioned = provisioned  # Keeps track of the number of times this question is selected for an exam
        self.attempts = attempts  # Keeps track of the number of times this question is attempted
        self.passes = passes  # Keeps track of the number of times this question was answered correctly
        self.failures = fails  # Keeps track of the number of times this question was answered incorrectly

    def to_dict(self) -> dict:
        return {
            "_id": self._id,
            "rel-link_id": self.rellink_id,
            "question_name": self.question_name,
            "question_path": self.question_path,
            "question_type": self.question_type,
            "topic": self.topic,
            "disabled": self.disabled,
            "provisioned": self.provisioned,
            "attempts": self.attempts,
            "passes": self.passes,
            "failures": self.failures
        }


class Knowledge(Question):
    def __init__(self, _id: str = "", rellink_id: str = "", question_name: str = "", question_path: str = "",
                 question_type: str = "", topic: str = "", disabled: bool = True, provisioned: int = 0,
                 attempts: int = 0, passes: int = 0, fails: int = 0, quest: str = "", snippet: str = None,
                 snippet_lang: str = None, choices: list = None, answer: int = -1, explanation: list = None,
                 grp_name: str = None, grp_questions: list = None):
        super().__init__(_id, rellink_id, question_name, question_path, question_type, topic,
                         disabled, provisioned, attempts, passes, fails)
        self.question = quest  # Write out the question
        if snippet is not None and snippet.strip() != "":
            self.snippet = snippet  # String representation of the snippet's image
            if snippet_lang is not None:
                self.snippet_lang = snippet_lang.strip()
            else:
                self.snippet_lang = ""
        self.choices = []  # The multiple choice options
        if choices is not None:
            self.choices = choices
        self.answer = answer  # The index of choices indicating the correct answer
        self.explanation = []  # Why each choice is correct or incorrect, coincides with choices index
        if explanation is not None:
            self.explanation = explanation
        # If this is a group this will exist
        # The key called name ID's the group's name; this name will be used in each question's name
        # The key called questions is a list of question UID's applicable to this group
        if grp_name is not None and grp_questions is not None:
            self.group = {
                "group_name": grp_name,
                "questions": grp_questions
            }
        elif (grp_name is None and grp_questions is not None) or (grp_name is not None and grp_questions is None):
            raise ValueError("To Create a group question, both the group name and the question list must be specified.")

    def to_dict(self) -> dict:
        knowledge_dict = Question.to_dict(self)
        knowledge_dict.update({
            "question": self.question,
        })
        if hasattr(self, "snippet"):
            knowledge_dict["snippet"] = self.snippet
        if hasattr(self, "snippet_lang"):
            knowledge_dict["snippet_lang"] = self.snippet_lang
        knowledge_dict.update({
            "choices": self.choices,
            "answer": self.answer,
            "explanation": self.explanation
        })
        if hasattr(self, "group"):
            knowledge_dict["group"] = self.group
        return knowledge_dict


class PerformanceDev(Question):
    def __init__(self, _id: str = "", rellink_id: str = "", question_name: str = "", question_path: str = "",
                 question_type: str = "", topic: str = "", disabled: bool = True, provisioned: int = 0,
                 attempts: int = 0, passes: int = 0, fails: int = 0, quest_filepath: str = "",
                 quest_headerpath: str = "", sup_files: list = None, tests: list = None, solution: str = "",
                 dep_libs: list = None, dep_req_soft: list = None):
        super().__init__(_id, rellink_id, question_name, question_path, question_type, topic,
                         disabled, provisioned, attempts, passes, fails)
        self.question = {
            "filepath": quest_filepath,  # URL to examinee source file
        }
        if quest_headerpath is not None and quest_headerpath.strip() != "":
            self.question["headerpath"] = quest_headerpath
        if not self.has_valid_source_code_dict(self.question):
            msg = "The question must have a filepath if there's a headerpath and neither can be blank."
            raise ValueError(msg)
        self.support_files = []
        if sup_files is not None:
            self.support_files = sup_files
        self.tests = []
        if tests is not None:
            for test in tests:
                if type(test) is dict:
                    if self.has_valid_source_code_dict(test):
                        if "filepath" in test and "headerpath" in test:
                            self.tests.append({
                                "filepath": test["filepath"],  # URL to test source file
                                "headerpath": test["headerpath"]  # URL to test header file
                            })
                        elif "filepath" in test:
                            self.tests.append({
                                "filepath": test["filepath"]
                            })
                    else:
                        msg = "Each test must have a filepath if there's a headerpath and neither can be blank."
                        raise ValueError(msg)
                else:
                    raise ValueError("Test objects must be a dictionary")
        self.solution = solution  # URL to solution file
        self.dependencies = {
            "libraries": [],
            "req_software": []
        }
        if dep_libs is not None:
            self.dependencies["libraries"] = dep_libs
        if dep_req_soft is not None:
            self.dependencies["req_software"] = dep_req_soft

    def to_dict(self):
        performance_dict = Question.to_dict(self)
        performance_dict.update({
            "question": self.question,
            "support_files": self.support_files,
            "tests": self.tests,
            "solution": self.solution,
            "dependencies": self.dependencies
        })
        return performance_dict

    @staticmethod
    def has_valid_source_code_dict(source_code_paths: dict):
        if ("headerpath" in source_code_paths and source_code_paths["headerpath"].strip() == "") or \
                ("headerpath" in source_code_paths and "filepath" not in source_code_paths) or \
                ("headerpath" in source_code_paths and source_code_paths["filepath"].strip() == ""):
            return False
        return True


class PerformanceNonDev(Question):
    def __init__(self, _id: str = "", rellink_id: str = "", question_name: str = "", question_path: str = "",
                 question_type: str = "", topic: str = "", disabled: bool = True, provisioned: int = 0,
                 attempts: int = 0, passes: int = 0, fails: int = 0, question: str = "", sup_files: list = None,
                 solution: str = ""):
        super().__init__(_id, rellink_id, question_name, question_path, question_type, topic,
                         disabled, provisioned, attempts, passes, fails)
        self.question = question
        self.support_files = []
        if sup_files is not None:
            self.support_files = sup_files
        self.solution = solution  # URL to solution file

    def to_dict(self):
        performance_dict = Question.to_dict(self)
        performance_dict.update({
            "question": self.question,
            "support_files": self.support_files,
            "solution": self.solution
        })
        return performance_dict
