import os
from colorama import Fore
from lib.default_strings import error_str


def escape_chars_for_md(string: str) -> str:
    escape_chars = ["\\", "`", "*", "_", "{", "}", "[", "]", "(", ")", "#", "+", "-", ".", "!"]

    # Escape all characters with special meaning
    for character in escape_chars:
        string = string.replace(character, "\\{0}".format(character))

    # Now ensure the pipe is escaped for a md table
    string = string.replace("|", "&#124;")

    return string


def get_justification(justification: str = ''):
    '''
    Purpose: This determines the justification markdown string to use and returns the proper string. If an invalid
    justification is entered then this will be processed as if an empty string were entered.
    :param justification: The cell justification; values are 'left', 'center', 'right' or '' (for none). Default = ''
    :return: cell_justification = :---, :---:, ---:, or ---.
    '''
    # Set the cell text justification format
    if "left" == justification.lower():
        # In case it should be left justified
        cell_justification = ":---"
    elif "center" == justification.lower():
        # In case it should be center justified
        cell_justification = ":---:"
    elif "right" == justification.lower():
        # In case it should be right justified
        cell_justification = "---:"
    else:
        # In case no justification is requested or an invalid entry is detected set to no justification
        cell_justification = "---"
    return cell_justification


def set_md_table_cell_justify(justification: str = "left", num_cols: int = 4, custom: list = None) -> str or int:
    '''
    Purpose: This will create the horizontal line that goes between rows in a markdown table. When entering an empty
    custom list of cell format strings, all cells will have the same justification (according to the justification
    parameter's value); note, the default value of None has the same affect. Otherwise, it uses the formatting in the
    custom list. If an invalid justification string is entered, then no justification is used (i.e. instead of :---,
    :---:, or ---: only --- will be used). To specify no justification, enter an empty string (i.e. '').
    :param num_cols: The number of columns in the desired markdown table. The default setting is 4.
    :param justification: The desired cell justification. Valid values are 'left', 'center', 'right', or '' (for none).
                          The default setting is left.
    :param custom: A list of custom cell formatting settings; an empty list sets everything to the same value according
                   to the justification parameter and invalid values are interpreted as no formatting (i.e. uses ---).
                   The default setting is None, which has the same behavior as an empty list.
    :return: 1 = Invalid value for num_cols; table_row_line = Success; the markdown horizontal line is returned
    '''
    # The horizontal line between each row in a markdown table
    table_row_line = ""
    # The current column's index
    index = 0
    col_format = []

    if custom is None:
        # In case the custom parameter wasn't set, make it an empty list
        custom = []

    # Ensure the requested number of columns is valid
    if num_cols <= 0:
        # In case the requested number of columns is zero or less, stop processing
        print("{0} {1}: Invalid number of columns; please enter a number greater than zero.".format(
            Fore.RED + os.path.basename(__file__) + Fore.RESET,
            Fore.RED + error_str + Fore.RESET
        ))
        return 1

    for item in custom:
        # Ensure the list of custom cell formatting is valid
        if ":---" != item and ":---:" != item and "---:" != item and "---" != item:
            # In case the value is not a valid cell format, try to convert to the desired format
            col_format.append(get_justification(item))
        else:
            # In case this is a valid cell format
            col_format.append(item)

    # Build the table row horizontal line with the requested justification
    if len(custom) == 0:
        # In case all columns should use the same justification
        while index < num_cols:
            # Set all cells to the requested cell format
            if len(custom) == 0:
                table_row_line += "| {0} ".format(get_justification(justification))
            index += 1

        # Add the trailing vertical line
        table_row_line += "|"
    else:
        # In case a custom set of cell formatting is desired, i.e. they're not all the same
        table_row_line = "| {0} |".format(" | ".join(col_format))

    return table_row_line
