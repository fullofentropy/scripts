import sys
import os
import argparse
import json
from bson import json_util
import unittest
sys.path.append("scripts")
import gen_trng_survey_csvs
from lib import mttl_db_helpers, file_list_generator
from lib.unit_test_helpers import fake_mttl_mongo_db
from lib.default_strings import ksats_id_key

test_dir = "test_trng_survey_files"


class InputArgs(argparse.Namespace):
    # This is a mock argparse object used for testing purposes only.
    # This prevents argparse collisions in certain modules.
    def __init__(self, c_headers):
        self.repo_name = "see-basics"
        self.Host = mttl_db_helpers.HOST_NAME
        self.Port = mttl_db_helpers.PORT_NUM
        self.column_headers = c_headers
        self.db_name ="mttl_unit_test"
        self.log_file ="GenTrngSurveyReport.json"
        self.mttl_dir = test_dir
        self.nameof_csv_base ="trng_surveys.csv"
        self.out_dir =os.path.join(test_dir, "gen_survey_files")


class GenTrngSurveyCsvsTests(unittest.TestCase):
    lt_fifteen = [
        "K0753",
        "K0133",
        "K0134",
        "K0135",
        "K0136",
        "K0137",
        "K0132",
        "K0024",
        "A0087",
        "A0093"
    ]
    fifteen = [
        "K0753",
        "K0133",
        "K0134",
        "K0135",
        "K0136",
        "K0137",
        "K0132",
        "K0024",
        "A0087",
        "A0093",
        "S0753",
        "S0133",
        "S0134",
        "T0135",
        "T0136"
    ]
    gt_fifteen = [
        "K0753",
        "K0133",
        "K0134",
        "K0135",
        "K0136",
        "K0137",
        "K0132",
        "K0024",
        "A0087",
        "A0093",
        "S0753",
        "S0133",
        "S0134",
        "T0135",
        "T0136",
        "A0753",
        "A0133",
        "A0134",
        "A0135",
        "A0136",
        "A0137",
        "A0132",
        "A0024",
        "K0087",
        "K0093",
        "T0753",
        "T0133",
        "T0134",
        "S0135",
        "S0136"
    ]
    k0753 = {
        "5f457f1e1ea90ba9adb32cef": {
            "_id": {
                "$oid": "5f457f1e1ea90ba9adb32cef"
            },
            "ksat_id": "K0753",
            "parent": [
                {
                    "$oid": "5f457f1e1ea90ba9adb32dcc"
                },
                {
                    "$oid": "5f457f1e1ea90ba9adb32dc5"
                },
                {
                    "$oid": "5f457f1e1ea90ba9adb32dc0"
                }
            ],
            "description": "Describe how to implement and use a Tree",
            "topic": "Data Structures",
            "requirement_src": [],
            "requirement_owner": "",
            "comments": "a general tree (other than a Binary Search Tree)",
            "updated_on": "2020-06-16"
        }
    }
    mongodb = None
    @classmethod
    def setUpClass(cls) -> None:
        cls.mongodb = fake_mttl_mongo_db.FakeMttlDb(test_dir)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.mongodb.delete_db()

    def setUp(self) -> None:
        self.col_headers = [
            "rel-link_path",
            "Module",
            "Topic",
            "Subject",
            "work-role",
            "ksat_id",
            "description",
            "0",
            "I",
            "II",
            "III",
            "IV",
            "Other"
        ]
        # Establish the questions used
        self.zero_quest = "I don't remember this concept being taught"
        self.knowl_options = [
            "I was taught a basic understanding of this concept",
            "I was taught a solid understanding of this concept",
            "I was taught an excellent understanding of this concept",
            "After this training, I feel like I could teach this concept to others"
        ]
        self.perf_options = [
            "After this training, I would need help with this concept in practice",
            "After this training, I can do some of this concept in practice",
            "After this training, I can do most of this concept in practice",
            "After this training, I can do all of this concept in practice"
        ]

    def test_get_rand_invalid_ksat(self):
        # Get work-role and KSAT maps
        wkroles_map = mttl_db_helpers.get_only_mapped_wkrole_ksat_prof_maps(self.mongodb.db)["CCD"]
        ksats_map = mttl_db_helpers.get_all_requirements(self.mongodb.db)
        ksat = gen_trng_survey_csvs.get_random_invalid_ksat(ksats_map, list(wkroles_map.keys()))
        self.assertRegex(ksat[ksats_id_key], r"[KSAT][0-9]{4,}", "Returned value not a KSAT ID")
        self.assertNotIn(ksat[ksats_id_key], wkroles_map, "Chosen KSAT shouldn't be a work-role KSAT")

    def test_calc_needed_invalid_ksats_empty_list(self):
        expected = 0
        self.assertEqual(expected, gen_trng_survey_csvs.calc_needed_invalid_ksats([]),
                         "Mismatch in chosen number of invalid KSATs")

    def test_calc_needed_invalid_ksats_lt_15(self):
        expected = 1
        self.assertEqual(expected, gen_trng_survey_csvs.calc_needed_invalid_ksats(self.lt_fifteen),
                         "Mismatch in chosen number of invalid KSATs")

    def test_calc_needed_invalid_ksats_15(self):
        expected = 2
        self.assertEqual(expected, gen_trng_survey_csvs.calc_needed_invalid_ksats(self.fifteen),
                         "Mismatch in chosen number of invalid KSATs")

    def test_calc_needed_invalid_ksats_gt_15(self):
        expected = 3
        self.assertEqual(expected, gen_trng_survey_csvs.calc_needed_invalid_ksats(self.gt_fifteen),
                         "Mismatch in chosen number of invalid KSATs")

    def test_get_question_options_knowledge(self):
        self.assertListEqual(self.knowl_options, gen_trng_survey_csvs.get_question_options("K1234"),
                             "Mismatch in returned KSAT question options list")

    def test_get_question_options_ability(self):
        self.assertListEqual(self.perf_options, gen_trng_survey_csvs.get_question_options("A1234"),
                             "Mismatch in returned KSAT question options list")

    def test_get_question_options_skill(self):
        self.assertListEqual(self.perf_options, gen_trng_survey_csvs.get_question_options("S1234"),
                             "Mismatch in returned KSAT question options list")

    def test_get_question_options_task(self):
        self.assertListEqual(self.perf_options, gen_trng_survey_csvs.get_question_options("T1234"),
                             "Mismatch in returned KSAT question options list")

    def test_get_survey_row(self):
        path = "Algorithms/alg_lsn.rel-link.json"
        module = "python"
        topic = "Algorithms"
        subject = "alg_lsn"
        ksat = "K0753"
        ksat_oid = "5f457f1e1ea90ba9adb32cef"
        role = "basic-dev"
        ksats_map = json_util.loads(json.dumps(self.k0753))  # Makes the file as seen in Mongo
        ksat_desc = ksats_map[ksat_oid]["description"]
        expected = f"{path},{module},{topic},{subject},{role},{ksat},\"{ksat_desc}\",\"{self.zero_quest}\""
        expected += ",\"I was taught a basic understanding of this concept\"," \
                    "\"I was taught a solid understanding of this concept\"," \
                    "\"I was taught an excellent understanding of this concept\"," \
                    "\"After this training, I feel like I could teach this concept to others\",\n"
        self.assertEqual(expected,
                         gen_trng_survey_csvs.get_survey_row(path, module, topic, subject, role, ksats_map[ksat_oid]),
                         "Mismatch in returned CSV formatted string")

    def test_build_survey(self):
        # Get a rel-link object for testing and convert to an object that would come from the DB
        trng_data = json_util.loads(json.dumps(self.mongodb.rel_link_data[0]))
        # Get work-role and KSAT maps
        wkroles_map = mttl_db_helpers.get_only_mapped_wkrole_ksat_prof_maps(self.mongodb.db)
        ksats_map = mttl_db_helpers.get_all_requirements(self.mongodb.db)
        # Run the function to be tested
        survey = gen_trng_survey_csvs.build_survey(trng_data, ksats_map, wkroles_map)
        # Test if the function builds the string with the same amount of lines
        # Adding two to expected number of lines for the 1 random KSAT and an empty line at the end
        expected_lines = len(trng_data["KSATs"]) + 2
        self.assertEqual(expected_lines, len(survey.split("\n")), msg="Mismatch in build_survey string")

    def test_main(self):
        test_args = InputArgs(self.col_headers)
        # Run the main() function in gen_trng_survey_csvs.py
        gen_trng_survey_csvs.main(test_args)
        # Determine the number of files generated
        csv_files = file_list_generator.dir_walk(test_dir, ["gen_survey_files"], ".csv")
        exp_files = [os.path.join(test_dir, "gen_survey_files", "see-basics_trng_surveys.csv")]
        # Test if the number generated is the number expected
        self.assertListEqual(exp_files, csv_files, msg="Mismatch in generated surveys")


if __name__ == "__main__":
    unittest.main()
