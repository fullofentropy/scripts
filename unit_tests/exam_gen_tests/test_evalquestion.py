#!/usr/bin/python3

import sys
import unittest
sys.path.append("scripts")
from lib.exam_generator import evalquestion


class ConstructorDefaultValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eval_q = evalquestion.EvalQuestion()

    def test_eval_q_obj(self):
        self.assertIsInstance(self.eval_q, evalquestion.EvalQuestion)

    def test_q_id_def_value(self):
        self.assertIsNone(self.eval_q.question_id, "Default question_id value should be None")

    def test_topic_def_value(self):
        self.assertEqual("", self.eval_q.topic, "Default topic value should be ''")

    def test_uses_def_value(self):
        self.assertEqual(0, self.eval_q.times_provisioned, "Default uses_test value should be 0")

    def test_times_asked_def_value(self):
        self.assertEqual(0, self.eval_q.times_asked, "Default times_asked value should be 0")

    def test_correct_answers_def_value(self):
        self.assertEqual(0, self.eval_q.correct_answers, "Default correct_answers value should be 0")

    def test_complete_time_def_value(self):
        self.assertEqual(0, self.eval_q.complete_time, "Default complete_time value should be 0")

    def test_weight_def_value(self):
        self.assertEqual(1, self.eval_q.weight, "Default weight value should be 1")

    def test_q_weight_scalar_def_value(self):
        self.assertEqual(1, self.eval_q.q_weight_scalar, "Default q_weight_scalar value should be 1")


class ConstructorAssignedValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_id = "dir1/dir2/question.rel-link.json"
        self.topic = "My Topic"
        self.uses = 2
        self.times_asked = 3
        self.correct_answers = 1
        self.complete_time = 5
        self.weight = 1.5
        self.q_weight_scalar = 2
        self.eval_q = evalquestion.EvalQuestion(question_id=self.q_id,
                                                topic=self.topic,
                                                times_provisioned=self.uses,
                                                times_asked=self.times_asked,
                                                correct_answers=self.correct_answers,
                                                complete_time=self.complete_time)
        self.eval_q.weight = self.weight
        self.eval_q.q_weight_scalar = self.q_weight_scalar

    def test_eval_q_obj(self):
        self.assertIsInstance(self.eval_q, evalquestion.EvalQuestion)

    def test_q_id_value(self):
        self.assertEqual(self.q_id, self.eval_q.question_id, f"'question_id' not set to requested value {self.q_id}")

    def test_topic_value(self):
        self.assertEqual(self.topic, self.eval_q.topic, f"'topic' not set to requested value'{self.topic}'")

    def test_uses_value(self):
        self.assertEqual(self.uses, self.eval_q.times_provisioned, f"'uses_test' not set to requested value {self.uses}")

    def test_times_asked_value(self):
        self.assertEqual(self.times_asked, self.eval_q.times_asked,
                         f"'times_asked' not set to requested value {self.times_asked}")

    def test_correct_answers_value(self):
        self.assertEqual(self.correct_answers, self.eval_q.correct_answers,
                         f"'correct_answers' not set to requested value {self.correct_answers}")

    def test_complete_time_value(self):
        self.assertEqual(self.complete_time, self.eval_q.complete_time,
                         f"'complete_time' not set to requested value {self.complete_time}")

    def test_weight_value(self):
        self.assertEqual(self.weight, self.eval_q.weight, f"'weight' not set to requested value {self.weight}")

    def test_q_weight_scalar_value(self):
        self.assertEqual(self.q_weight_scalar, self.eval_q.q_weight_scalar,
                         f"'q_weight_scalar' not set to requested value {self.q_weight_scalar}")


class GetAccuracyTests(unittest.TestCase):
    def setUp(self) -> None:
        # (correct_answers, times_asked)
        self.test_values = [
            (2, 0),
            (0, 0),
            (-3, 4),
            (-7, -56),
            (23, 45)
        ]
        # The algorithm in use by this function
        # max(correct_answers, 1) / max(times_asked, 1); 1 is to prevent div by zero
        self.expected = [2, 1, 0.25, 1, 23/45]
        self.eval_q = evalquestion.EvalQuestion(question_id="dir1/dir2/question.rel-link.json",
                                                topic="My Topic",
                                                times_provisioned=2,
                                                times_asked=3,
                                                correct_answers=4,
                                                complete_time=5)
        self.eval_q.weight = 1.5
        self.eval_q.q_weight_scalar = 2

    def test_get_accuracy(self):
        for index in range(len(self.test_values)):
            self.eval_q.correct_answers = self.test_values[index][0]
            self.eval_q.times_asked = self.test_values[index][1]
            self.assertEqual(self.expected[index], self.eval_q.get_accuracy())


class GetQuestionWeightTests(unittest.TestCase):
    def setUp(self) -> None:
        # (q_weight_scalar, exams_generated, uses_test)
        self.test_values = [
            (-1, -2, 2),
            (0, 0, 0),
            (1, 3, -2),
            (2, 25, 45),
            (1, 100, 76),
            (1, 255, 287),
            (1, 23746, 23246)
        ]
        # The algorithm in use by this function
        # q_weight_scalar * (max(exams_generated, 1) / max(uses_test, 1)); 1 is to prevent div by zero error
        self.expected = [-1 * (1/2), 0, 1 * (3/1), 2 * (25/45), 1 * (100/76), 1 * (255/287), 1 * (23746/23246)]
        self.eval_q = evalquestion.EvalQuestion(question_id="dir1/dir2/question.rel-link.json",
                                                topic="My Topic",
                                                times_provisioned=2,
                                                times_asked=3,
                                                correct_answers=4,
                                                complete_time=5)

    def test_get_question_weight(self):
        for index in range(len(self.test_values)):
            self.eval_q.q_weight_scalar = self.test_values[index][0]
            self.eval_q.times_provisioned = self.test_values[index][2]
            self.assertEqual(self.expected[index], self.eval_q.get_question_weight(self.test_values[index][1]))


if __name__ == "__main__":
    unittest.main()
