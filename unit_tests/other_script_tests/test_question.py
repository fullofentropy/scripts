import sys
import unittest
sys.path.append("scripts")
from lib import question


class QuestionTests(unittest.TestCase):
    def test_question_default_init(self):
        quest = question.Question()
        expected = {
            "_id": "",
            "rel-link_id": "",
            "question_name": "",
            "question_path": "",
            "question_type": "",
            "topic": "",
            "disabled": True,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0
        }
        self.assertIsInstance(quest, question.Question, msg="Mismatch in Question Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_question_init(self):
        quest = question.Question("ID", "rel_ID", "q name", "q/path", "knowledge", "topic", False, 1, 1, 1, 1)
        expected = {
            "_id": "ID",
            "rel-link_id": "rel_ID",
            "question_name": "q name",
            "question_path": "q/path",
            "question_type": "knowledge",
            "topic": "topic",
            "disabled": False,
            "provisioned": 1,
            "attempts": 1,
            "passes": 1,
            "failures": 1,
        }
        self.assertIsInstance(quest, question.Question, msg="Mismatch in Question Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_knowledge_default_init(self):
        quest = question.Knowledge()
        expected = {
            "_id": "",
            "rel-link_id": "",
            "question_name": "",
            "question_path": "",
            "question_type": "",
            "topic": "",
            "disabled": True,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "",
            "choices": [],
            "answer": -1,
            "explanation": []
        }
        self.assertIsInstance(quest, question.Knowledge, msg="Mismatch in Knowledge Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_knowledge_init(self):
        quest = question.Knowledge("ID", "rel_ID", "q name", "q/path", "knowledge", "topic", False, 1, 1, 1, 1,
                                   "The question", "snippet", "snippet lang", ["A", "B", "C", "D"], 1,
                                   ["The explanation"])
        expected = {
            "_id": "ID",
            "rel-link_id": "rel_ID",
            "question_name": "q name",
            "question_path": "q/path",
            "question_type": "knowledge",
            "topic": "topic",
            "disabled": False,
            "provisioned": 1,
            "attempts": 1,
            "passes": 1,
            "failures": 1,
            "question": "The question",
            "snippet": "snippet",
            "snippet_lang": "snippet lang",
            "choices": ["A", "B", "C", "D"],
            "answer": 1,
            "explanation": ["The explanation"]
        }
        self.assertIsInstance(quest, question.Knowledge, msg="Mismatch in Knowledge Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_knowledge_group_default_init(self):
        quest = question.Knowledge(grp_name="", grp_questions=[])
        expected = {
            "_id": "",
            "rel-link_id": "",
            "question_name": "",
            "question_path": "",
            "question_type": "",
            "topic": "",
            "disabled": True,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "",
            "choices": [],
            "answer": -1,
            "explanation": [],
            "group": {
                "group_name": "",
                "questions": []
            }
        }
        self.assertIsInstance(quest, question.Knowledge, msg="Mismatch in Knowledge Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_knowledge_no_groupname_default_init(self):
        self.assertRaises(ValueError, question.Knowledge, grp_questions=[])

    def test_knowledge_no_group_questions_default_init(self):
        self.assertRaises(ValueError, question.Knowledge, grp_name="")

    def test_performance_dev_default_init(self):
        quest = question.PerformanceDev()
        expected = {
            "_id": "",
            "rel-link_id": "",
            "question_name": "",
            "question_path": "",
            "question_type": "",
            "topic": "",
            "disabled": True,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "",  # URL to examinee source file
            },
            "support_files": [],
            "tests": [],
            "solution": "",
            "dependencies": {
                "libraries": [],
                "req_software": []
            }
        }
        self.assertIsInstance(quest, question.PerformanceDev, msg="Mismatch in PerformanceDev Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_performance_dev_init(self):
        quest = question.PerformanceDev("ID", "rel_ID", "q name", "q/path", "knowledge", "topic", False, 1, 1, 1, 1,
                                        "q/filepath",
                                        "q/headerpath",
                                        ["the support files"],
                                        [{"filepath": "t/filepath", "headerpath": "t/headerpath"}],
                                        "solution/path",
                                        ["dep libraries"],
                                        ["dep software"])
        expected = {
            "_id": "ID",
            "rel-link_id": "rel_ID",
            "question_name": "q name",
            "question_path": "q/path",
            "question_type": "knowledge",
            "topic": "topic",
            "disabled": False,
            "provisioned": 1,
            "attempts": 1,
            "passes": 1,
            "failures": 1,
            "question": {
                "filepath": "q/filepath",  # URL to examinee source file
                "headerpath": "q/headerpath"  # URL to header file
            },
            "support_files": ["the support files"],
            "tests": [
                {
                    "filepath": "t/filepath",  # URL to examinee source file
                    "headerpath": "t/headerpath"  # URL to header file
                }
            ],
            "solution": "solution/path",
            "dependencies": {
                "libraries": ["dep libraries"],
                "req_software": ["dep software"]
            }
        }
        self.assertIsInstance(quest, question.PerformanceDev, msg="Mismatch in PerformanceDev Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_performance_dev_init_no_headers(self):
        quest = question.PerformanceDev("ID", "rel_ID", "q name", "q/path", "knowledge", "topic", False, 1, 1, 1, 1,
                                        "q/filepath",
                                        "",
                                        ["the support files"],
                                        [{"filepath": "t/filepath"}],
                                        "solution/path",
                                        ["dep libraries"],
                                        ["dep software"])
        expected = {
            "_id": "ID",
            "rel-link_id": "rel_ID",
            "question_name": "q name",
            "question_path": "q/path",
            "question_type": "knowledge",
            "topic": "topic",
            "disabled": False,
            "provisioned": 1,
            "attempts": 1,
            "passes": 1,
            "failures": 1,
            "question": {
                "filepath": "q/filepath",  # URL to examinee source file
            },
            "support_files": ["the support files"],
            "tests": [
                {
                    "filepath": "t/filepath",  # URL to examinee source file
                }
            ],
            "solution": "solution/path",
            "dependencies": {
                "libraries": ["dep libraries"],
                "req_software": ["dep software"]
            }
        }
        self.assertIsInstance(quest, question.PerformanceDev, msg="Mismatch in PerformanceDev Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_performance_dev_init_no_test_filepath(self):
        self.assertRaises(ValueError, question.PerformanceDev, tests=[{"headerpath": "t/headerpath"}])

    def test_performance_dev_init_test_invalid_objects(self):
        self.assertRaises(ValueError, question.PerformanceDev, tests=["headerpath", "t/headerpath"])

    def test_performance_dev_init_no_quest_filepath(self):
        self.assertRaises(ValueError, question.PerformanceDev, quest_headerpath="q/headerpath")

    def test_performance_dev_init_space_quest_filepath(self):
        self.assertRaises(ValueError, question.PerformanceDev, quest_filepath=" ", quest_headerpath="q/headerpath")

    def test_performance_nondev_default_init(self):
        quest = question.PerformanceNonDev()
        expected = {
            "_id": "",
            "rel-link_id": "",
            "question_name": "",
            "question_path": "",
            "question_type": "",
            "topic": "",
            "disabled": True,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "",
            "support_files": [],
            "solution": ""
        }
        self.assertIsInstance(quest, question.PerformanceNonDev, msg="Mismatch in PerformanceNonDev Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")

    def test_performance_nondev_init(self):
        quest = question.PerformanceNonDev("ID", "rel_ID", "q name", "q/path", "knowledge", "topic", False, 1, 1, 1, 1,
                                           "q/filepath", ["the support files"], "solution/path")
        expected = {
            "_id": "ID",
            "rel-link_id": "rel_ID",
            "question_name": "q name",
            "question_path": "q/path",
            "question_type": "knowledge",
            "topic": "topic",
            "disabled": False,
            "provisioned": 1,
            "attempts": 1,
            "passes": 1,
            "failures": 1,
            "question": "q/filepath",
            "support_files": ["the support files"],
            "solution": "solution/path"
        }
        self.assertIsInstance(quest, question.PerformanceNonDev, msg="Mismatch in PerformanceNonDev Class objects")
        self.assertDictEqual(expected, quest.to_dict(), msg="Mismatch in default values")


if __name__ == "__main__":
    unittest.main()
