#!/usr/var/bin/python3

import sys
import os
import unittest
import json
from bson import json_util
sys.path.append("scripts")
from lib import mttl_db_helpers
from lib.unit_test_helpers import fake_mttl_mongo_db
from lib.default_strings import id_key

test_dir = "test_mttl_helpers"
test_req_dir = "reqs"
rdmap_file = "Roadmap.json"
no_exist_file = "non-existent.json"
invalid_json_file = "invalid-json.json"
invalid_json = {
    34,
    'thirty-four'
}


class MttlHelpersTests(unittest.TestCase):
    mongodb = None
    @classmethod
    def setUpClass(cls) -> None:
        cls.mongodb = fake_mttl_mongo_db.FakeMttlDb(test_dir)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.mongodb.delete_db()

    def setUp(self) -> None:
        self.roles_expected = []
        for role, data in self.mongodb.rdmap_data.items():
            if role == "Special Missions" or role == "MPO" or role == "MSM" or role == "MCCD":
                if role != "Special Missions" and role not in self.roles_expected:
                    self.roles_expected.append(role)
                for course in data["courses"]:
                    if course["name"] not in self.roles_expected:
                        self.roles_expected.append(course["name"])
            else:
                self.roles_expected.append(role)

    def test_get_rdmap_names(self):
        self.assertListEqual(self.roles_expected, mttl_db_helpers.get_rdmap_names(test_dir),
                             msg="Mismatch in list of work-role names")

    def test_get_rdmap_names_value_err(self):
        self.assertRaises(FileNotFoundError, mttl_db_helpers.get_rdmap_names, test_dir, no_exist_file)

    def test_get_rdmap_names_bad_json(self):
        with open(os.path.join(test_dir, invalid_json_file), "w") as fp:
            fp.writelines(f"{invalid_json}")
        self.assertRaises(json.JSONDecodeError, mttl_db_helpers.get_rdmap_names, test_dir, invalid_json_file)
        os.remove(os.path.join(test_dir, invalid_json_file))

    def test_get_all_ksats(self):
        # json.dumps converts the dict into a json string and json_util.loads converts $oid into an ObjectId object
        # This is what will be returned from the MongoDB
        expected = {json_util.loads(json.dumps(item))[id_key]: json_util.loads(json.dumps(item))
                    for item in self.mongodb.requirement_data}
        self.assertDictEqual(expected, mttl_db_helpers.get_all_requirements(self.mongodb.db),
                             msg="Mismatch in KSAT data")

    def test_get_wkrole_ksat_prof_maps(self):
        expected = {item["ksat_id"]: item["proficiency"] for item in self.mongodb.work_role_data
                    if item["work-role"] == "Basic-Dev"}
        self.assertDictEqual(expected, mttl_db_helpers.get_wkrole_ksat_prof_maps("Basic-Dev", self.mongodb.db),
                             msg="Mismatch in work-role proficiency map")

    def test_get_wkrole_ksat_prof_maps_null_role(self):
        expected = {}
        self.assertDictEqual(expected, mttl_db_helpers.get_wkrole_ksat_prof_maps("", self.mongodb.db),
                             msg="Mismatch in work-role proficiency map")

    def test_get_all_wkrole_ksat_prof_maps(self):
        expected = {}
        for role in self.roles_expected:
            expected[role.upper()] = {}
        for item in self.mongodb.work_role_data:
            expected[item["work-role"].upper()].update({item["ksat_id"]: item["proficiency"]})
        self.assertDictEqual(expected, mttl_db_helpers.get_all_wkrole_ksat_prof_maps(test_dir, self.mongodb.db),
                             msg="Mismatch in all work-role KSAT to proficiency maps")

    def test_get_all_wkrole_ksat_prof_maps_no_exist_dir(self):
        self.assertRaises(FileNotFoundError,
                          mttl_db_helpers.get_all_wkrole_ksat_prof_maps, "no_exist_dir", self.mongodb.db)

    def test_get_all_wkrole_ksat_prof_maps_null_dir_param(self):
        self.assertRaises(FileNotFoundError, mttl_db_helpers.get_all_wkrole_ksat_prof_maps, "", self.mongodb.db)

    def test_get_only_mapped_wkrole_ksat_prof_maps(self):
        expected = {item["work-role"].upper(): {} for item in self.mongodb.work_role_data}
        for item in self.mongodb.work_role_data:
            expected[item["work-role"].upper()].update({item["ksat_id"]: item["proficiency"]})
        self.assertDictEqual(expected, mttl_db_helpers.get_only_mapped_wkrole_ksat_prof_maps(self.mongodb.db),
                             msg="Mismatch in work-role KSAT to proficiency maps")

    def test_get_mapped_wkrole_names(self):
        expected = []
        for item in self.mongodb.work_role_data:
            if item["work-role"] not in expected:
                expected.append(item["work-role"])
        # Since we don't care about order
        expected.sort()
        returned = mttl_db_helpers.get_mapped_wkrole_names(self.mongodb.db)
        returned.sort()
        self.assertListEqual(expected, returned, msg="Mismatch in work-role names")

    def test_get_module_lsn_data(self):
        # json.dumps converts the dict into a json string and json_util.loads converts $oid into an ObjectId object
        # This is what will be returned from the MongoDB
        sorted_topics = [item["topic"] for item in self.mongodb.rel_link_data
                         if "module" in item and item["module"] == "c-programming"]
        sorted_topics.sort()
        expected = []
        for topic in sorted_topics:
            for item in self.mongodb.rel_link_data:
                if "module" in item:
                    item = json_util.loads(json.dumps(item))  # Convert data to MongoDB returned format
                    if "c-programming" == item["module"] and topic == item["topic"] and item not in expected:
                        expected.append(item)
        returned = mttl_db_helpers.get_module_lsn_data("c-programming", self.mongodb.db)
        self.assertListEqual(expected, returned,
                             msg="Mismatch in Module list")

    def test_get_module_topics(self):
        expected = [item["topic"] for item in self.mongodb.rel_link_data
                    if "module" in item and item["module"] == "Basic-Dev"]
        expected.sort()
        self.assertListEqual(expected, mttl_db_helpers.get_module_topics("Basic-Dev", self.mongodb.db),
                             msg="Mismatch in Module's topic list")

    def test_get_module_subjects(self):
        expected = [item["subject"] for item in self.mongodb.rel_link_data
                    if "module" in item and item["module"] == "Basic-Dev"]
        expected.sort()
        self.assertListEqual(expected, mttl_db_helpers.get_module_subjects("Basic-Dev", self.mongodb.db),
                             msg="Mismatch in Module's subject list")


if __name__ == "__main__":
    unittest.main()
