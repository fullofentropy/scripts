import os
import sys
import json
import shutil
import unittest
sys.path.append("scripts")
from lib import file_list_generator


class FileListGeneratorTests(unittest.TestCase):
    test_dir = "test-file-list-gen"
    test_sub_dir = "test_sub_dir"
    test_sub_sub_dir = "test_sub_sub_dir"
    test_json_files = {
        "test_empty.rel-link.json": {},
        "test_1_attribute.rel-link.json": {
            "attribute1": "value1"
        },
        "test_3_attributes.rel-link.json": {
            "attribute1": "value1",
            "attribute2": ["value1", "value2"],
            "attribute3": 3
        },
        "test_null_dict.rel-link.json": {
            "attribute1": "",
            "attribute2": {"key1": "value1", "key2": "value2"},
            "attribute3": [],
            "attribute4": None
        },
        "test_eval_content.rel-link.json": {
            "uid": "BPO_0003",
            "checksum": "13e6c7127c827f7e3297bf83bcdf8413ef16c1e5156f7c931295f3c1315ce594",
            "TRN_EVL_ID": "Basic-PO",
            "NETWORK": "commercial",
            "topic": "Product Owner",
            "version": "",
            "KSATs": [
                [
                    "K0367",
                    "B"
                ]
            ],
            "workroles": [
                "Basic-Po"
            ],
            "proficiency": "B",
            "complexity": 1,
            "etc": 2,
            "creation_date": "2019-12-11 20:11:44",
            "revision_date": "2020-01-08 17:51:10",
            "revision_number": 1,
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "knowledge": {
                "question": "Developers sign up to own backlog items during Sprint Planning.",
                "snippet": None,
                "choices": [
                    "True",
                    "False"
                ],
                "answer": 1,
                "explanation": [
                    "The ownership of the items and tasks are shared. The items are not even \"assigned\" to developers"
                    " because they require multiple expertise. Only the tasks are assigned and even then, their "
                    "ownership is shared."
                ]
            }
        },
        "test_trng_content.rel-link.json": {
            "uid": "SEE_Training",
            "TRN_EVL_ID": "SEE-Basics",
            "NETWORK": "NIPR",
            "KSATs": [
                [
                    "A0653",
                    "3"
                ],
                [
                    "A0654",
                    "3"
                ],
                [
                    "A0657",
                    "3"
                ],
                [
                    "A0658",
                    "3"
                ]
            ],
            "course": "Basic SEE Training",
            "module": "SEE 101",
            "topic": "SEE 101",
            "subject": "SEE 101",
            "lecture_time": 60,
            "perf_demo_time": 0,
            "references": [],
            "lesson_objectives": [],
            "performance_objectives": [],
            "workroles": [
                "Basic-SEE"
            ]
        },
        "test_ojt_content.rel-link.json": {
            "uid": "SEE_OJT",
            "TRN_EVL_ID": "SEE-Basics",
            "NETWORK": "NIPR",
            "KSATs": [
                [
                    "A0670",
                    "3"
                ],
                [
                    "A0672",
                    "3"
                ],
                [
                    "A0673",
                    "3"
                ],
                [
                    "A0674",
                    "3"
                ],
                [
                    "A0675",
                    "3"
                ],
                [
                    "A0676",
                    "3"
                ],
                [
                    "A0677",
                    "3"
                ],
                [
                    "A0679",
                    "3"
                ],
                [
                    "A0680",
                    "3"
                ],
                [
                    "A0684",
                    "3"
                ]
            ],
            "workroles": [
                "Basic-SEE"
            ]
        },
        "test_other.json": [
            {"somekey": "somevalue"}
        ]
    }
    test_md_files = {
        "test_empty.md": "",
        "test_content.md": "# SEE Training Material\n\n"
                           "See Training Slides folder for training material.",
        "test_eval_content.md": "## Developers sign up to own backlog items during Sprint Planning.\n\n"
                                "KSAs: 853\n\n"
                                "## Answer\n"
                                "| A | ***B*** |\n"
                                "| :--- | :--- |\n"
                                "| True | ***False*** |\n\n\n"
                                "Feedback:\n\n"
                                "- The ownership of the items and tasks are shared. The items are not even \"assigned\""
                                " to developers because they require multiple expertise. Only the tasks are assigned "
                                "and even then, their ownership is shared.\n\n"
                                "[**<- Back To Basic PO**](../../Basic_PO.md)\n",
        "test_trng_content.md": "# Bitwise Operations\n\n"
                                "A bit is the smallest unit of information on a computer. Bits are normally represented"
                                " by 1 and 0. 1 generally meaning \[on, true, set, yes\] and 0 generally meaning \[off,"
                                " false, cleared, no\]. Bitwise operators test and/or modify bits. We use bit "
                                "manipulations to control the machine at the lowest level. \n\n"
                                "## Why Bitwise Operations?\n\n"
                                "Bitwise operations can be used to pack single-bit values in a large storage space "
                                "\(see: flags\). With that said, bitwise operations are also good for saving space by "
                                "making efficient use of larger storage spaces. Due to it's minimalistic approach and "
                                "low level abstraction, it's also used heavily in device drivers, pixel-level graphics,"
                                " compression, encryption and Assembly. Lastly, bit shifting is faster than "
                                "multiplication. \(Though modern compilers will automatically optimize as "
                                "appropriate\) \n\n\n"
                                "---"
    }
    @classmethod
    def setUpClass(cls) -> None:
        os.makedirs(os.path.join(cls.test_dir, cls.test_sub_dir, cls.test_sub_sub_dir))
        for file_name, data in cls.test_json_files.items():
            with open(os.path.join(cls.test_dir, file_name), "w") as data_fp:
                json.dump(data, data_fp, indent=None)
            with open(os.path.join(cls.test_dir, cls.test_sub_dir, file_name), "w") as data_fp:
                json.dump(data, data_fp, indent=None)
            with open(os.path.join(cls.test_dir, cls.test_sub_dir, cls.test_sub_sub_dir, file_name), "w") as data_fp:
                json.dump(data, data_fp, indent=None)
        for file_name, data in cls.test_md_files.items():
            with open(os.path.join(cls.test_dir, file_name), "w") as data_fp:
                data_fp.write(data)
            with open(os.path.join(cls.test_dir, cls.test_sub_dir, file_name), "w") as data_fp:
                data_fp.write(data)
            with open(os.path.join(cls.test_dir, cls.test_sub_dir, cls.test_sub_sub_dir, file_name), "w") as data_fp:
                data_fp.write(data)

    @classmethod
    def tearDownClass(cls) -> None:
        if os.path.exists(cls.test_dir):
            shutil.rmtree(cls.test_dir)

    def setUp(self) -> None:
        self.all_paths_no_recurse = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_paths = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_paths.sort()
        self.all_rel_link_paths_no_recurse = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_rel_link_paths = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_rel_link_paths.sort()
        self.all_json_paths_no_recurse = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_json_paths = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_ojt_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_json_paths.sort()
        self.all_json_paths_except_ojt_no_recurse = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_json_paths_except_ojt = [
            f"{os.path.join(self.test_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_1_attribute.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_3_attributes.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_null_dict.rel-link.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_other.json')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.rel-link.json')}"
        ]
        self.all_json_paths_except_ojt.sort()
        self.all_md_paths = [
            f"{os.path.join(self.test_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_trng_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_trng_content.md')}"
        ]
        self.all_md_paths_no_recurse = [
            f"{os.path.join(self.test_dir, 'test_content.md')}",
            f"{os.path.join(self.test_dir, 'test_empty.md')}",
            f"{os.path.join(self.test_dir, 'test_eval_content.md')}",
            f"{os.path.join(self.test_dir, 'test_trng_content.md')}"
        ]
        self.all_md_paths.sort()
        self.msg = "Mismatch in returned list"

    def tearDown(self) -> None:
        if os.path.basename(os.getcwd()) == self.test_dir:
            os.chdir("..")

    def test_strip_avoid_paths_mt_lists(self):
        # Tests the case where both paths and avoid list are empty
        self.assertListEqual([], file_list_generator.strip_avoid_paths(paths=[], avoid_list=[]), self.msg)

    def test_strip_avoid_paths_mt_avoid_list(self):
        # Tests an empty avoid list
        avoid_list = []
        expected_list = self.all_paths
        result = file_list_generator.strip_avoid_paths(self.all_paths, avoid_list)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_strip_avoid_paths_md(self):
        # Tests an avoid list with a list of md file paths
        avoid_list = self.all_md_paths
        expected_list = self.all_json_paths
        result = file_list_generator.strip_avoid_paths(self.all_paths, avoid_list)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_strip_avoid_paths_rel_link(self):
        # Tests an avoid list with a list of rel-link file paths
        avoid_list = self.all_rel_link_paths
        expected_list = self.all_md_paths + [f"{os.path.join(self.test_dir, 'test_other.json')}"]
        expected_list.append(f"{os.path.join(self.test_dir, self.test_sub_dir, 'test_other.json')}")
        expected_list.append(f"{os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir, 'test_other.json')}")
        expected_list.sort()
        result = file_list_generator.strip_avoid_paths(self.all_paths, avoid_list)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_strip_avoid_paths_keyword1(self):
        # Tests an avoid list with 1 item
        avoid_list = [".md"]
        expected_list = self.all_json_paths
        result = file_list_generator.strip_avoid_paths(self.all_paths, avoid_list)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_strip_avoid_paths_keyword2(self):
        # Tests an avoid list with 2 items
        avoid_list = [".md", "ojt"]
        expected_list = self.all_json_paths_except_ojt
        result = file_list_generator.strip_avoid_paths(self.all_paths, avoid_list)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_strip_avoid_file_name_only(self):
        # Tests an avoid list with 1 item that only considers the file name and not the entire path when avoiding
        avoid_list = ["ojt"]
        test_list = [
            "ojt_files/ojt.rel-link.json",
            "ojt_files/training.rel-link.json",
            "trng_files/ojt.rel-link.json",
            "trng_files/training.rel-link.json"
        ]
        expected_list = [
            "ojt_files/training.rel-link.json",
            "trng_files/training.rel-link.json"
        ]
        result = file_list_generator.strip_avoid_paths(test_list, avoid_list, True)
        result.sort()
        self.assertListEqual(expected_list, result, self.msg)

    def test_dir_walk_def_with_rt_dir_param(self):
        # Tests getting all default keyword files recursively using a directory as the root dir and dot in rel-link dirs
        # Convert directory/file.ext or directory/subdirectory/file.ext to directory/./file.ext or
        # directory/./subdirectory/file.ext
        expected = [os.path.join(x.split(os.sep)[0], ".", os.path.join(f"{os.sep}".join(x.split(os.sep)[1:])))
                    for x in self.all_json_paths]
        result = file_list_generator.dir_walk(self.test_dir, ["."])
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_def_with_rel_dirs_param(self):
        # Tests getting all default keyword files recursively using dot as the root dir and a directory in rel-link dirs
        expected = [os.path.join(".", x) for x in self.all_json_paths]
        result = file_list_generator.dir_walk(".", [self.test_dir])
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_with_keyword_param_rellink(self):
        # Tests getting all .rel-link.json files recursively
        expected = [os.path.join(".", x) for x in self.all_rel_link_paths]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".rel-link.json")
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_with_keyword_param_json(self):
        # Tests getting all .json files recursively
        expected = [os.path.join(".", x) for x in self.all_json_paths]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".json")
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_with_keyword_param_md(self):
        # Tests getting all .md files recursively
        expected = [os.path.join(".", x) for x in self.all_md_paths]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".md")
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_keyword_json_avoid_ojt(self):
        # Tests getting all .json files with an avoid list recursively
        avoid_list = ['ojt']
        expected = [os.path.join(".", x) for x in self.all_json_paths_except_ojt]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".json", avoid_list=avoid_list)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_no_recurse_rellink(self):
        # Tests getting all .rel-link.json files without recursion
        expected = [os.path.join(".", x) for x in self.all_rel_link_paths_no_recurse]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".rel-link.json", recurse=False)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_no_recurse_md(self):
        # Tests getting all .md files without recursion
        # Convert directory/file.ext or directory/subdirectory/file.ext to directory/./file.ext or
        # directory/./subdirectory/file.ext
        expected = [os.path.join(x.split(os.sep)[0], ".", os.path.join(f"{os.sep}".join(x.split(os.sep)[1:])))
                    for x in self.all_md_paths_no_recurse]
        result = file_list_generator.dir_walk(self.test_dir, ["."], ".md", recurse=False)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_no_recurse_jsons(self):
        # Tests getting all .json files without recursion
        expected = [os.path.join(".", x) for x in self.all_json_paths_no_recurse]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".json", recurse=False)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_no_recurse_jsons_avoid_ojt(self):
        # Tests getting all .json files using an avoid list without recursion
        avoid_list = ["ojt"]
        expected = [os.path.join(".", x) for x in self.all_json_paths_except_ojt_no_recurse]
        result = file_list_generator.dir_walk(".", [self.test_dir], ".json", recurse=False, avoid_list=avoid_list)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_dir_walk_no_recurse_avoid_ojt_md(self):
        # Tests using no keyword with an avoid list without recursion
        avoid_list = ["ojt", ".md"]
        expected = [os.path.join(".", x) for x in self.all_json_paths_except_ojt_no_recurse]
        expected.append(os.path.join(".", self.test_dir, self.test_sub_dir))  # Need to add the sub directory name
        expected.sort()
        result = file_list_generator.dir_walk(".", [self.test_dir], "", recurse=False, avoid_list=avoid_list)
        result.sort()
        self.assertListEqual(expected, result, self.msg)

    def test_get_dirs_mt_string(self):
        # Tests the case of an empty string
        self.assertRaises(OSError, file_list_generator.get_dirs, "")

    def test_get_dirs_dot(self):
        # Tests the case of using the dot
        os.chdir(self.test_dir)
        expected = [self.test_sub_dir]
        expected.sort()
        result = file_list_generator.get_dirs(".")
        result.sort()
        self.assertListEqual(expected, result)

    def test_get_dirs_folder(self):
        # Tests the case of using a folder name
        expected = [self.test_sub_dir]
        expected.sort()
        result = file_list_generator.get_dirs(self.test_dir)
        result.sort()
        self.assertListEqual(expected, result)

    def test_get_dirs_subfolder(self):
        # Tests the case of using a sub folder with a folder that also has a sub folder within itself
        expected = [self.test_sub_sub_dir]
        expected.sort()
        result = file_list_generator.get_dirs(os.path.join(self.test_dir, self.test_sub_dir))
        result.sort()
        self.assertListEqual(expected, result)

    def test_get_dirs_nosubfolders(self):
        # Tests the case where there are no sub folders within the desired path
        expected = []
        expected.sort()
        result = file_list_generator.get_dirs(os.path.join(self.test_dir, self.test_sub_dir, self.test_sub_sub_dir))
        result.sort()
        self.assertListEqual(expected, result)


if __name__ == "__main__":
    unittest.main()
