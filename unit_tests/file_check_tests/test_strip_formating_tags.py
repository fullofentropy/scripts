import unittest
from bs4 import BeautifulSoup
from validate_slides import strip_formating_tags


class TestStrip_formating_tags(unittest.TestCase):
    def test_strip_formating_tags(self):
        parser = BeautifulSoup('<center><p><u><b><i>A</i></b></u></p></center>Unit<br>Test</br>', 'html.parser')
        expected = 'UnitTest'

        self.assertEqual(expected, strip_formating_tags(parser).text)


if __name__ == "__main__":
    unittest.main()
