import unittest
from bs4 import BeautifulSoup
from validate_slides import strip_comments


class TestStripComments(unittest.TestCase):
    def test_strip_comments(self):
        data = """
        <div class="foo">
        A Unit Test
        <!--
        <p>test</p>
        -->
        </div>"""
        parser = BeautifulSoup(data, 'html.parser')
        output = strip_comments(parser).prettify()

        expected = """
        <div class="foo">
        A Unit Test
        </div>"""
        expected_output = BeautifulSoup(expected, 'html.parser').prettify()

        self.assertEqual(expected_output, output)


if __name__ == "__main__":
    unittest.main()
