import sys
import os
import io
import copy
import json
import shutil
from colorama import Fore
import argparse
import unittest
import unittest.mock as mock
sys.path.append("scripts")
import validate_question_jsons
from lib.default_strings import error_str, param_root_dir_default
from lib.errors import ErrorCode


class InputArgs(argparse.Namespace):
    def __init__(self, knowl_schema_fname: str = "", pf_dev_schema_fname: str = "", pf_nondev_schema_fname: str = "",
                 question_dirs: list = None, schema_defs_fname: str = "", keyword: str = "", root_dir: str = ".",
                 out_dir: str = "", avoid_dirs: list = None, changed: bool = False, log_file: str = "val_q_log.json"):
        self.knowl_schema_fname = knowl_schema_fname
        self.pf_dev_schema_fname = pf_dev_schema_fname
        self.pf_nondev_schema_fname = pf_nondev_schema_fname
        self.question_dirs = question_dirs
        if question_dirs is None:
            self.question_dirs = []
        self.schema_defs_fname = schema_defs_fname
        self.filter = keyword
        self.root_dir = root_dir
        self.out_dir = out_dir
        self.avoid_dirs = avoid_dirs
        if avoid_dirs is None:
            self.avoid_dirs = []
        self.changed = changed
        self.log_file = log_file


class ValidateQuestionJsonsTests(unittest.TestCase):
    test_dir = "test_validate_question_jsons"
    schema_dir = "schemas"
    schema_defs_file = "definitions.json"
    knowl_val_schema_file = "knowl_val_schema.json"
    perf_dev_val_schema_file = "perf_dev_val_schema.json"
    perf_nondev_val_schema_file = "perf_nondev_val_schema.json"

    schema_defs_path = os.path.join(schema_dir, schema_defs_file)
    knowl_val_schema_path = os.path.join(schema_dir, knowl_val_schema_file)
    perf_dev_val_schema_path = os.path.join(schema_dir, perf_dev_val_schema_file)
    perf_nondev_val_schema_path = os.path.join(schema_dir, perf_nondev_val_schema_file)

    knowl_no_snip = {
        "_id": "BD_ALG_0001",
        "rel-link_id": "ID for BD_ALG_0001",
        "question_name": "algorithms-big-O.question.json",
        "question_path": "knowledge/Algorithms/algorithms-big-O",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In computer science, what term is used for the notation to classify algorithms according to how "
                    "their running time or space requirements grow as the input size grows?",
        "choices": [
            "Capacity measure",
            "Big O notation",
            "Complexity rating",
            "Weighted measure"
        ],
        "answer": 1,
        "explanation": [
            "Big-O is the common term used to classify a sorting or searching algorithm according to the size of the "
            "data structure being used."
        ]
    }

    knowl_snip = {
        "_id": "BD_DSTRUC_0003",
        "rel-link_id": "ID for BD_DSTRUC_0003",
        "question_name": "doubly-linked-list.question.json",
        "question_path": "knowledge/Data_Structures/data-structures-doubly-linked-lists",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "The above C struct definition is ideal for what type of Data Structure?",
        "snippet": " 1  |  struct Node {\n 2  |    int data;\n 3  |    struct Node *next;\n "
                   "4  |    struct Node *prev;\n 5  |  };\n",
        "snippet_lang": "mermaid",
        "choices": [
            "Linked List",
            "Doubly Linked List",
            "Binary Tree",
            "Graph"
        ],
        "answer": 1,
        "explanation": [
            "Incorrect: Linked list nodes will only have a single pointer to the next node.",
            "Correct: Doubly linked lists will have a pointer that points to the previous node or NULL, and to the "
            "next node or NULL",
            "Incorrect: Although Binary Tree nodes contain two pointers, these typically are labled left and right "
            "respectively",
            "Incorrect: Each node in a graph can point to more than two other graph nodes. Therefore there must be a "
            "way to track an unknown number of nodes such as with an array."
        ]
    }
    knowl_sniplang_snip_missing = {
        "_id": "BD_C_0115",
        "rel-link_id": "ID for BD_C_0115",
        "question_name": "c-basic-file-handling.question.json",
        "question_path": "knowledge/C_Programming/c-basic-file-handling/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which of the following is true when reading and writing files in C?",
        "snippet_lang": "language",
        "choices": [
            "File operations are all functions called with a File object using the dot (.) operator",
            "You can only open in either read or write mode, not both",
            "You have to explicitly close files that are opened",
            "You can only write one character at a time"
        ],
        "answer": 2,
        "explanation": [
            "Incorrect. Objects do not exist in C. The file IO operations in stdio.h are functions that you pass "
            "file names or file descriptors to for them to affect.",
            "Incorrect. You can pass r+ or a+ to open a file in read and write mode.",
            "Correct. File descriptors do not close automatically and unclosed files can use up the file "
            "descriptor table.",
            "Incorrect. You can use fputs() to write character arrays to files."
        ]
    }
    knowl_snip_lang_missing = {
        "_id": "BD_ASM_0002",
        "rel-link_id": "ID for BD_ASM_0002",
        "question_name": "assembly-arithmetic-instructions_1.question.json",
        "question_path": "knowledge/Assembly/assembly-arithmetic-instructions_1/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In the above Assembly code, what does eax contain after all the operations?",
        "snippet": "1 | mov eax, 12\n2 | mov ebx, 4\n3 | add eax, ebx\n4 | inc ebx\n5 | inc eax\n6 | shr eax, 1\n"
                   "7 | ",
        "choices": [
            "17",
            "16",
            "8",
            "18"
        ],
        "answer": 2,
        "explanation": [
            "Line 1 moves the value 12 into the eax register and line 2 moves the value 4 into the ebx register. "
            "Line 3 adds the two values, storing the result (16) in the eax register. Line 4 can be ignored since "
            "the ebx register isn't used anymore. Line 5 increments the eax register value by one, now (17), and "
            "line 6 moves bit values right one spot, from 0001 0001 (17) to 0000 1000 (8)."
        ]
    }
    knowl_snip_empty = {
        "_id": "BD_GIT_0002",
        "rel-link_id": "ID for BD_GIT_0002",
        "question_name": "changes.question.json",
        "question_path": "knowledge/Git/git-changes",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "What does the 'git diff' command do?",
        "snippet": "",
        "snippet_lang": "language",
        "choices": [
            "Shows unstaged changes between your branch and working directory.",
            "Shows unstaged changes between your branch and previous version of branch.",
            "Shows changes since last commit.",
            "Shows changes between different master branch versions."
        ],
        "answer": 0,
        "explanation": [
            "The git diff command shows unstaged changes between your branch and working directory."
        ]
    }
    knowl_group = {
        "_id": "BD_PY_0105",
        "rel-link_id": "ID for BD_PY_0105",
        "question_name": "inheritence.question.json",
        "question_path": "knowledge/Python/inheritence/",
        "question_type": "knowledge",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "What is being implemented on line 8?",
        "snippet": "1 |   class Person:\n2 |     def __init__(self, name):\n3 |         self.name = name\n4 |\n"
                   "5 |     def print(self):\n6 |         print(self.name)\n7 |\n8 |   class Airman(Person):\n"
                   "9 |     def __init__(self, name, rank):\n10 |         super().__init__(name)\n"
                   "11 |         self.rank = rank\n12 |\n13 |     def print(self):\n"
                   "14 |         print(self.rank, self.name)  \n15 |\n"
                   "16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}\n17 |\n"
                   "18 |   for peep in people:\n19 |     peep.print()",
        "snippet_lang": "python",
        "choices": [
            "Polymorphism",
            "Inheritance",
            "A list",
            "Overloading"
        ],
        "answer": 1,
        "explanation": [
            "The Airman class is inheriting from the Person class with this syntax  Airman(Person)"
        ],
        "group": {
            "group_name": "python-group_2",
            "questions": [
                "BD_PY_0103",
                "BD_PY_0104",
                "BD_PY_0105",
                "BD_PY_0106",
                "BD_PY_0107",
                "BD_PY_0108",
                "BD_PY_0109"
            ]
        }
    }
    perf_dev = {
        "_id": "BD_C_0067",
        "rel-link_id": "ID for BD_C_0067",
        "question_name": "c_binary_search_tree.question.json",
        "question_path": "performance/C_Programming_Converted/c_binary_search_tree/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp"
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }
    perf_dev_empty_rel_link_id = {
        "_id": "BD_C_0067",
        "rel-link_id": "",
        "question_name": "perf_dev_empty_rel_link_id.question.json",
        "question_path": "performance/C_Programming_Converted/c_binary_search_tree/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp"
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }
    perf_dev_spaced_rel_link_id = {
        "_id": "BD_C_0067",
        "rel-link_id": " ",
        "question_name": "perf_dev_spaced_rel_link_id.question.json",
        "question_path": "performance/C_Programming_Converted/c_binary_search_tree/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp"
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }
    perf_dev_invalid_header = {
        "_id": "BD_C_0067",
        "rel-link_id": "ID for BD_C_0067",
        "question_name": "invalid_header.question.json",
        "question_path": "performance/C_Programming_Converted/invalid_header/",
        "question_type": "performance_dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
            "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
        },
        "support_files": [],
        "tests": [
            {
                "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp",
                "headerpath": ""
            }
        ],
        "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
        "dependencies": {
            "libraries": [
                "gmock-1.7.0"
            ],
            "req_software": [
                "MS Visual Studio 2017"
            ]
        }
    }
    perf_nondev = {
        "_id": "BPO_0074",
        "rel-link_id": "ID for BPO_0074",
        "question_name": "scenarioA-NewProject.question.json",
        "question_path": "performance/scenarioA-NewProject/",
        "question_type": "performance_non-dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "performance/scenarioA-NewProject/ScenarioA_NewProject.md",
        "support_files": [
            "CNF",
            "Instructions.md",
            "MADP(APIC)",
            "ProductOwner_Cert_TaskList.csv"
        ],
        "solution": "performance/scenarioA-NewProject/Performance_EVAL_Grading_Ruberic.csv"
    }
    invalid_qtype_json = {
        "_id": "BPO_1074",
        "rel-link_id": "ID for BPO_1074",
        "question_name": "invalid_qtype.question.json",
        "question_path": "performance/invalid_qtype/",
        "question_type": "invalid_type",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "performance/invalid_qtype/invalid_qtype.md",
        "support_files": [],
        "solution": "performance/invalid_qtype/invalid_qtype_Grading_Ruberic.csv"
    }
    missing_rel_link_id_json = {
        "_id": "BPO_1074",
        "question_name": "missing_rel_link_id_json.question.json",
        "question_path": "performance/missing_rel_link_id_json/",
        "question_type": "performance_non-dev",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "performance/missing_rel_link_id_json/invalid_qtype.md",
        "support_files": [],
        "solution": "performance/missing_rel_link_id_json/missing_rel_link_id_json_Grading_Ruberic.csv"
    }
    knowl_whitespace_json = {
        "_id": "   BD_ALG_0001",
        "rel-link_id": "5ee9250b761cf87371cf315e  ",
        "question_name": " ",
        "question_path": "knowledge/Algorithms/algorithms-big-O",
        "question_type": "knowledge",
        "topic": "Algorithms",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "  In computer science, what term is used for the notation to classify algorithms according to how "
                    "their running time or space requirements grow as the input size grows?",
        "choices": [
            "Capacity measure ",
            " ",
            "Complexity rating",
            "           Weighted measure"
        ],
        "answer": 1,
        "explanation": [
            "Big-O is the common term used to classify a sorting or searching algorithm according to the size of the "
            "data structure being used."
        ]
    }
    knowl_whitespace_json_expected = {
        "_id": "BD_ALG_0001",
        "rel-link_id": "5ee9250b761cf87371cf315e",
        "question_name": "",
        "question_path": "knowledge/Algorithms/algorithms-big-O",
        "question_type": "knowledge",
        "topic": "Algorithms",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In computer science, what term is used for the notation to classify algorithms according to how "
                    "their running time or space requirements grow as the input size grows?",
        "choices": [
            "Capacity measure",
            "",
            "Complexity rating",
            "Weighted measure"
        ],
        "answer": 1,
        "explanation": [
            "Big-O is the common term used to classify a sorting or searching algorithm according to the size of the "
            "data structure being used."
        ]
    }
    perf_whitespace_json = {
        "_id": "BD_ASM_0036",
        "rel-link_id": " ",
        "question_name": " add_values",
        "question_path": "performance/Assembly/add_values ",
        "question_type": "performance_dev",
        "topic": "Assembly",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": " "
        },
        "support_files": [
            " performance/Assembly/add_values/ASM.vcxproj",
            " ",
            "performance/Assembly/add_values/main.c "
        ],
        "tests": [
            {
                "filepath": " "
            },
            {
                "filepath": " performance/Assembly/add_values/test_defs.c",
                "headerpath": "performance/Assembly/add_values/test_defs.h "
            }
        ],
        "solution": "performance/Assembly/add_values/Solution/testfile.S",
        "dependencies": {
            "libraries": [],
            "req_software": [
                " ",
                "MS Visual Studio 2017   "
            ]
        }
    }
    perf_whitespace_json_expected = {
        "_id": "BD_ASM_0036",
        "rel-link_id": "",
        "question_name": "add_values",
        "question_path": "performance/Assembly/add_values",
        "question_type": "performance_dev",
        "topic": "Assembly",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": {
            "filepath": ""
        },
        "support_files": [
            "performance/Assembly/add_values/ASM.vcxproj",
            "",
            "performance/Assembly/add_values/main.c"
        ],
        "tests": [
            {
                "filepath": ""
            },
            {
                "filepath": "performance/Assembly/add_values/test_defs.c",
                "headerpath": "performance/Assembly/add_values/test_defs.h"
            }
        ],
        "solution": "performance/Assembly/add_values/Solution/testfile.S",
        "dependencies": {
            "libraries": [],
            "req_software": [
                "",
                "MS Visual Studio 2017"
            ]
        }
    }

    @classmethod
    def require_group(cls, schema: dict):
        new_schema = copy.deepcopy(schema)
        for index, schema_part in enumerate(new_schema["allOf"]):
            if "properties" in schema_part:
                new_schema["allOf"][index]["required"].append("group")
        return new_schema

    @classmethod
    def require_snippet(cls, schema: dict):
        new_schema = copy.deepcopy(schema)
        for index, schema_part in enumerate(new_schema["allOf"]):
            if "properties" in schema_part:
                new_schema["allOf"][index]["required"].append("snippet")
                new_schema["allOf"][index]["required"].append("snippet_lang")
        return new_schema

    @classmethod
    def build_files(cls):
        mode = "w"
        qpath_key = "question_path"
        qname_key = "question_name"
        if not os.path.exists(cls.test_dir):
            os.mkdir(cls.test_dir)
        if not os.path.exists(os.path.join(cls.test_dir, cls.schema_dir)):
            os.mkdir(os.path.join(cls.test_dir, cls.schema_dir))

        # Good formatted JSON files
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_no_snip[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_no_snip[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_snip[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_snip[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_group[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_group[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.perf_dev[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.perf_dev[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.perf_nondev[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.perf_nondev[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_no_snip[qpath_key],
                               cls.knowl_no_snip[qname_key]), mode) as no_snip_fp:
            json.dump(cls.knowl_no_snip, no_snip_fp)
        with open(os.path.join(cls.test_dir, cls.knowl_snip[qpath_key], cls.knowl_snip[qname_key]), mode) as snip_fp:
            json.dump(cls.knowl_snip, snip_fp)
        with open(os.path.join(cls.test_dir, cls.knowl_group[qpath_key], cls.knowl_group[qname_key]), mode) as group_fp:
            json.dump(cls.knowl_group, group_fp)
        with open(os.path.join(cls.test_dir, cls.perf_dev[qpath_key], cls.perf_dev[qname_key]), mode) as perf_dev_fp:
            json.dump(cls.perf_dev, perf_dev_fp)
        with open(os.path.join(cls.test_dir, cls.perf_nondev[qpath_key],
                               cls.perf_nondev[qname_key]), mode) as perf_nondev_fp:
            json.dump(cls.perf_nondev, perf_nondev_fp)

        # Purposely bad formatted JSON files
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_snip_empty[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_snip_empty[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_snip_lang_missing[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_snip_lang_missing[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.knowl_sniplang_snip_missing[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.knowl_sniplang_snip_missing[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.invalid_qtype_json[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.invalid_qtype_json[qpath_key]))
        if not os.path.exists(os.path.join(cls.test_dir, cls.perf_dev_invalid_header[qpath_key])):
            os.makedirs(os.path.join(cls.test_dir, cls.perf_dev_invalid_header[qpath_key]))
        with open(os.path.join(cls.test_dir, cls.knowl_snip_empty[qpath_key],
                               cls.knowl_snip_empty[qname_key]), mode) as snip_mt_fp:
            json.dump(cls.knowl_snip_empty, snip_mt_fp)
        with open(os.path.join(cls.test_dir, cls.knowl_snip_lang_missing[qpath_key],
                               cls.knowl_snip_lang_missing[qname_key]), mode) as snip_lang_missing_fp:
            json.dump(cls.knowl_snip_lang_missing, snip_lang_missing_fp)
        with open(os.path.join(cls.test_dir, cls.knowl_sniplang_snip_missing[qpath_key],
                               cls.knowl_sniplang_snip_missing[qname_key]), mode) as snip_missing_fp:
            json.dump(cls.knowl_sniplang_snip_missing, snip_missing_fp)
        with open(os.path.join(cls.test_dir, cls.invalid_qtype_json[qpath_key],
                               cls.invalid_qtype_json[qname_key]), mode) as invalid_fp:
            json.dump(cls.invalid_qtype_json, invalid_fp)
        with open(os.path.join(cls.test_dir, cls.perf_dev_invalid_header[qpath_key],
                               cls.perf_dev_invalid_header[qname_key]), mode) as bad_hdr_fp:
            json.dump(cls.perf_dev_invalid_header, bad_hdr_fp)

    @classmethod
    def remove_files(cls):
        if os.path.exists(cls.test_dir):
            shutil.rmtree(cls.test_dir)

    @classmethod
    def setUpClass(cls) -> None:
        cls.build_files()
        with open(cls.schema_defs_path) as schdef_fp:
            cls.schema_defs = json.load(schdef_fp)
        with open(cls.knowl_val_schema_path) as schknowl_fp:
            cls.knowl_val_schema = json.load(schknowl_fp)
        with open(cls.perf_dev_val_schema_path) as schpfdev_fp:
            cls.perf_dev_val_schema = json.load(schpfdev_fp)
        with open(cls.perf_nondev_val_schema_path) as schpfndev_fp:
            cls.perf_nondev_val_schema = json.load(schpfndev_fp)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.remove_files()

    def setUp(self) -> None:
        self.qpath_key = "question_path"
        self.qname_key = "question_name"
        self.entire_knowl_schema = {**self.schema_defs, **self.knowl_val_schema}
        self.entire_perf_dev_schema = {**self.schema_defs, **self.perf_dev_val_schema}
        self.entire_perf_nondev_schema = {**self.schema_defs, **self.perf_nondev_val_schema}
        self.valid_jsons = [
            os.path.join(self.test_dir, self.knowl_no_snip[self.qpath_key]),
            os.path.join(self.test_dir, self.knowl_snip[self.qpath_key]),
            os.path.join(self.test_dir, self.knowl_group[self.qpath_key]),
            os.path.join(self.test_dir, self.perf_dev[self.qpath_key]),
            os.path.join(self.test_dir, self.perf_nondev[self.qpath_key])
        ]
        self.invalid_jsons = [
            os.path.join(self.test_dir, self.knowl_snip_empty[self.qpath_key]),
            os.path.join(self.test_dir, self.perf_dev_invalid_header[self.qpath_key])
        ]
        self.invalid_jsons_combined_paths = [
            os.path.join(self.test_dir, self.knowl_snip_empty[self.qpath_key], self.knowl_snip_empty[self.qname_key]),
            os.path.join(self.test_dir, self.perf_dev_invalid_header[self.qpath_key],
                         self.perf_dev_invalid_header[self.qname_key])
        ]
        self.parser = argparse.ArgumentParser(description=validate_question_jsons.description)
        self.parser.add_argument("-a", "--avoid_dirs", nargs='+', type=str,
                                 default=validate_question_jsons.avoid_dirs_default)
        self.parser.add_argument("-c", "--changed", action="store_true")
        self.parser.add_argument("-f", "--filter", type=str, default=validate_question_jsons.keyword_default)
        self.parser.add_argument("-l", "--log_file", type=str, default=validate_question_jsons.log_file_default)
        self.parser.add_argument("-o", "--out_dir", type=str, default=self.test_dir)
        self.parser.add_argument("-R", "--root_dir", type=str, default=self.test_dir)

    def test_find_file(self):
        expected = os.path.join(self.test_dir, self.knowl_snip[self.qpath_key],
                                self.knowl_snip[self.qname_key]).replace("\\", "/")
        returned = validate_question_jsons.find_file(self.knowl_snip[self.qname_key], self.test_dir)
        if returned is not None:
            returned = returned.replace("\\", "/")
        self.assertEqual(expected, returned, msg="Mismatch when searching for an existent file")

    def test_find_file_no_exist(self):
        no_exist = "non-existent"
        self.assertIsNone(validate_question_jsons.find_file(no_exist, self.test_dir),
                          msg="Mismatch when searching for a non-existent file")

    def test_determine_schema_paths_valid(self):
        expected = (self.schema_defs_path, self.knowl_val_schema_path, self.perf_dev_val_schema_path,
                    self.perf_nondev_val_schema_path)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, self.schema_defs_file,
                                                                             self.knowl_val_schema_file,
                                                                             self.perf_dev_val_schema_file,
                                                                             self.perf_nondev_val_schema_file),
                              msg="Mismatch in returned schema paths")

    def test_determine_schema_paths_invalid_defs(self):
        invalid = "not_exist"
        expected = (None, self.knowl_val_schema_path, self.perf_dev_val_schema_path, self.perf_nondev_val_schema_path)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, invalid,
                                                                             self.knowl_val_schema_file,
                                                                             self.perf_dev_val_schema_file,
                                                                             self.perf_nondev_val_schema_file),
                              msg="Mismatch in returned schema paths")

    def test_determine_schema_paths_invalid_knowl(self):
        invalid = "not_exist"
        expected = (self.schema_defs_path, None, self.perf_dev_val_schema_path, self.perf_nondev_val_schema_path)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, self.schema_defs_file,
                                                                             invalid,
                                                                             self.perf_dev_val_schema_file,
                                                                             self.perf_nondev_val_schema_file),
                              msg="Mismatch in returned schema paths")

    def test_determine_schema_paths_invalid_perf_dev(self):
        invalid = "not_exist"
        expected = (self.schema_defs_path, self.knowl_val_schema_path, None, self.perf_nondev_val_schema_path)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, self.schema_defs_file,
                                                                             self.knowl_val_schema_file,
                                                                             invalid,
                                                                             self.perf_nondev_val_schema_file),
                              msg="Mismatch in returned schema paths")

    def test_determine_schema_paths_invalid_perf_nondev(self):
        invalid = "not_exist"
        expected = (self.schema_defs_path, self.knowl_val_schema_path, self.perf_dev_val_schema_path, None)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, self.schema_defs_file,
                                                                             self.knowl_val_schema_file,
                                                                             self.perf_dev_val_schema_file,
                                                                             invalid),
                              msg="Mismatch in returned schema paths")

    def test_determine_schema_paths_invalid(self):
        invalid = "not_exist"
        expected = (None, None, None, None)
        self.assertTupleEqual(expected,
                              validate_question_jsons.determine_schema_paths(self.test_dir, invalid,
                                                                             invalid,
                                                                             invalid,
                                                                             invalid),
                              msg="Mismatch in returned schema paths")

    def test_build_schemas(self):
        expected = (self.entire_knowl_schema, self.entire_perf_dev_schema, self.entire_perf_nondev_schema)
        self.assertTupleEqual(expected,
                              validate_question_jsons.build_schemas(self.schema_defs_path, self.knowl_val_schema_path,
                                                                    self.perf_dev_val_schema_path,
                                                                    self.perf_nondev_val_schema_path),
                              msg="Mismatch in returned schemas")

    def test_determine_schema_invalid_json(self):
        msg_er_color = " {0}:".format(error_str + Fore.RESET)
        invalid = {}
        expected = f"{msg_er_color} Unable to determine schema due to a missing 'question_type' key in: '{invalid}'"
        with mock.patch('sys.stdout', new=io.StringIO()) as fake_out:
            validate_question_jsons.determine_schema(invalid, self.knowl_val_schema, self.perf_dev_val_schema,
                                                     self.perf_nondev_val_schema)
            self.assertIn(expected, fake_out.getvalue().strip(),
                          "Mismatch in message when 'question_type' key doesn't exist")

    def test_determine_schema_knowl(self):
        expected = self.entire_knowl_schema
        self.assertDictEqual(expected, validate_question_jsons.determine_schema(self.knowl_snip,
                                                                                self.entire_knowl_schema,
                                                                                self.entire_perf_dev_schema,
                                                                                self.entire_perf_nondev_schema),
                             msg="Mismatch in returned schema for a knowledge question")

    def test_determine_schema_perf_dev(self):
        expected = self.entire_perf_dev_schema
        self.assertDictEqual(expected, validate_question_jsons.determine_schema(self.perf_dev,
                                                                                self.entire_knowl_schema,
                                                                                self.entire_perf_dev_schema,
                                                                                self.entire_perf_nondev_schema),
                             msg="Mismatch in returned schema for a performance dev question")

    def test_determine_schema_perf_nondev(self):
        expected = self.entire_perf_nondev_schema
        self.assertDictEqual(expected, validate_question_jsons.determine_schema(self.perf_nondev,
                                                                                self.entire_knowl_schema,
                                                                                self.entire_perf_dev_schema,
                                                                                self.entire_perf_nondev_schema),
                             msg="Mismatch in returned schema for a performance non-dev question")

    def test_determine_schema_invalid_qtype(self):
        expected = {}
        self.assertDictEqual(expected, validate_question_jsons.determine_schema(self.invalid_qtype_json,
                                                                                self.entire_knowl_schema,
                                                                                self.entire_perf_dev_schema,
                                                                                self.entire_perf_nondev_schema),
                             msg="Mismatch in returned schema for a question with an invalid question type")

    def test_group_check_with_group(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = self.require_group(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.group_check(self.knowl_group, schema))

    def test_group_check_with_no_group(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = copy.deepcopy(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.group_check(self.knowl_snip, schema))

    def test_snippet_check_with_snip_and_snip_lang(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = self.require_snippet(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.snippet_check(self.knowl_snip, schema))

    def test_snippet_check_without_snip_or_snip_lang(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = copy.deepcopy(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.snippet_check(self.knowl_no_snip, schema))

    def test_snippet_check_with_snip_lang_and_empty_snippet(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = self.require_snippet(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.snippet_check(self.knowl_sniplang_snip_missing, schema))

    def test_snippet_check_with_snippet_only(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = self.require_snippet(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.snippet_check(self.knowl_snip_lang_missing, schema))

    def test_snippet_check_with_sniplang_only(self):
        schema = copy.deepcopy(self.entire_knowl_schema)
        expected = self.require_snippet(self.entire_knowl_schema)
        self.assertDictEqual(expected, validate_question_jsons.snippet_check(self.knowl_sniplang_snip_missing, schema))

    def test_strip_whitespace_knowl(self):
        self.assertDictEqual(self.knowl_whitespace_json_expected,
                             validate_question_jsons.strip_whitespace(self.knowl_whitespace_json),
                             msg="Mismatch in returned dict when striping leading/trailing whitespace")

    def test_strip_whitespace_perf(self):
        self.assertDictEqual(self.perf_whitespace_json_expected,
                             validate_question_jsons.strip_whitespace(self.perf_whitespace_json),
                             msg="Mismatch in returned dict when striping leading/trailing whitespace")

    def test_validate_json_knowl_snip(self):
        expected = {}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.knowl_snip, self.knowl_snip[self.qname_key],
                                                                   self.entire_knowl_schema),
                             msg="Mismatch in returned status from validate_json")

    def test_validate_json_knowl_group(self):
        expected = {}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.knowl_group, self.knowl_snip[self.qname_key],
                                                                   self.entire_knowl_schema),
                             msg="Mismatch in returned status from validate_json")

    def test_validate_json_knowl_snip_empty(self):
        msg_details = [
            "Failed validating 'minLength': '' is too short",
            {"On instance:": {
                "[snippet]": ""
            }},
            {"In schema": "[allOf][2][properties][snippet][minLength]"}
        ]
        msg = f"validate_question_jsons.py ERROR: in '[snippet]' key of '{self.knowl_snip_empty[self.qname_key]}'"
        expected = {ErrorCode.JSON_DECODE_ERROR.value: [{msg: msg_details}]}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.knowl_snip_empty,
                                                                   self.knowl_snip_empty[self.qname_key],
                                                                   self.entire_knowl_schema),
                             msg="Mismatch in returned status from validate_json with an empty string for the snippet")

    def test_validate_json_perf_dev(self):
        expected = {}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.perf_dev, self.perf_dev[self.qname_key],
                                                                   self.entire_perf_dev_schema),
                             msg="Mismatch in returned status from validate_json")

    def test_validate_json_perf_dev_bad_header_key(self):
        msg_details = [
            "Failed validating 'minLength': '' is too short",
            {"On instance:": {
                "[tests][0][headerpath]": ""
            }},
            {"In schema": "[allOf][2][properties][tests][items][properties][headerpath][minLength]"}
        ]
        msg = f"validate_question_jsons.py ERROR: in '[tests][0][headerpath]' key of " \
              f"'{self.perf_dev_invalid_header[self.qname_key]}'"
        expected = {ErrorCode.JSON_DECODE_ERROR.value: [{msg: msg_details}]}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.perf_dev_invalid_header,
                                                                   self.perf_dev_invalid_header[self.qname_key],
                                                                   self.entire_perf_dev_schema),
                             msg="Mismatch in returned status from validate_json with an empty string for the snippet")

    def test_validate_json_perf_nondev(self):
        expected = {}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.perf_nondev, self.perf_nondev[self.qname_key],
                                                                   self.entire_perf_nondev_schema),
                             msg="Mismatch in returned status from validate_json")

    def test_validate_json_missing_rel_link_id(self):
        msg_details = [
            "Failed validating 'required': 'rel-link_id' is a required property",
            {"On instance:": {
                "[]": {
                    '_id': 'BPO_1074',
                    'question_name': 'missing_rel_link_id_json.question.json',
                    'question_path': 'performance/missing_rel_link_id_json/',
                    'question_type': 'performance_non-dev',
                    'disabled': False,
                    'provisioned': 0,
                    'attempts': 0,
                    'passes': 0,
                    'failures': 0,
                    'question': 'performance/missing_rel_link_id_json/invalid_qtype.md',
                    'support_files': [],
                    'solution': 'performance/missing_rel_link_id_json/missing_rel_link_id_json_Grading_Ruberic.csv'
                }
            }},
            {"In schema": "[allOf][0][required]"}
        ]
        msg = f"validate_question_jsons.py ERROR: in '[]' key of " \
              f"'{self.missing_rel_link_id_json[self.qname_key]}'"
        expected = {ErrorCode.JSON_DECODE_ERROR.value: [{msg: msg_details}]}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.missing_rel_link_id_json,
                                                                   self.missing_rel_link_id_json[self.qname_key],
                                                                   self.entire_perf_nondev_schema),
                             msg="Mismatch in returned status from validate_json with a missing rel-link_id")

    def test_validate_json_empty_rel_link_id(self):
        msg_details = [
            "Failed validating 'minLength': '' is too short",
            {"On instance:": {
                "[rel-link_id]": ''
            }},
            {"In schema": "[allOf][0][properties][rel-link_id][minLength]"}
        ]
        msg = f"validate_question_jsons.py ERROR: in '[rel-link_id]' key of " \
            f"'{self.perf_dev_empty_rel_link_id[self.qname_key]}'"
        expected = {ErrorCode.JSON_DECODE_ERROR.value: [{msg: msg_details}]}
        result = validate_question_jsons.validate_json(self.perf_dev_empty_rel_link_id,
                                                                   self.perf_dev_empty_rel_link_id[self.qname_key],
                                                                   self.entire_perf_dev_schema)
        self.assertDictEqual(expected,
                             result,
                             msg="Mismatch in returned status from validate_json with an rel-link_id of ''")

    def test_validate_json_spaced_rel_link_id(self):
        msg_details = [
            "Failed validating 'minLength': '' is too short",
            {"On instance:": {
                "[rel-link_id]": ''
            }},
            {"In schema": "[allOf][0][properties][rel-link_id][minLength]"}
        ]
        msg = f"validate_question_jsons.py ERROR: in '[rel-link_id]' key of " \
            f"'{self.perf_dev_spaced_rel_link_id[self.qname_key]}'"
        expected = {ErrorCode.JSON_DECODE_ERROR.value: [{msg: msg_details}]}
        self.assertDictEqual(expected,
                             validate_question_jsons.validate_json(self.perf_dev_spaced_rel_link_id,
                                                                   self.perf_dev_spaced_rel_link_id[self.qname_key],
                                                                   self.entire_perf_dev_schema),
                             msg="Mismatch in returned status from validate_json with an rel-link_id of ' '")

    def test_main_valid(self):
        params = InputArgs(knowl_schema_fname=validate_question_jsons.knowl_schema_default,
                           pf_dev_schema_fname=validate_question_jsons.perf_dev_schema_default,
                           pf_nondev_schema_fname=validate_question_jsons.perf_nondev_schema_default,
                           question_dirs=self.valid_jsons,
                           schema_defs_fname=validate_question_jsons.schema_defs_default,
                           keyword=".question.json",
                           root_dir=param_root_dir_default,
                           out_dir=self.test_dir,
                           log_file=validate_question_jsons.log_file_default)

        expected = {ErrorCode.SUCCESS.value: ["All Jsons are validated."]}
        self.assertDictEqual(expected, validate_question_jsons.main(params),
                             msg="Mismatch in returned status code when running main().")

    def test_main_invalid_schema_file_name(self):
        params = InputArgs(knowl_schema_fname="invalid-schema.json",
                           pf_dev_schema_fname=validate_question_jsons.perf_dev_schema_default,
                           pf_nondev_schema_fname=validate_question_jsons.perf_nondev_schema_default,
                           question_dirs=self.valid_jsons,
                           schema_defs_fname=validate_question_jsons.schema_defs_default,
                           keyword=".question.json",
                           root_dir=param_root_dir_default,
                           out_dir=self.test_dir,
                           log_file=validate_question_jsons.log_file_default)

        expected = {ErrorCode.INVALID_INPUT.value: [
            "validate_question_jsons.py ERROR: Unable to locate one or more schemas. Please check that the file exists."
        ]}

        self.assertDictEqual(expected, validate_question_jsons.main(params),
                             msg="Mismatch in returned status code when running main().")

    def test_main_invalid_question_folder(self):
        params = InputArgs(knowl_schema_fname=validate_question_jsons.knowl_schema_default,
                           pf_dev_schema_fname=validate_question_jsons.perf_dev_schema_default,
                           pf_nondev_schema_fname=validate_question_jsons.perf_nondev_schema_default,
                           question_dirs=["non-existent"],
                           schema_defs_fname=validate_question_jsons.schema_defs_default,
                           keyword=".question.json",
                           root_dir=param_root_dir_default,
                           out_dir=self.test_dir,
                           log_file=validate_question_jsons.log_file_default)

        expected = {ErrorCode.EMPTY_LIST.value: [
            "validate_question_jsons.py WARNING: Received empty file list."
        ]}

        self.assertDictEqual(expected, validate_question_jsons.main(params),
                             msg="Mismatch in returned status code when running main().")

    def test_main_undetermined_schema(self):
        params = InputArgs(knowl_schema_fname=validate_question_jsons.knowl_schema_default,
                           pf_dev_schema_fname=validate_question_jsons.perf_dev_schema_default,
                           pf_nondev_schema_fname=validate_question_jsons.perf_nondev_schema_default,
                           question_dirs=[os.path.join(self.test_dir, self.invalid_qtype_json[self.qpath_key])],
                           schema_defs_fname=validate_question_jsons.schema_defs_default,
                           keyword=".question.json",
                           root_dir=param_root_dir_default,
                           out_dir=self.test_dir,
                           log_file=validate_question_jsons.log_file_default)

        path = os.path.join(param_root_dir_default, self.test_dir, self.invalid_qtype_json[self.qpath_key],
                            self.invalid_qtype_json[self.qname_key]).replace("\\", "/")

        expected = {
            ErrorCode.INVALID_INPUT.value: [
                "validate_question_jsons.py ERROR: Unable to determine the Schema for validation. "
                f"Ensure '{path}' uses the proper Format."
            ],
            ErrorCode.INVALID_JSON_FORMAT.value: [
                f"validate_question_jsons.py ERROR: INVALID_JSON_FORMAT: Refer to '{self.test_dir}/"
                f"{validate_question_jsons.log_file_default}' for details."
            ]
        }

        self.assertDictEqual(expected, validate_question_jsons.main(params),
                             msg="Mismatch in returned status code when running main().")

    def test_main_invalid_jsons(self):
        params = InputArgs(knowl_schema_fname=validate_question_jsons.knowl_schema_default,
                           pf_dev_schema_fname=validate_question_jsons.perf_dev_schema_default,
                           pf_nondev_schema_fname=validate_question_jsons.perf_nondev_schema_default,
                           question_dirs=self.invalid_jsons,
                           schema_defs_fname=validate_question_jsons.schema_defs_default,
                           keyword=".question.json",
                           root_dir=param_root_dir_default,
                           out_dir=self.test_dir,
                           log_file=validate_question_jsons.log_file_default)

        path0 = os.path.join(param_root_dir_default, self.invalid_jsons_combined_paths[0])
        path1 = os.path.join(param_root_dir_default, self.invalid_jsons_combined_paths[1])

        expected = {
            ErrorCode.JSON_DECODE_ERROR.value: [
                {f"validate_question_jsons.py ERROR: in '[snippet]' key of '{path0}'": [
                    "Failed validating 'minLength': '' is too short",
                    {"On instance:": {
                        "[snippet]": ""
                    }},
                    {"In schema": "[allOf][2][properties][snippet][minLength]"}
                ]},
                {f"validate_question_jsons.py ERROR: in '[tests][0][headerpath]' key of "
                 f"'{path1}'": [
                    "Failed validating 'minLength': '' is too short",
                    {"On instance:": {
                        "[tests][0][headerpath]": ""
                    }},
                    {"In schema": "[allOf][2][properties][tests][items][properties][headerpath][minLength]"}
                 ]}
            ],
            ErrorCode.INVALID_JSON_FORMAT.value: [
                f"validate_question_jsons.py ERROR: INVALID_JSON_FORMAT: Refer to '{self.test_dir}/"
                f"{validate_question_jsons.log_file_default}' for details."
            ]
        }
        result = validate_question_jsons.main(params)
        self.assertDictEqual(expected, result,
                             msg="Mismatch in returned status code when running main().")


if __name__ == "__main__":
    unittest.main()
