#!/usr/bin/python3

import json
import os
import sys
import argparse
from colorama import Fore
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 2 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
from lib.errors import ErrorCode
from lib.file_list_generator import dir_walk, changed_paths
from lib.default_strings import param_root_dir_desc, param_question_dir_desc, opt_param_changed_desc, warning_str, \
    error_str, topic_key, id_key, param_root_dir_default, keyword_default, keyword_desc, question_type_key
from lib.qid_helpers import get_next_id_nums, get_qid_code

description = "This function takes in a string representing the path to a file, expected to be a question JSON file, " \
              "and updates the question ID; if the question ID doesn't exist it will be generated. If the content of " \
              "the question didn't change then the question ID will be the same. Upon successful operation, the file " \
              "on the input system is modified with the new question ID."
out_dir_default = "."
out_dir_desc = "The directory to store the resulting question ID update report, relative to root_dir. " \
               "The default is '{}'.".format(out_dir_default)
report_file_default = "QIDUpdateReport.json"
report_file_desc = "The name of the report file; the default is '{}'.".format(report_file_default)

msg_er_color = "{0} {1}:".format(Fore.RED + os.path.basename(__file__), error_str + Fore.RESET)
msg_er = "{0} {1}:".format(os.path.basename(__file__), error_str)
msg_wn_color = "{0} {1}:".format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET)
msg_wn = "{0} {1}:".format(os.path.basename(__file__), warning_str)
msg_gd_color = "{0}:".format(Fore.GREEN + os.path.basename(__file__) + Fore.RESET)
msg_gd = "{0}:".format(os.path.basename(__file__))


def update_uid(in_json_file: str, next_id_nums: dict, wkrole: str) -> (dict, dict):
    '''
    Purpose: This function takes in a string representing the path to a file, expected to be a question JSON file, and
    updates the question ID; if the question ID doesn't exist it will be generated. If the content of the question
    didn't change then the question ID will be the same. Upon successful operation, the file on the input system is
    modified with the new question ID.
    :param in_json_file: This is the path to a json file containing the question's properties.
    :param next_id_nums: This is a dict of the next available uids; expects dict object from
                         uid_helpers.get_next_id_nums().
    :param wkrole: This is the name of the applicable work-role for the question.
    :return: status - a dictionary with keys representing error codes according to class Error or the exception, when
             present.
    '''
    this_status = {}
    if not os.path.exists(in_json_file):
        # In case the input file is invalid
        msg = "{0}: Couldn't find '{1}'."
        print(msg.format(Fore.RED + error_str + Fore.RESET, Fore.CYAN + in_json_file + Fore.RESET))
        this_status[ErrorCode.INVALID_INPUT.value] = [msg.format(error_str, in_json_file)]
        return this_status, next_id_nums

    try:
        # Get the dictionary version of the JSON file
        with open(in_json_file, "r") as json_fp:
            data = json.load(json_fp)

        if question_type_key not in data:
            # This is an older file that uses a one-way hash for the ID or it's not a question file so skip
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(
                "Skipping '{0}' because it's either outdated or not a question JSON.".format(in_json_file))
            return this_status, next_id_nums

        # Ensure the json file has a question id generated and update as necessary
        prev_id = data[id_key]

        # If the question id is blank, it must be a new question
        if prev_id == "":
            data[id_key], next_id_nums = get_qid_code(wkrole, data[topic_key], next_id_nums)
            # Save new data to disk in JSON tabbed format
            with open(in_json_file, "w") as json_w_fp:
                json.dump(data, json_w_fp, indent=4)
            msg = "Updated '{0}' successfully."
            print(msg.format(Fore.GREEN + in_json_file + Fore.RESET))
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(msg.format(in_json_file))

    except json.JSONDecodeError as err:
        msg = "{0} A problem occurred during JSON decode of '{1}': {2}"
        print(msg.format(msg_er_color, os.path.basename(in_json_file), err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value] = msg.format(msg_er, os.path.basename(in_json_file), err)

    except (KeyError, ValueError) as err:
        msg = "{0} {1}: Invalid input or key name: {2}"
        print(msg.format(msg_er_color, os.path.basename(in_json_file), err))
        if ErrorCode.INVALID_INPUT.value not in this_status:
            this_status[ErrorCode.INVALID_INPUT.value] = []
        this_status[ErrorCode.INVALID_INPUT.value] = msg.format(msg_er, os.path.basename(in_json_file), err)

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "{0} Something occurred while processing '{1}': ({2}){3}"
        print(msg.format(msg_er_color, os.path.basename(in_json_file), err.errno, err))
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append(msg.format(msg_er, os.path.basename(in_json_file), err.errno, err))

    return this_status, next_id_nums


def update_status(new_status: dict, dst_status: dict):
    '''
    Purpose: This updates the status code in the main status object.
    :param new_status: A dictionary representing the new status codes.
    :param dst_status: A dictionary representing the main status object.
    :return: None
    '''
    for code, msg_list in new_status.items():
        if code not in dst_status:
            dst_status[code] = []
        dst_status[code] += msg_list


def main(args: argparse.Namespace) -> (dict, str):
    '''
    Purpose: Allows generation of question IDs for JSON files from the auto.py script.
    :return: this_status - a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}
    out_dir = args.out_dir
    if args.out_dir != ".":
        # Eliminates the redundant ././ when combining root_dir and out_dir
        out_dir = os.path.join(args.root_dir, args.out_dir)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if args.question_dirs is None:
        args.question_dirs = ["."]
    try:
        if args.changed:
            # In case only changed files are requested
            paths = changed_paths(repo_base_dir=args.root_dir, keyword=args.filter)
        else:
            # In case all files are requested
            paths = dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs, keyword=args.filter)
        if len(paths) == 0:
            # In case the list is empty
            msg = "{0} {1}: Received empty file list."
            print(msg.format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET))
            if ErrorCode.EMPTY_LIST.value not in this_status:
                this_status[ErrorCode.EMPTY_LIST.value] = []
            this_status[ErrorCode.EMPTY_LIST.value].append(msg.format(os.path.basename(__file__), warning_str))
            return this_status, out_dir

        next_uid_nums = get_next_id_nums(args.filter)

    except ValueError as err:
        msg = "{0} {1}: {2}"
        print(msg.format(msg_er_color, ErrorCode.INVALID_INPUT.name, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, ErrorCode.INVALID_INPUT.name, err))
        return this_status, out_dir

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "({0}){1}".format(err.errno, err)
        print("{0} {1}".format(msg_er_color, msg))
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append("{0} {1}".format(msg_er, msg))
        return this_status, out_dir

    for item in paths:
        # Process each question
        if not os.path.exists(item):
            # In case the path is bad
            msg = "{0}: Couldn't find {1}."
            if ErrorCode.EMPTY_LIST.value not in this_status:
                this_status[ErrorCode.EMPTY_LIST.value] = []
            this_status[ErrorCode.EMPTY_LIST.value].append(msg)
            continue

        try:
            wkrole = os.path.basename(os.getcwd())
            rtn_code, next_uid_nums = update_uid(item, next_uid_nums, wkrole)  # Update the question ID as necessary
        except json.JSONDecodeError as err:
            msg = "{0} Unable to open {1}\n{2}"
            if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
                this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
            this_status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, item, err))
            continue

        update_status(rtn_code, this_status)

    return this_status, out_dir


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    parser.add_argument("-r", "--report_file", type=str, default=report_file_default, help=report_file_desc)
    parser.add_argument("-R", "--root_dir", type=str, default=param_root_dir_default, help=param_root_dir_desc)
    params = parser.parse_args()
    status, out = main(params)
    try:
        msg_success = "{0} question IDs updated successfully; log written to '{1}'."
        if len(status) == 0:
            status[ErrorCode.SUCCESS.value] = [msg_success.format(msg_gd, os.path.join(out, params.report_file))]
        with open(os.path.join(out, params.report_file), "w") as report_fp:
            json.dump(status, report_fp, indent=4)
    except (IOError, json.JSONDecodeError, ValueError, TypeError, OverflowError) as err:
        msg = "{0} Could not create the uid update report file '{1}'; {2}"
        print(msg.format(msg_er_color, Fore.CYAN + os.path.join(out, params.report_file) + Fore.RESET, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in status:
            status[ErrorCode.JSON_DECODE_ERROR.value] = []
        status[ErrorCode.JSON_DECODE_ERROR.value].append(
            msg.format(msg_er, os.path.join(out, params.report_file), err))
    else:
        if (len(status) == 1 and ErrorCode.SUCCESS.value in status) or len(status) == 0:
            # In case the process was successful
            print(msg_success.format(msg_gd_color, Fore.CYAN + os.path.join(out, params.report_file) + Fore.RESET))
        else:
            print(f"{msg_er_color} refer to '{Fore.CYAN + os.path.join(out, params.report_file) + Fore.RESET}' "
                  f"for details.")
    if not (len(status) == 1 and ErrorCode.SUCCESS.value in status):
        # In case status has errors, prevent returning zero
        if ErrorCode.SUCCESS.value in status:
            # For cases where ErrorCode.SUCCESS.value isn't present
            del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
